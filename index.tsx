/**
 * @format
 */

import React from 'react'
import { AppRegistry, Platform } from 'react-native'
import { App } from './src/views/app/App'
import { name as appName } from './app.json'
import { Provider } from 'react-redux'
import { store } from './src/store'
import axios from 'axios'

if (__DEV__) {
    import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))
}

if (Platform.OS === 'android') {
    if (__DEV__) {
        axios.defaults.baseURL = process.env.REACT_APP_API_URL || 'http://192.168.1.166:43708'
    }
} else if (Platform.OS === 'ios') {
    if (__DEV__) {
        axios.defaults.baseURL = 'http://192.168.1.67:43708'
    }
}

// axios.interceptors.request.use(request => {
//     // Edit request config
//     request.headers.common['Content-Type'] = 'application/json'
//     return request
// }, error => {
//     console.log('err')
//     console.log(error)
//     return Promise.reject(error)
// })

// axios.interceptors.response.use(response => {
//     // Edit response config
//     return response
// }, error => {
//     console.log('res error')
//     console.log(error)
//     return Promise.reject(error)
// })

const Main = () => (
    <Provider store={store}>
        {/* <PersistGate loading={null} persistor={persistor}> */}
        <App />
        {/* </PersistGate> */}
    </Provider>
)

AppRegistry.registerComponent(appName, () => Main)
