import { PermissionsAndroid, Platform } from "react-native"

export const askForLocationPermission = async () => {
    if (Platform.OS === 'android') {
        return await askForLocationPermissionAndroid()
    }
    return true
}

export const askForLocationPermissionAndroid = async () => {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: "Roamr location permission",
                message:
                    `Roamr would like to access your device's location`,
                // buttonNeutral: "Ask Me Later",
                buttonNegative: "Cancel",
                buttonPositive: "OK",
            }
        )

        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("Location permission has been granted")
            return true
        } else {
            console.log("Location permission denied")
            return false
        }
    } catch (err) {
        console.log('Error in requesting location permission')
        console.warn(err)
        return false
    }
}