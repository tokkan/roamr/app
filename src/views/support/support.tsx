import React from 'react'
import { Layout, Text } from '@ui-kitten/components'

export const SupportScreen = () => {

    // TODO: Add capability to create support requests and report requests
    // TODO: Have navigation.GoBack() functionality when request has been made
    // TODO: Have optional in parameters to set starting values

    return (
        <Layout
            style={{
                flex: 1,
            }}
        >
            <Text>
                Support screen
            </Text>
        </Layout>
    )
}