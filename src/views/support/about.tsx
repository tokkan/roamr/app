import React from 'react'
import { Layout, Text } from '@ui-kitten/components'

export const AboutScreen = () => {
    // TODO: Add special thanks, licenses, etc.
    // TODO: Add license for mapicons, check what applies for MDI and FA
    // TODO: Check mapbox license
    return (
        <Layout
            style={{
                flex: 1,
            }}
        >
            <Text>
                About screen
            </Text>
        </Layout>
    )
}