import { BusinessRoutes } from "./../../models/common/routes"

export type RootStackBusinessRoutesParamList = {
    [BusinessRoutes.BUSINESS]: undefined
    [BusinessRoutes.CREATE]: {
        latitude: number
        longitude: number
        municipality: {
            municipality_name: string
            municipality_code: string
        }
    }
    [BusinessRoutes.POST]: {
        business_id: string
    }
    // Feed: { sort: 'latest' | 'top' } | undefined
}

export enum BusinessRequestsRoutes {
    DETAILS = 'Details',
    REQUESTS = 'Requests',
    EDIT = 'Edit business',
    EDIT_BUSINESS_TAGS = 'Edit business tags',
}