import React, { useState, useCallback } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { Layout, Text, List, Divider, Button, Spinner } from '@ui-kitten/components'
import { TopNavigationComponent } from './../../../components/top-navigation/top-navigation-stack'
import { i18n } from './../../../locales/localization'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { IRootState } from './../../../store'
import { SocialMediaTypes } from 'src/models'
import { BusinessRoutes } from '..'
import { StyleSheet, View } from 'react-native'
import { ConfirmationModal } from '../../../components/modal/common/confirmation-modal'

interface ISocialMedia {
    handle: string
    type: SocialMediaTypes
}

interface IContact {
    email: string
    phone: string
    social_media: ISocialMedia[]
}

interface IResponseData {
    _id: string
    fire_id: string
    contact: IContact
    name: string
    sub_title: string
    pin_id: string
    users: string[]
}

export const BusinessRequests = ({ navigation }: any) => {
    const auth = useSelector((s: IRootState) => s.auth)
    const [items, setItems] = useState<IResponseData[]>([])
    const [currentItem, setCurrentItem] = useState<IResponseData>()
    const [source, setSource] = useState(axios.CancelToken.source())
    const [isLoading, setIsLoading] = useState(true)
    const [isModalVisible, setIsModalVisible] = useState(false)

    useFocusEffect(
        useCallback(() => {
            setTimeout(() => {
                getBusinesses()
            }, 1000)

            return () => {
                // getBusinesses
                source.cancel
            }
        }, [])
    )

    const navigateToDetails = (business_id: string) => {
        console.log(business_id)
        navigation.navigate(BusinessRoutes.DETAILS, {
            business_id: business_id
        })
    }

    const openAcceptModal = (item: IResponseData) => {
        setCurrentItem(item)
        setIsModalVisible(true)
    }

    const getBusinesses = async () => {
        try {
            setIsLoading(true)

            let response =
                await axios.get<IResponseData[]>(`business/business-requests?limit=20&offset=0`,
                    {
                        headers: {
                            user_id: auth.id,
                            access_token: auth.facebook_access_token,
                            social_provider: auth.social_provider,
                        },
                        cancelToken: source.token,
                    })

            if (response) {
                console.log(response.data)
                setItems(response.data)
            }

            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.debug(error)
        }
    }

    const activateBusiness = async () => {
        console.log('activate business')
        setIsModalVisible(false)
        await activateBusinessRequest()
        await getBusinesses()
    }

    const activateBusinessRequest = async () => {

        if (currentItem === undefined) {
            return
        }

        try {
            console.debug('activate')
            console.debug(currentItem)
            let response =
                await axios.post(`business/activate`, {
                    user_id: auth.id,
                    access_token: auth.facebook_access_token,
                    social_provider: auth.social_provider,
                    business_id: currentItem?.fire_id
                }, {
                    cancelToken: source.token
                })

            if (response) {
                console.debug(response.data)
            }
        } catch (error) {
            console.debug('error')
        }
    }

    const renderItem = ({ item, index }: any) => (
        <Layout>
            <Layout style={
                styles.header
            }>
                <Text category='h6'>
                    {item.name}
                </Text>
            </Layout>
            <Divider />
            <Layout
                style={
                    styles.actionContainer
                }
            >
                <Button
                    style={{
                        marginRight: 10,
                    }}
                    size='tiny'
                    appearance='outline'
                    onPress={() => navigateToDetails(item._id)}
                >
                    {i18n.t('business.requests.info')}
                </Button>
                <Button
                    style={{
                        marginRight: 10,
                    }}
                    size='tiny'
                    appearance='outline'
                    onPress={() => openAcceptModal(item)}
                >
                    {i18n.t('common.accept')}
                </Button>
            </Layout>
            <Divider />
        </Layout>
    )

    const ListItemsComponent = () => {
        return (
            <>
                {items.length > 0 ?
                    <List
                        data={items}
                        renderItem={renderItem}
                    />
                    :
                    <Text style={{
                        padding: 10,
                    }}>
                        {i18n.t('common.no_items')}
                    </Text>
                }
            </>
        )
    }

    const LoadingIndicator = (props: any) => {
        return (
            <View style={[props.style, { flex: 1 }]}>
                {isLoading ?
                    <Layout style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignSelf: 'center',
                        flexDirection: 'column',
                    }}>
                        <Spinner size='giant' />
                    </Layout>
                    : <ListItemsComponent />
                }
            </View>
        )
    }

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                title={i18n.t('business.requests.title')}
                // title=''
                navigation={navigation}
            />
            <Layout style={{ flex: 1, paddingTop: 20, }}>
                <LoadingIndicator />
            </Layout>
            <ConfirmationModal
                visible={isModalVisible}
                setVisible={setIsModalVisible}
                title={currentItem?.name}
                subTitle={currentItem?._id}
                content={i18n.t('business.requests.confirm_message')}
                onConfirm={activateBusiness}
            />
        </Layout>
    )
}

const styles = StyleSheet.create({
    header: {
        height: 40,
        justifyContent: 'center',
        paddingLeft: 15,
    },
    actionContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingLeft: 15,
        paddingTop: 10,
        paddingBottom: 10,
    },
    item: {
        marginVertical: 4,
    },
})