import React, { useState, useCallback } from 'react'
import {
    Layout, Button, List, Text, Divider
} from '@ui-kitten/components'
import { TopNavigationComponent } from '../../../components/top-navigation/top-navigation-stack'
import { StyleSheet } from 'react-native'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { useFocusEffect } from '@react-navigation/native'
import { BusinessRoutes } from '..'
import { i18n } from '../../../locales/localization'

export const ListBusinessScreen = ({ navigation }: any) => {

    const auth = useSelector((s: IRootState) => s.auth)
    const [items, setItems] = useState([])
    const [source, setSource] = useState(axios.CancelToken.source())

    useFocusEffect(
        useCallback(() => {
            getBusinesses()

            return () => {
                // getBusinesses
                source.cancel
            }
        }, [])
    )

    const getBusinesses = async () => {
        try {
            let response =
                await axios.get(`/business?user_id=${auth.id}`,
                    {
                        headers: {
                            user_id: auth.id,
                            access_token: auth.facebook_access_token,
                            social_provider: auth.social_provider,
                        },
                        cancelToken: source.token,
                    })

            if (response) {
                setItems(response.data)
            }
        } catch (error) {
            console.debug(error)
        }
    }

    const renderItem = ({ item, index }: any) => (
        <Layout>
            <Layout style={
                styles.header
            }>
                <Text category='h6'>
                    {item.name}
                </Text>
            </Layout>
            <Divider />
            <Layout
                style={
                    styles.actionContainer
                }
            >
                <Button
                    style={{
                        marginRight: 10,
                    }}
                    size='tiny'
                    appearance='outline'
                    onPress={() => navigation.navigate(BusinessRoutes.POST, {
                        business_id: item._id
                    })}
                >
                    {i18n.t('business.list.post')}
                </Button>
                <Button
                    style={{
                        marginRight: 10,
                    }}
                    size='tiny'
                    appearance='outline'
                >
                    {i18n.t('business.list.edit')}
                </Button>
                <Button
                    style={{
                        marginRight: 10,
                    }}
                    size='tiny'
                    appearance='outline'
                >
                    {i18n.t('business.list.tags')}
                </Button>
                <Button
                    style={{
                        marginRight: 10,
                    }}
                    size='tiny'
                    appearance='outline'
                >
                    {i18n.t('business.list.social_media')}
                </Button>
            </Layout>
            <Divider />
        </Layout>
    )

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title=''
            />
            <Layout style={{ flex: 1, paddingTop: 20, }}>
                <List
                    data={items}
                    renderItem={renderItem}
                />
            </Layout>
        </Layout>
    )
}

const styles = StyleSheet.create({
    header: {
        height: 40,
        justifyContent: 'center',
        paddingLeft: 15,
    },
    actionContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingTop: 10,
        paddingBottom: 10,
    },
    item: {
        marginVertical: 4,
    },
})