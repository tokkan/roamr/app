import { StyleSheet } from "react-native"
// import { PinTypesEnum } from "./../../../models"

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    containerItem: {
        paddingTop: 20,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        marginTop: 20,
    },
    registerButton: {
        marginTop: 50,
    },
})

export interface IState extends IStateFields {
    showPassword: boolean
}

export interface IStateFields {
    // username: string
    // password: string
    name: string
    sub_title: string | undefined
    description: string | undefined
    // user_id: string
    // latitude: number
    // longitude: number
    phone: string | undefined
    email: string | undefined
    // pin_type: PinTypes & string
    // sub_pin_types: PinTypes[]
    social_media_handle: string | undefined
    social_media_type: 'Facebook' | 'Instagram' | 'Twitter'
}

export const initStateFields: IStateFields = {
    // username: '',
    // password: '',
    name: '',
    sub_title: '',
    description: '',
    // user_id: '',
    phone: '',
    email: '',
    // pin_type: PinTypesEnum.store,
    social_media_handle: '',
    social_media_type: 'Facebook',
}

export interface IFieldValidation {
    nameMinLength: number
    nameMaxLength: number
    descriptionMaxLength: number
    socialMediaHandleMinLength: number
    socialMediaHandleMaxLength: number
    usernameMinLength: number
    playerNameMinLength: number
    passwordMinLength: number
}

export const initFieldValidation: IFieldValidation = {
    nameMinLength: 3,
    nameMaxLength: 25,
    descriptionMaxLength: 100,
    socialMediaHandleMinLength: 3,
    socialMediaHandleMaxLength: 25,
    usernameMinLength: 3,
    playerNameMinLength: 3,
    passwordMinLength: 6,
}