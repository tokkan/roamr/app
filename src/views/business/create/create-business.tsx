import React, { useState } from 'react'
import { View } from 'react-native'
import {
    Layout, Text, Input, Button, Spinner, Icon, IndexPath, Select, SelectItem
} from '@ui-kitten/components'
import { object, string, mixed } from 'yup'
import { Formik } from 'formik'
import { i18n } from './../../../locales/localization'
import { ScrollView } from 'react-native-gesture-handler'
import { useSelector } from 'react-redux'
import axios from 'axios'
import { StackNavigationProp } from '@react-navigation/stack'
import { RouteProp } from '@react-navigation/native'
import {
    TopNavigationComponent
} from '../../../components/top-navigation/top-navigation-stack'
import {
    IStateFields, styles, initFieldValidation, initStateFields
} from '.'
import { InfoIcon, AllPinTypes, SocialMediaTypes, PinTypesEnum } from './../../../models'
import { IRootState } from 'src/store'
import { RootStackMapRoutesParamList } from './../../map'
import { LanguageTypes } from './../../../models/common/common'
import { MapRoutes } from './../../../models/common/routes'

interface ICreateBusinessPayload {
    zone_id: string
    businessName: string
    pin_type: AllPinTypes
    sub_pin_types: AllPinTypes[]
    sub_title: string
    description: string
    latitude: number
    longitude: number
    emailAddress: string
    phoneNumber: string
    socialMediaHandle: string
    socialMediaProvider: SocialMediaTypes
}

const validationSchema = object().shape<IStateFields>({
    name: string()
        .min(3)
        .required(),
    description: string()
        .max(100)
        .optional(),
    email: string()
        .max(30)
        .optional(),
    phone: string()
        .max(30)
        .optional(),
    social_media_handle: string()
        .max(30)
        .optional(),
    social_media_type: string()
        .oneOf(['Facebook', 'Instagram', 'Twitter'])
        .required(),
    sub_title: string()
        .max(30)
        .optional(),
    // pin_type: mixed().oneOf(['Camping'] as PinTypes[]).required()
    // pin_type: mixed()
    //     .oneOf([...Object.values(PinTypes)] as PinTypes[])
    //     .required()
    // pin_type: string()
    //     .oneOf(Object.entries(PinTypes).map(m => m[1]))
    //     .required()
})

const options = Object.entries(PinTypesEnum)
const languageTypeOptions = Object.entries(LanguageTypes)

type ScreenRouteProp = RouteProp<
    RootStackMapRoutesParamList,
    'Create business'
    // MapRoutes.CREATE_BUSINESS
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackMapRoutesParamList,
    'Create business'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

export const CreateBusinessScreen: React.FC<IProps> = ({
    route,
    navigation
}) => {
    const { latitude, longitude, municipality, country } = route.params
    const auth = useSelector((s: IRootState) => s.auth)
    const [source, setSource] = useState(axios.CancelToken.source())
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [socialProviderIndex, setSocialProviderIndex] =
        useState(new IndexPath(0))
    const [socialProviderOptions] = useState([
        'Facebook',
        'Instagram',
        'Twitter',
    ])
    const socialMediaProviderDisplayValue =
        socialProviderOptions[socialProviderIndex.row]

    const [languageIndex, setLanguageIndex] =
        useState(new IndexPath(0))
    const [languageOptions] = useState(languageTypeOptions)
    const languageDisplayValue = languageOptions[languageIndex.row]
    const [pinTypeIndex, setPinTypeIndex] =
        useState(new IndexPath(0))
    const [pinTypeOptions] = useState(options)
    const pinTypeDisplayValue = pinTypeOptions[pinTypeIndex.row]
    const [subPinTypeIndex, setSubPinTypeIndex] =
        useState<IndexPath[]>([
            // new IndexPath(0),
            // new IndexPath(1),
        ])
    const [subPinTypeOptions] = useState(options)
    const subPinTypeDisplayValues = subPinTypeIndex.map(index => {
        return subPinTypeOptions[index.row]
    })

    console.log('Create business screen')
    console.log(municipality)
    console.debug('options')
    console.debug(options)
    console.debug(pinTypeDisplayValue)

    // useEffect(() => {
    //     const focus = navigation.addListener('focus', () => {
    //         setSource(axios.CancelToken.source())
    //     })

    //     const unsubscribe = navigation.addListener('blur', () => {
    //         source.cancel()
    //     })

    //     return () => {
    //         focus
    //         unsubscribe
    //         source.cancel()
    //     }
    // }, [navigation])

    const onSubmit = async (values: IStateFields) => {
        try {
            setIsSubmitting(true)
            console.debug('on submit')
            console.debug(values)

            let response =
                await axios.post('business/create-business', {
                    user_id: auth.id,
                    access_token: auth.facebook_access_token,
                    social_provider: auth.social_provider,
                    payload: {
                        zone_id: '',
                        businessName: values.name,
                        // business_tags: [],
                        sub_title: values.sub_title,
                        description: values.description,
                        latitude: latitude,
                        longitude: longitude,
                        emailAddress: values.email,
                        phoneNumber: values.phone,
                        pin_type: pinTypeDisplayValue[0],
                        sub_pin_types: subPinTypeDisplayValues.map(m => m[1]),
                        socialMediaHandle: values.social_media_handle,
                        // socialMediaProvider: values.social_media_type,
                        socialMediaProvider: socialMediaProviderDisplayValue,
                        language: languageDisplayValue[0],
                        municipality,
                        country: country,
                    }
                }, {
                    cancelToken: source.token
                })

            setIsSubmitting(false)

            if (response) {
                console.debug(response)
                navigation.goBack()
            }
        } catch (error) {
            setIsSubmitting(false)
            console.debug(error)
        }
    }

    const setSocialMediaTypeSelect = (index: any) => {
        setSocialProviderIndex(index)
    }

    const setLanguageSelect = (index: any) => {
        setLanguageIndex(index)
    }

    const setPinTypeSelect = (index: any) => {
        setPinTypeIndex(index)
    }

    const setSubPinTypeSelect = (index: any) => {
        setSubPinTypeIndex(index)
    }

    const isFieldValid = (
        fieldValue: keyof IStateFields,
        errorValue: string | undefined,
        touched: boolean | undefined
    ) => {
        if (errorValue === undefined) {
            return true
        } else if (fieldValue.length > 0 && errorValue !== undefined) {
            return false
        } else {
            return false
        }
    }

    const LoadingIndicator = (props: any) => (
        <View style={[props.style, styles.indicator]}>
            {isSubmitting ?
                <Spinner size='small' /> :
                <Icon {...props} name='plus-square-outline' />
            }
        </View>
    )

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title=''
            />
            <Layout style={{ flex: 1 }}>
                <ScrollView>
                    <Layout style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        marginTop: 20,
                        marginBottom: 30,
                    }}>
                        <Text category='h5'>
                            {i18n.t('business.create.title')}
                        </Text>
                    </Layout>
                    <Formik
                        initialValues={initStateFields}
                        validationSchema={validationSchema}
                        onSubmit={values => onSubmit(values)}
                        // isInitialValid={false}
                        initialErrors={{
                            name: undefined
                        }}
                    // initialTouched={{
                    //     name: false
                    // }}
                    // validateOnMount={true}
                    >
                        {({
                            values,
                            errors,
                            isValid,
                            touched,
                            handleChange,
                            handleBlur,
                            handleReset,
                            handleSubmit,
                        }) => (
                                <Layout style={styles.container}>
                                    <Input
                                        label={i18n.t('business.model.name')}
                                        caption={i18n.t('common.required')}
                                        captionIcon={InfoIcon}
                                        status={
                                            isFieldValid(
                                                'name',
                                                errors.name,
                                                touched.name
                                            ) ? 'basic' : 'danger'
                                        }
                                        value={values.name}
                                        onChangeText={handleChange('name')}
                                    // onBlur={handleBlur('name')}
                                    />
                                    <Input
                                        style={styles.containerItem}
                                        value={values.sub_title}
                                        label={i18n.t('business.model.sub_title')}
                                        caption={`Should contain at least ${initFieldValidation.passwordMinLength} symbols`}
                                        // accessoryRight={renderIcon}
                                        // captionIcon={AlertIcon}
                                        onChangeText={handleChange('sub_title')}
                                    // onBlur={handleBlur('sub_title')}
                                    />
                                    <Input
                                        style={styles.containerItem}
                                        label={i18n.t('business.model.description')}
                                        multiline={true}
                                        caption={i18n.t('common.required')}
                                        captionIcon={InfoIcon}
                                        value={values.description}
                                        onChangeText={handleChange('description')}
                                    // onBlur={handleBlur('description')}
                                    />
                                    <Select
                                        style={[
                                            styles.containerItem,
                                            {
                                                width: '100%',
                                            }
                                        ]}
                                        label={i18n.t('common.language')}
                                        selectedIndex={languageIndex}
                                        value={languageDisplayValue[1]}
                                        onSelect={index => setLanguageSelect(index)}
                                    >
                                        {languageOptions.map((m, i) => (
                                            <SelectItem key={i} title={m[1]} />
                                        ))}
                                    </Select>
                                    <Input
                                        style={styles.containerItem}
                                        label={i18n.t('business.model.phone')}
                                        caption={i18n.t('common.optional')}
                                        captionIcon={InfoIcon}
                                        dataDetectorTypes='phoneNumber'
                                        textContentType='telephoneNumber'
                                        keyboardType='phone-pad'
                                        keyboardAppearance='dark'
                                        value={values.phone}
                                        onChangeText={handleChange('phone')}
                                    // onBlur={handleBlur('phone')}
                                    />
                                    <Input
                                        style={styles.containerItem}
                                        label={i18n.t('business.model.email')}
                                        caption={i18n.t('common.required')}
                                        captionIcon={InfoIcon}
                                        textContentType='emailAddress'
                                        keyboardType='email-address'
                                        value={values.email}
                                        onChangeText={handleChange('email')}
                                    />
                                    <Select
                                        style={[
                                            styles.containerItem,
                                            {
                                                width: '100%',
                                            }
                                        ]}
                                        label={i18n.t('business.model.pin_type')}
                                        selectedIndex={pinTypeIndex}
                                        value={pinTypeDisplayValue[1]}
                                        onSelect={index => setPinTypeSelect(index)}
                                    >
                                        {pinTypeOptions.map((m, i) => (
                                            <SelectItem key={i} title={m[1]} />
                                        ))}
                                    </Select>
                                    <Select
                                        style={[
                                            styles.containerItem,
                                            {
                                                width: '100%',
                                            }
                                        ]}
                                        multiSelect={true}
                                        label={i18n.t('business.model.sub_pin_type')}
                                        caption={i18n.t('common.optional')}
                                        // captionIcon={InfoIcon}
                                        selectedIndex={subPinTypeIndex}
                                        value={subPinTypeDisplayValues.map(m => m[1]).join(',')}
                                        onSelect={index => setSubPinTypeSelect(index)}
                                    >
                                        {subPinTypeOptions.map((m, i) => (
                                            <SelectItem key={i} title={m[1]} />
                                        ))}
                                    </Select>
                                    <Input
                                        style={styles.containerItem}
                                        label={i18n.t('business.model.social_media_handle')}
                                        caption={i18n.t('common.optional')}
                                        captionIcon={InfoIcon}
                                        value={values.social_media_handle}
                                        onChangeText={handleChange('social_media_handle')}
                                    // onBlur={handleBlur('social_media_handle')}
                                    />
                                    <Select
                                        style={[
                                            styles.containerItem,
                                            {
                                                width: '100%',
                                            }
                                        ]}
                                        label={i18n.t('business.model.social_media_type')}
                                        selectedIndex={socialProviderIndex}
                                        value={socialMediaProviderDisplayValue}
                                        onSelect={index => setSocialMediaTypeSelect(index)}
                                    >
                                        {socialProviderOptions.map((m, i) => (
                                            <SelectItem key={i} title={m} />
                                        ))}
                                    </Select>
                                    <Layout style={{
                                        marginBottom: 20,
                                        flexDirection: 'row',
                                    }}>
                                        <Button
                                            style={[
                                                styles.button,
                                                {
                                                    marginRight: 20,
                                                }
                                            ]}
                                            appearance='outline'
                                            onPress={handleReset}
                                        >
                                            {i18n.t('common.reset')}
                                        </Button>
                                        <Button
                                            style={styles.button}
                                            disabled={!isValid}
                                            appearance='outline'
                                            accessoryLeft={LoadingIndicator}
                                            onPress={handleSubmit}
                                        >
                                            {i18n.t('common.create')}
                                        </Button>
                                    </Layout>
                                </Layout>
                            )}
                    </Formik>
                </ScrollView>
            </Layout>
        </Layout>
    )
}