import React, { useState, useCallback } from 'react'
import { Layout, Text, Spinner } from '@ui-kitten/components'
import { TopNavigationComponent } from './../../../components/top-navigation/top-navigation-stack'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { useFocusEffect } from '@react-navigation/native'
import { SocialMediaTypes } from './../../../models'
import { i18n } from './../../../locales/localization'
import { View } from 'react-native'

interface ISocialMedia {
    handle: string
    type: SocialMediaTypes
}

interface IContact {
    email: string
    phone: string
    social_media: ISocialMedia[]
}

interface IResponseData {
    _id: string
    parent_id: string
    is_parent: boolean
    name: string
    sub_title: string
    description: string
    users: string[]
    // business_tags: BusinessTypes[]
    pin_id: string
    // merchandise: IMerchandise
    // default_business_hours?: IBusinessHours
    // custom_business_hours?: ICustomBusinessHours
    contact: IContact
    is_verified: boolean
    is_active: boolean
}

export const BusinessDetailsScreen = ({ route, navigation }: any) => {
    const { business_id } = route.params
    const auth = useSelector((s: IRootState) => s.auth)
    const [item, setItem] = useState<IResponseData | undefined>(undefined)
    const [source, setSource] = useState(axios.CancelToken.source())
    const [isLoading, setIsLoading] = useState(true)

    useFocusEffect(
        useCallback(() => {
            if (business_id === undefined) {
                navigation.goBack()
            }

            getBusinesses()

            return () => {
                // getBusinesses
                source.cancel
            }
        }, [])
    )

    const getBusinesses = async () => {
        try {
            setIsLoading(true)

            let response =
                await axios.get<IResponseData>(`/business/business/${business_id}`,
                    {
                        headers: {
                            user_id: auth.id,
                            access_token: auth.facebook_access_token,
                            social_provider: auth.social_provider,
                        },
                        cancelToken: source.token,
                    })

            if (response) {
                setItem(response.data)
            }

            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.debug(error)
        }
    }

    const LoadingIndicator = (props: any) => {
        return (
            <View style={[props.style, { flex: 1 }]}>
                {isLoading ?
                    <Layout style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignSelf: 'center',
                        flexDirection: 'column',
                    }}>
                        <Spinner size='giant' />
                    </Layout>
                    : <ListItemsComponent />
                }
            </View>
        )
    }

    const ListItemsComponent = () => {
        return (
            <>
                {item !== undefined ?
                    <Layout>
                        <Layout>
                            <Text category='h5'>
                                {item.name}
                            </Text>
                            <Text>
                                {item._id}
                            </Text>
                        </Layout>
                        <Layout>
                            <Text>
                                {item.contact.email}
                            </Text>
                            <Text>
                                {item.contact.phone}
                            </Text>
                            <Text>
                                {item.contact.social_media[0].handle}
                            </Text>
                            <Text>
                                {item.contact.social_media[0].type}
                            </Text>
                        </Layout>
                        <Layout>
                            <Text>
                                {item.description}
                            </Text>
                        </Layout>
                    </Layout>
                    :
                    <Text>
                        {i18n.t('business.details.no_items')}
                    </Text>
                }
            </>
        )
    }

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                title=''
                navigation={navigation}
            />
            <Layout style={{
                flex: 1,
                paddingHorizontal: 10,
                paddingVertical: 20
            }}>
                <LoadingIndicator />
            </Layout>
        </Layout>
    )
}