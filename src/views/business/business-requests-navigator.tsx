import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { BusinessRequestsRoutes as BusinessRoutes } from '.'
import { BusinessDetailsScreen } from './details/business-details'
import { BusinessRequests } from './requests/business-requests'

const Stack = createStackNavigator()

export const BusinessRequestsNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={BusinessRoutes.REQUESTS}
            headerMode='none'
        >
            <Stack.Screen
                name={BusinessRoutes.REQUESTS}
                component={BusinessRequests}
            />
            <Stack.Screen
                name={BusinessRoutes.DETAILS}
                component={BusinessDetailsScreen}
            />
        </Stack.Navigator>
    )
}