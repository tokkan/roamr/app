import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { BusinessRoutes, RootStackBusinessRoutesParamList } from '.'
import { CreateBusinessScreen } from '../business/create/create-business'
import { ListBusinessScreen } from './list/list-business'
import { CreatePostScreen } from '../social/posts/create/create-post'
import { BusinessDetailsScreen } from './details/business-details'

const Stack = createStackNavigator<RootStackBusinessRoutesParamList>()

export const BusinessNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={BusinessRoutes.BUSINESS}
            headerMode='none'
        >
            <Stack.Screen
                name={BusinessRoutes.BUSINESS}
                component={ListBusinessScreen}
            />
            <Stack.Screen
                name={BusinessRoutes.POST}
                component={CreatePostScreen}
            />
        </Stack.Navigator>
    )
}