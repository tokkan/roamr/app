import React from 'react'
import { View } from "react-native"
import {
    Layout, Button, Text, Spinner, Icon
} from "@ui-kitten/components"
import { styles } from ".."
import { i18n } from './../../../locales/localization'

interface IProps {
    isAuthenticating: boolean
    onLogin: Function
}

export const FacebookLoginComponent: React.FC<IProps> = ({
    isAuthenticating,
    onLogin,
}) => {

    const FBLoadingIndicator = (props: any) => (
        <View style={[props.style, styles.indicator]}>
            {isAuthenticating ?
                <Spinner size='small' /> :
                <Icon
                    {...props}
                    name='facebook'
                />
            }
        </View>
    )

    return (
        <Layout style={styles.container}>
            <Text style={{
                marginBottom: 10
            }}>
                {i18n.t("login.facebook_login")}
            </Text>
            <Button
                style={{
                    backgroundColor: 'rgb(59,89,152)',
                    borderColor: 'rgb(59,89,152)',
                    paddingLeft: 0,
                }}
                accessoryLeft={FBLoadingIndicator}
                onPress={() => onLogin()}
            >
                Facebook
            </Button>
        </Layout>
    )
}