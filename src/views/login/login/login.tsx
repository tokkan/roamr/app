import React, { useState, useEffect } from 'react'
import {
    Image
} from 'react-native'
import {
    Divider, Layout
} from '@ui-kitten/components'
import axios from 'axios'
import { useDispatch } from 'react-redux'
import { IAuthActionDispatch, AuthActions } from '../../../store/auth'
import { NavigationProp } from '@react-navigation/native'
import { LoginPaths } from '..'
import { IActionDispatch } from 'src/store'
import auth from '@react-native-firebase/auth'
import { LoginManager, AccessToken } from 'react-native-fbsdk'
import { IStateFields } from '.'
import { FacebookLoginComponent } from './fb-login-component'
import { SocialMediaTypes } from './../../../models'

interface IProps {
    navigation: NavigationProp<any, any>
}

export const LoginScreen: React.FC<IProps> = ({ navigation }) => {

    const dispatch = useDispatch<IActionDispatch<IAuthActionDispatch>>()
    const [isAuthenticating, setIsAuthenticating] = useState(false)
    const [isFBAuthenticating, setIsFBAuthenticating] = useState(false)
    const [source, setSource] = useState(axios.CancelToken.source())

    useEffect(() => {

        const focus = navigation.addListener('focus', () => {
            setSource(axios.CancelToken.source())
        })

        const unsubscribe = navigation.addListener('blur', () => {
            source.cancel()
        })

        return () => {
            focus
            unsubscribe
            source.cancel()
        }
    }, [navigation])

    const onLogin = async (values: IStateFields) => {
        try {
            setIsAuthenticating(true)

            console.log('onlogin')

            let response =
                await axios.post(LoginPaths.LOGIN,
                    {
                        username: values.username,
                        password: values.password
                    },
                    {
                        cancelToken: source.token
                    })

            console.log('onlogin response')
            console.log(response)

            if (response) {
                setIsAuthenticating(false)
                dispatch({
                    type: AuthActions.LOGIN,
                    payload: {
                        username: response.data.username,
                        access_token: response.data.access_token,
                    }
                })

            } else {
                setIsAuthenticating(false)
                source.cancel('Operation cancelled by failing getting response')
            }

        } catch (error) {
            setIsAuthenticating(false)

            if (axios.isCancel(error)) {
                if (__DEV__) {
                    console.debug('HTTP request cancelled', error.message)
                }
            }

            if (__DEV__) {
                console.debug(error)
            }
        }
    }

    const onFacebookLogin = async () => {
        setIsFBAuthenticating(true)

        // Attempt login with permissions
        const result =
            await LoginManager.logInWithPermissions(['public_profile', 'email'])

        if (result.isCancelled) {
            throw 'User cancelled the login process'
        }

        // Once signed in, get the users AccessToken
        const data = await AccessToken.getCurrentAccessToken()

        if (!data) {
            throw 'Something went wrong obtaining access token'
        }

        // Create a Firebase credential with the AccessToken
        const facebookCredential =
            auth.FacebookAuthProvider.credential(data.accessToken)

        // Sign-in the user with the credential
        let credential = await auth().signInWithCredential(facebookCredential)

        console.log('firebase credentials')
        console.log(credential)

        try {
            let social_response =
                await axios.post(LoginPaths.CHECK_SOCIAL_REGISTER,
                    {
                        user_id: credential.user.uid,
                        email: credential.user.email,
                        // user_id: credential.user.uid,
                        access_token: facebookCredential.token,
                    })

            if (!social_response.data.validation_passed) {
                throw 'Creation of user account failed'
            }

            let response =
                await axios.post(LoginPaths.SOCIAL_LOGIN,
                    {
                        user_id: credential.user.uid,
                        // username: social_response.data.username,
                        access_token: facebookCredential.token,
                        social_provider: 'facebook',
                        // password: social_response.data.password
                    },
                    {
                        // cancelToken: source.token
                    })

            if (response) {
                setIsFBAuthenticating(false)

                let first =
                    credential.additionalUserInfo?.profile?.first_name || ''
                let last =
                    credential.additionalUserInfo?.profile?.last_name || ''

                dispatch({
                    type: AuthActions.SOCIAL_LOGIN,
                    payload: {
                        _id: response.data._id,
                        id: credential.user.uid,
                        username: response.data.username,
                        avatar: credential.user.photoURL || '',
                        name: credential.user.displayName || '',
                        first_name: first,
                        last_name: last,
                        access_token: response.data.access_token,
                        social_provider: SocialMediaTypes.FACEBOOK,
                        facebook_access_token: facebookCredential.token,
                        mapbox_access_token: response.data.mapbox_access_token,
                        roles: response.data.roles,
                    }
                })

            } else {
                setIsFBAuthenticating(false)
                source.cancel('Operation cancelled by failing getting response')
            }
        } catch (error) {
            setIsFBAuthenticating(false)
            source.cancel()

            if (axios.isCancel(error)) {
                if (__DEV__) {
                    console.debug('HTTP request cancelled', error.message)
                }
            }

            if (__DEV__) {
                console.debug(error)
            }
        }
    }

    return (
        <Layout style={{
            flex: 1,
        }}>
            <Layout style={{
                paddingTop: 125,
                flexDirection: 'row',
                justifyContent: 'center',
            }}>
                <Image
                    style={{
                        height: 200,
                        resizeMode: 'contain',
                    }}
                    source={require('./../../../images/roamr-with-text.png')}
                />
            </Layout>
            <Layout style={{
                flex: 1,
                justifyContent: 'flex-start',
                paddingTop: 50,
            }}>
                <FacebookLoginComponent
                    isAuthenticating={isFBAuthenticating}
                    onLogin={onFacebookLogin}
                />
            </Layout>
            <Divider />
        </Layout>
    )
}