import { Layout, Input, Button, Icon, Spinner } from "@ui-kitten/components"
import { styles, AlertIcon } from ".."
import { useFormik } from "formik"
import { useState, useEffect } from "react"
import { object, string } from "yup"
import {
    initFieldValidation, IStateFields, IState
} from "."
import { TouchableWithoutFeedback, View } from "react-native"

interface IProps {
    isAuthenticating: boolean,
    onLogin: (values: IStateFields) => void
}

export const LocalLoginComponent: React.FC<IProps> = ({
    isAuthenticating,
    onLogin,
}) => {

    const [fieldValidation] = useState(initFieldValidation)
    const [secureTextEntry, setSecureTextEntry] = useState(true)

    useEffect(() => {
        // validate so that button become disabled at startup
        setValues({ ...values, username: '' }, true)
    }, [])

    const validationSchema = object().shape<IStateFields>({
        username: string()
            .min(fieldValidation.usernameMinLength)
            .required('Please provide a username'),
        password: string()
            .min(fieldValidation.passwordMinLength, 'Minimum of 4 characters are required')
            .required('Please enter a password'),
    })

    const {
        // submitForm,
        initialValues,
        values,
        setValues,
        setFieldValue,
        isValid = false,
        isSubmitting,
        validateForm,
        handleSubmit,
        handleChange,
    } = useFormik<IStateFields>({
        initialValues: {
            username: '',
            password: ''
        },
        validationSchema: validationSchema,
        onSubmit: values => onLogin(values),
    })

    const onHandleChange = (prop: keyof IState) => (value: string) => {
        setValues({ ...values, [prop]: value })
    }

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry)
    }

    const renderIcon = (props: any) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const LoadingIndicator = (props: any) => (
        <View style={[props.style, styles.indicator]}>
            {isAuthenticating ?
                <Spinner size='small' /> :
                <Icon {...props} name='log-in' />
            }
        </View>
    )

    return (
        <Layout style={styles.container}>
            <Input
                label='Username'
                placeholder='Username'
                value={values.username}
                onChangeText={onHandleChange('username')}
            />
            <Input
                style={styles.containerItem}
                value={values.password}
                label='Password'
                placeholder='Password'
                caption={`Should contain at least ${fieldValidation.passwordMinLength} symbols`}
                accessoryRight={renderIcon}
                captionIcon={AlertIcon}
                secureTextEntry={secureTextEntry}
                onChangeText={onHandleChange('password')}
            />
            <Button
                style={styles.button}
                disabled={!isValid}
                appearance='outline'
                accessoryLeft={LoadingIndicator}
                onPress={handleSubmit}
            >
                Sign in
            </Button>
        </Layout>
    )
}