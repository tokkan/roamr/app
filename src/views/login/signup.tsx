import React, { useState, useEffect, useCallback } from 'react'
import {
    StyleSheet, View, TouchableWithoutFeedback, ImageProps
} from 'react-native'
import {
    Divider, Icon, Layout, TopNavigationAction, Input, Spinner, Button
} from '@ui-kitten/components'
import axios, { AxiosResponse } from 'axios'
import { useFormik } from 'formik'
import { object, string } from 'yup'
import { IAuthActionDispatch, AuthActions } from './../../store/auth'
import { LoginPaths, initFieldValidation } from '.'
import { NavigationProp } from '@react-navigation/native'
import { debounce } from 'lodash'
import { useDispatch } from 'react-redux'
import { IActionDispatch } from 'src/store'

// https://medium.com/swlh/forms-in-react-native-with-formik-and-yup-the-hooks-way-246aba04e740

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    containerItem: {
        paddingTop: 20,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        marginTop: 20,
    },
})

enum InputTrafficStatus {
    BASIC = 'basic',
    PRIMARY = 'primary',
    SUCCESS = 'success',
    INFO = 'info',
    WARNING = 'warning',
    DANGER = 'danger',
    CONTROL = 'control',
}

const BackIcon = (props: any) => (
    <Icon {...props} name='arrow-back' />
)

const AlertIcon = (props: any) => (
    <Icon {...props} name='alert-circle-outline' />
)

enum NameCheck {
    USERNAME = 'username',
    PLAYER_NAME = 'player_name',
    FAMILY_NAME = 'family_name',
}

interface INameCheck<T extends string | boolean> {
    [NameCheck.USERNAME]: T
    [NameCheck.PLAYER_NAME]: T
    [NameCheck.FAMILY_NAME]: T
}

interface INameFieldsBase<T extends string | boolean> {
    'username': T
    'player_name': T
    'family_name': T
}

interface ILoginFields<T extends string> {
    username: T
    password: T
}

interface IStateNameFields extends INameCheck<string> {
}

interface IStateFields extends IStateNameFields {
    password: string
}

interface IState extends INameFieldsBase<string> {
    // interface IState extends INameCheck<string> {
    password: string
    showPassword: boolean
}

interface IFailedValidationResponse extends INameCheck<boolean> {
    validation_passed: boolean,
}

interface ILoginResponse {
    username: string
    access_token: string
}

interface IRegisterResponse extends INameCheck<boolean> {
    validation_passed: boolean
}

enum AvailabilityState {
    UNDETERMINED,
    AVAILABLE,
    NOT_AVAILABLE,
    CHECKING_AVAILABILITY,
}

interface IProps {
    navigation: NavigationProp<any, any>
}

export const SignUpScreen: React.FC<IProps> = ({ navigation }) => {

    const dispatch = useDispatch<IActionDispatch<IAuthActionDispatch>>()
    const [secureTextEntry, setSecureTextEntry] = useState(true)
    const [isAuthenticating, setIsAuthenticating] = useState(false)
    const [source, setSource] = useState(axios.CancelToken.source())
    const [fieldValidation] = useState(initFieldValidation)
    const [usernameState, setUsernameState] = useState(AvailabilityState.UNDETERMINED)
    const [playerNameState, setPlayerNameState] = useState(AvailabilityState.UNDETERMINED)
    const [familyNameState, setFamilyNameState] = useState(AvailabilityState.UNDETERMINED)

    const validationSchema = object().shape<IStateFields>({
        username: string()
            .min(fieldValidation.usernameMinLength)
            .required('Please provide a username'),
        player_name: string()
            .min(fieldValidation.playerNameMinLength)
            .required('Please provide a player name'),
        family_name: string()
            .min(fieldValidation.playerNameMinLength)
            .required('Please provide a family name'),
        password: string()
            .min(fieldValidation.passwordMinLength, 'Minimum of 4 characters are required')
            .required('Please enter a password')
    })

    const {
        // submitForm,
        initialValues,
        values,
        setValues,
        setFieldValue,
        isValid = false,
        isSubmitting,
        validateForm,
        handleSubmit,
        handleChange,
    } = useFormik<IStateFields>({
        initialValues: {
            username: '',
            player_name: '',
            family_name: '',
            password: '',
        },
        validationSchema: validationSchema,
        onSubmit: values => onRegisterSubmit(values),
    })

    useEffect(() => {
        // validate so that button become disabled at startup
        setValues({ ...values, username: '' }, true)
    }, [])

    useEffect(() => {

        const focus = navigation.addListener('focus', () => {
            setSource(axios.CancelToken.source())
        })

        const unsubscribe = navigation.addListener('blur', () => {
            source.cancel()
        })

        return () => {
            focus
            unsubscribe
            source.cancel()
        }
    }, [navigation])

    const getInputTrafficStatus = (status: AvailabilityState) => {
        switch (status) {
            case AvailabilityState.AVAILABLE:
                return InputTrafficStatus.PRIMARY

            case AvailabilityState.NOT_AVAILABLE:
                return InputTrafficStatus.DANGER

            case AvailabilityState.UNDETERMINED:
            case AvailabilityState.CHECKING_AVAILABILITY:
            default:
                return 'basic'
        }
    }

    const setFieldNameState = (state: AvailabilityState) => {
        setUsernameState(state)
        setPlayerNameState(state)
        setFamilyNameState(state)
    }

    const checkNameAvailability = async () => {
        try {
            setFieldNameState(AvailabilityState.CHECKING_AVAILABILITY)

            const response =
                await axios.post<IStateNameFields, AxiosResponse<INameCheck<boolean>>>
                    (LoginPaths.CHECK_NAME,
                        {
                            username: values.username,
                            player_name: values.player_name,
                            family_name: values.family_name,
                        },
                        {
                            cancelToken: source.token
                        })

            if (response) {
                console.log('response')
                console.log(response.data)

                response.data.username ?
                    setUsernameState(AvailabilityState.AVAILABLE) :
                    setUsernameState(AvailabilityState.NOT_AVAILABLE)

                response.data.player_name ?
                    setPlayerNameState(AvailabilityState.AVAILABLE) :
                    setPlayerNameState(AvailabilityState.NOT_AVAILABLE)

                response.data.family_name ?
                    setFamilyNameState(AvailabilityState.AVAILABLE) :
                    setFamilyNameState(AvailabilityState.NOT_AVAILABLE)

                return response
            }

            return null

        } catch (error) {
            source.cancel()
            return null
        }
    }

    const nameCheckHandler =
        useCallback(
            // debounce(() => checkNameAvailability(),
            debounce(checkNameAvailability,
                1000, {
                trailing: true
            }), [])

    const onRegister = async (values: IStateFields) => {
        try {
            setIsAuthenticating(true)

            let response =
                await axios.post<
                    IStateFields,
                    AxiosResponse<IRegisterResponse>
                >
                    (LoginPaths.REGISTER,
                        {
                            username: values.username,
                            // username: null,
                            password: values.password,
                            player_name: values.player_name,
                            family_name: values.family_name
                        },
                        {
                            cancelToken: source.token
                        })

            if (!response) {
                setIsAuthenticating(false)
                source.cancel('Operation cancelled by failing getting response')
                return null
            }

            if (response
                .data
                .validation_passed === false) {
                // let failedData = (response as AxiosResponse<IFailedValidationResponse>).data
                let failedData = response.data

                failedData.username ?
                    setUsernameState(AvailabilityState.AVAILABLE) :
                    setUsernameState(AvailabilityState.NOT_AVAILABLE)

                failedData.player_name ?
                    setPlayerNameState(AvailabilityState.AVAILABLE) :
                    setPlayerNameState(AvailabilityState.NOT_AVAILABLE)

                failedData.family_name ?
                    setFamilyNameState(AvailabilityState.AVAILABLE) :
                    setFamilyNameState(AvailabilityState.NOT_AVAILABLE)

            } else {
                onLogin(values)
                setIsAuthenticating(false)
                // return response
            }

        } catch (error) {
            setIsAuthenticating(false)

            if (axios.isCancel(error)) {
                if (__DEV__) {
                    console.log('HTTP request cancelled', error.message)
                }
            }

            if (__DEV__) {
                console.log(error)
            }

            return null
        } finally {
            setIsAuthenticating(false)
        }
    }

    const onRegisterSubmit = async (values: IStateFields) => {
        const nres = await checkNameAvailability()

        if (nres === null) {
            // error message
            console.log('Name check failed')
        } else {
            const entries = Object.values(nres.data)

            if (entries.some((s) => s === false)) {
                console.log('All names arent ok')
                return
            }

            const res = onRegister(values)
        }
    }

    const onLogin = async (values: IStateFields) => {

        try {
            setIsAuthenticating(true)

            let response =
                await axios.post(LoginPaths.LOGIN,
                    {
                        username: values.username,
                        password: values.password
                    },
                    {
                        cancelToken: source.token
                    })

            if (response) {
                dispatch({
                    type: AuthActions.LOGIN,
                    payload: {
                        username: response.data.username,
                        access_token: response.data.access_token,
                    }
                })

                setIsAuthenticating(false)
            } else {
                source.cancel('Operation cancelled by failing getting response')
            }

        } catch (error) {
            setIsAuthenticating(false)

            if (axios.isCancel(error)) {
                if (__DEV__) {
                    console.log('HTTP request cancelled', error.message)
                }
            }

            if (__DEV__) {
                console.log(error)
            }
        }
    }

    const onHandleChange = (prop: keyof IState) => (value: string) => {

        if (prop === 'username') {
            setUsernameState(AvailabilityState.UNDETERMINED)
        }

        if (prop === 'player_name') {
            setUsernameState(AvailabilityState.UNDETERMINED)
        }

        if (prop === 'family_name') {
            setUsernameState(AvailabilityState.UNDETERMINED)
        }

        setValues({ ...values, [prop]: value })
        // nameCheckHandler()
    }

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry)
    }

    const renderIcon = (props: any) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const playerIcon = (props: Partial<ImageProps> | undefined, state: AvailabilityState) => {
        switch (state) {
            case AvailabilityState.UNDETERMINED:
                return <Icon {...props} name={'person-outline'} />

            case AvailabilityState.AVAILABLE:
                return <Icon {...props} name={'person-done'} />

            case AvailabilityState.NOT_AVAILABLE:
                return <Icon {...props} name={'person-delete'} />

            case AvailabilityState.CHECKING_AVAILABILITY:
                return <Spinner size='small' />
        }
    }

    const LoadingIndicator = (props: Partial<ImageProps> | undefined) => (
        <View style={[props?.style, styles.indicator]}>
            {isAuthenticating ?
                <Spinner size='small' /> :
                <Icon {...props} name='log-in' />
            }
        </View>
    )

    const navigateBack = () => {
        navigation.goBack();
    }

    const BackAction = () => (
        <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
    )

    return (
        <Layout style={{ flex: 1 }}>
            <Divider />
            <Layout style={styles.container}>
                <Input
                    label='Username'
                    placeholder='Username'
                    status={getInputTrafficStatus(usernameState)}
                    accessoryRight={(e) => playerIcon(e, usernameState)}
                    value={values.username}
                    onChangeText={onHandleChange(NameCheck.USERNAME)}
                />
                <Input
                    style={styles.containerItem}
                    label='Player name'
                    placeholder='Player name'
                    status={getInputTrafficStatus(playerNameState)}
                    accessoryRight={(e) => playerIcon(e, playerNameState)}
                    value={values.player_name}
                    onChangeText={onHandleChange('player_name')}
                />
                <Input
                    style={styles.containerItem}
                    label='Family name'
                    placeholder='Family name'
                    status={getInputTrafficStatus(familyNameState)}
                    accessoryRight={(e) => playerIcon(e, familyNameState)}
                    value={values.family_name}
                    onChangeText={onHandleChange('family_name')}
                />
                <Input
                    style={styles.containerItem}
                    value={values.password}
                    label='Password'
                    placeholder='Password'
                    caption={`Should contain at least ${fieldValidation.passwordMinLength} symbols`}
                    accessoryRight={renderIcon}
                    captionIcon={AlertIcon}
                    secureTextEntry={secureTextEntry}
                    onChangeText={onHandleChange('password')}
                />
                <Button
                    style={styles.button}
                    disabled={!isValid}
                    appearance='outline'
                    accessoryLeft={LoadingIndicator}
                    onPress={handleSubmit}
                >
                    Sign up
                </Button>
            </Layout>
        </Layout>
    )
}