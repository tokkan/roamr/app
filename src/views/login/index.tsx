import { StyleSheet } from "react-native"
import { Icon } from "@ui-kitten/components"

export const styles = StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    containerItem: {
        paddingTop: 20,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        marginTop: 20,
    },
    registerButton: {
        marginTop: 50,
    },
})

export const BackIcon = (props: any) => (
    <Icon {...props} name='arrow-back' />
)

export const AlertIcon = (props: any) => (
    <Icon {...props} name='alert-circle-outline' />
)

export enum LoginPaths {
    LOGIN = '/auth/login',
    SOCIAL_LOGIN = '/auth/social-login',
    REGISTER = '/auth/register',
    CHECK_NAME = '/auth/check-name',
    CHECK_SOCIAL_REGISTER = '/auth/check-social-register',
}

export interface IFieldValidation {
    usernameMinLength: number,
    playerNameMinLength: number,
    passwordMinLength: number,
}

export const initFieldValidation: IFieldValidation = {
    usernameMinLength: 3,
    playerNameMinLength: 3,
    passwordMinLength: 6,
}