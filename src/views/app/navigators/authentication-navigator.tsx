import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { BottomNavigation, BottomNavigationTab } from '@ui-kitten/components'
import { LoginScreen } from '../../login/login/login'
import { SignUpScreen } from '../../login/signup'

const { Navigator, Screen } = createBottomTabNavigator()

const BottomTabBar = ({ navigation, state }: any) => (
    <BottomNavigation
        selectedIndex={state.index}
        onSelect={index => navigation.navigate(state.routeNames[index])}
    >
        <BottomNavigationTab title='Login' />
        {/* <BottomNavigationTab title='Register' /> */}
    </BottomNavigation>
)

const LoginNavigator = () => (
    <Navigator
        tabBar={(props: any) => <BottomTabBar {...props} />}>
        <Screen name='Login' component={LoginScreen} />
        {/* <Screen name='Register' component={SignUpScreen} /> */}
    </Navigator>
)

export const AuthenticationNavigator = () => {
    return (
        <LoginNavigator />
    )
}