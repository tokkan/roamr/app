import React, { useState, useEffect } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { BottomNavigation, BottomNavigationTab, Divider } from '@ui-kitten/components'
import { HomeScreen } from '../../../home/home'
import { SearchScreen } from '../../../search/search'
import { NotificationsScreen } from '../../../user/notifications/notifications'
import { MapNavigator } from '../../../map/map-navigator'
import { UserIcon, HomeIcon, SearchIcon, MapIcon, NotificationsIcon } from '../../../../models'
import { UserScreen } from '../../../user/user/user'
import { NotificationBadge } from '../../../../components/common/badge/notification-badge'
import { AppRoutes } from './../../../../models/common/routes'

const { Navigator, Screen } =
    createBottomTabNavigator<Record<AppRoutes, object | undefined>>()

const BottomTabBar = ({ navigation, state }: any) => {
    const [notificationsCount, setNotificationsCount] = useState(5)
    const [messagesCount, setMessagesCount] = useState(5)

    useEffect(() => {
        // TODO: Listen to store for notifications and messages, if they have been read/opened
        // TODO: dispatch reset of notification count in onPressIn below
        // TODO: To reset messages, the message screen should be opened, and also the conversation
    }, [])

    return (
        <>
            <Divider />
            <BottomNavigation
                selectedIndex={state.index}
                onSelect={index => navigation.navigate(state.routeNames[index])}
            >
                <BottomNavigationTab icon={HomeIcon} />
                <BottomNavigationTab icon={SearchIcon} />
                <BottomNavigationTab icon={MapIcon} />
                <BottomNavigationTab
                    title={props => <NotificationBadge
                        {...props}
                        count={notificationsCount}
                    />}
                    icon={NotificationsIcon}
                    onPressIn={() => setNotificationsCount(0)}
                />
                <BottomNavigationTab
                    title={props => <NotificationBadge
                        {...props}
                        count={messagesCount}
                    />}
                    icon={UserIcon}
                />
            </BottomNavigation>
        </>
    )
}

export const HomeNavigator = () => (
    <Navigator
        tabBar={(props: any) => <BottomTabBar {...props} />}>
        <Screen name={'Home'} component={HomeScreen} />
        <Screen name={'Search'} component={SearchScreen} />
        <Screen name={'Map'} component={MapNavigator} />
        <Screen name={'Notifications'} component={NotificationsScreen} />
        <Screen name={'User'} component={UserScreen} />
        {/* <Screen name={AppRoutes.APP_HOME} component={HomeScreen} />
        <Screen name={AppRoutes.APP_SEARCH} component={SearchScreen} />
        <Screen name={AppRoutes.APP_MAP} component={MapNavigator} />
        <Screen name={AppRoutes.APP_NOTIFICATIONS} component={NotificationsScreen} />
        <Screen name={AppRoutes.APP_USER_ACCOUNT} component={UserScreen} /> */}
    </Navigator>
)