import React from 'react'
import { useDispatch } from 'react-redux'
import { createDrawerNavigator } from '@react-navigation/drawer'
import {
    IAuthActionDispatch
} from '../../../../store/auth'
import { IActionDispatch } from '../../../../store'
import { DrawerContent } from '../drawer/app-drawer'
import { MapScreen } from '../../../map/map/map'
import { SearchScreen } from '../../../search/search'
import { HomeNavigator } from '../bottom-navigator/home-navigator'
import { BusinessNavigator } from '../../../business/business-navigator'
import { BusinessRequestsNavigator } from '../../../business/business-requests-navigator'
import { SettingsNavigator } from '../../../settings/settings-navigator'
import { UserAccountNavigator } from './../../../user/account/account-navigator'
import { AppRoutes } from './../../../../models/common/routes'

const { Navigator, Screen } =
    createDrawerNavigator<Record<AppRoutes, object | undefined>>()

export const AppNavigator: React.FC = () => {
    const dispatch = useDispatch<IActionDispatch<IAuthActionDispatch>>()

    const authorizedScreens = () => {

        // if (false) {
        return (
            <>
                <Screen name={'Business'} component={BusinessNavigator} />
                <Screen name={'Business requests'} component={BusinessRequestsNavigator} />
                {/* <Screen name={AppRoutes.APP_BUSINESS} component={BusinessNavigator} />
                <Screen name={AppRoutes.APP_BUSINESS_REQUESTS} component={BusinessRequestsNavigator} /> */}
            </>
        )
        // } else {
        //     return <>
        //     </>
        // }
    }

    return (
        <Navigator
            drawerContent={props =>
                <DrawerContent
                    dispatch={dispatch}
                    {...props} />
            }>
            <Screen name={'Home'} component={HomeNavigator} />
            <Screen name={'Search'} component={SearchScreen} />
            <Screen name={'Map'} component={MapScreen} />
            <Screen name={'Settings'} component={SettingsNavigator} />
            <Screen name={'User account'} component={UserAccountNavigator} />
            <Screen name={'Business'} component={BusinessNavigator} />
            <Screen name={'Business requests'} component={BusinessRequestsNavigator} />
            {/* <Screen name={AppRoutes.APP_HOME} component={HomeNavigator} />
            <Screen name={AppRoutes.APP_SEARCH} component={SearchScreen} />
            <Screen name={AppRoutes.APP_MAP} component={MapScreen} />
            <Screen name={AppRoutes.APP_SETTINGS} component={SettingsNavigator} />
            <Screen name={AppRoutes.APP_USER_ACCOUNT} component={UserAccountNavigator} /> */}
            {/* {authorizedScreens} */}
        </Navigator>
    )
}