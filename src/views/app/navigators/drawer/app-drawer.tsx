import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import { DrawerActions } from '@react-navigation/native'
import {
    TopNavigation,
    TopNavigationAction,
    Layout,
    Drawer,
    List,
    ListItem,
    Divider,
    Text,
} from '@ui-kitten/components'
import {
    AuthActions, AuthRoles
} from '../../../../store/auth'
import { IRootState } from '../../../../store'
import {
    IDrawerProps, IDrawerItem, IDrawerItemAction, styles, IDrawerItemActionProps, IDrawerItemProps
} from '.'
import { UserIcon, SettingsIcon, BusinessIcon, LogoutIcon, BackIcon } from './../../../../models'
import { useSelector } from 'react-redux'
import { i18n } from './../../../../locales/localization'
import { AppRoutes } from './../../../../models/common/routes'

export const DrawerContent: React.FC<IDrawerProps> = (
    { navigation, state, dispatch }
) => {
    const auth = useSelector((s: IRootState) => s.auth)
    const [screenItems] = useState<IDrawerItem[]>([
        {
            title: 'drawer.tabs.home.title',
            description: 'drawer.tabs.home.sub_title',
            leftAccessory: UserIcon,
            function: () => navigate('Home'),
            // function: () => navigate(AppRoutes.APP_HOME),
            roles: []
        },
        {
            title: 'drawer.tabs.user_account.title',
            description: 'drawer.tabs.user_account.sub_title',
            leftAccessory: UserIcon,
            function: () => navigate('User account'),
            // function: () => navigate(AppRoutes.APP_USER_ACCOUNT),
            roles: []
        },
        {
            title: 'drawer.tabs.business.title',
            description: 'drawer.tabs.business.sub_title',
            leftAccessory: BusinessIcon,
            function: () => navigate('Business'),
            // function: () => navigate(AppRoutes.APP_BUSINESS),
            roles: [
                AuthRoles.AFFILIATE,
                AuthRoles.MOD,
                AuthRoles.ADMIN,
            ]
        },
        {
            title: 'drawer.tabs.business_requests.title',
            description: 'drawer.tabs.business_requests.sub_title',
            leftAccessory: BusinessIcon,
            function: () => navigate('Business requests'),
            // function: () => navigate(AppRoutes.APP_BUSINESS_REQUESTS),
            roles: [
                AuthRoles.AFFILIATE,
                AuthRoles.MOD,
                AuthRoles.ADMIN,
            ]
        },
        {
            title: 'drawer.tabs.municipality.title',
            description: 'drawer.tabs.municipality.sub_title',
            leftAccessory: BusinessIcon,
            function: () => navigate('Municipality'),
            // function: () => navigate(AppRoutes.APP_MUNICIPALITY),
            roles: [
                AuthRoles.AFFILIATE,
                AuthRoles.MOD,
                AuthRoles.ADMIN,
            ]
        },
    ])

    const [items] = useState<IDrawerItemAction[]>([
        {
            title: 'drawer.tabs.logout.title',
            description: 'drawer.tabs.logout.sub_title',
            leftAccessory: LogoutIcon,
            // leftAccessory: <Icon name='person' />
            // rightAccessory: renderItemIcon,
            function: () => onLogout(),
            roles: []
        }
    ])

    const [authorizedItems, setAuthorizedItems] = useState<IDrawerItem[]>([])

    useEffect(() => {
        setAuthorizedItems(screenItems.filter(f => {
            let hasRole = false
            auth.roles.forEach(m => {
                if (f.roles.length <= 0) {
                    hasRole = true
                    return
                } else if (f.roles.includes(m)) {
                    hasRole = true
                    return
                }
            })
            if (hasRole) {
                return f
            }
        }))
    }, [auth])

    const navigate = (screen: AppRoutes) => {
        navigation.navigate(screen)
    }

    const closeDrawer = () => {
        navigation.dispatch(DrawerActions.closeDrawer())
    }

    const renderBackAction = () => (
        <TopNavigationAction
            icon={BackIcon}
            onPress={closeDrawer}
        />
    )

    const renderSettingsAction = () => (
        <TopNavigationAction
            icon={SettingsIcon}
            onPress={() => navigate('Settings')}
            // onPress={() => navigate(AppRoutes.APP_SETTINGS)}
        />
    )

    const onLogout = () => {
        closeDrawer()

        setTimeout(() => {
            dispatch({
                type: AuthActions.LOGOUT,
                payload: null
            })
        }, 200)
    }

    const renderHeader = () => (
        <Layout style={styles.headerContainer}>
            <TopNavigation
                title='Menu'
                accessoryLeft={renderBackAction}
                accessoryRight={renderSettingsAction}
            />
            <Divider />
            <Layout
                style={styles.header}
                level='2'
            >
                <Layout style={styles.drawerHeaderContainer}>
                    <View style={styles.drawerInnerContainer}>
                        <View>
                            {/* <Avatar
                                size='giant'
                                source={require('./../../../assets/Elf-A-01.png')}
                            /> */}
                            <Text>
                                Status
                            </Text>
                        </View>
                    </View>
                </Layout>
                {/* <ImageBackground
                    source={require('./../../../assets/Elf-A-01.png')}
                    style={styles.drawerHeaderImageContainer}
                >
                </ImageBackground> */}
            </Layout>
        </Layout>
    )

    const renderFooter = () => (
        <>
            <Divider />
            <List
                // style={styles.container}
                // style={{
                //     position: 'bottom'
                // }}
                data={items}
                renderItem={renderItemAction}
            />
        </>
    )

    const renderItemAction: React.FC<IDrawerItemActionProps> = ({ item }) => {
        return (
            <>
                {item.roles.length <= 0 ||
                    auth.roles.some(s => item.roles.some(s2 => s2 === s)) ?
                    (
                        <>
                            <ListItem
                                // style={{
                                //     position: 'relative',
                                //     bottom: 10,
                                // }}
                                title={i18n.t(item.title)}
                                description={i18n.t(item.description)}
                                accessoryLeft={item.leftAccessory}
                                accessoryRight={item.rightAccessory}
                                onPress={item.function}
                            />
                            <Divider />
                        </>
                    )
                    : <></>
                }
            </>
        )
    }

    const footerItem = () => {
        return (
            <>
                <ListItem
                    // title={i18n.t(item.title)}
                    // description={i18n.t(item.description)}
                    // accessoryLeft={item.leftAccessory}
                    // accessoryRight={item.rightAccessory}
                    // onPress={item.function}
                    title='drawer.tabs.logout.title'
                    description='drawer.tabs.logout.sub_title'
                    accessoryLeft={LogoutIcon}
                    // leftAccessory: <Icon name='person' />
                    // rightAccessory: renderItemIcon,
                    onPress={() => onLogout}
                />
                <Divider />
            </>
        )
    }

    const renderScreenItem: React.FC<IDrawerItemProps> = ({ item }) => {
        return (
            <>
                {item.roles.length <= 0 ||
                    auth.roles.some(s => item.roles.some(s2 => s2 === s)) ?
                    <>
                        <ListItem
                            title={i18n.t(item.title)}
                            description={i18n.t(item.description)}
                            accessoryLeft={item.leftAccessory}
                            accessoryRight={item.rightAccessory}
                            onPress={item.function}
                        />
                        <Divider />
                    </>
                    : <></>
                }
            </>
        )
    }

    return (
        <Drawer
            header={renderHeader}
            footer={renderFooter}
            // ListFooterComponent={footerItem}
            // style={{
            //     backgroundColor: 'yellow',
            //     height: '100%',
            // }}
            contentContainerStyle={{
                // flex: 1,
                height: '67%',
                flexDirection: 'column',
                justifyContent: 'space-between',
            }}
            selectedIndex={state.index}
        >
            <Divider />
            <List
                // style={styles.container}
                // contentContainerStyle={styles.container}
                // ListFooterComponent={footerItem}
                // data={screenItems}
                data={authorizedItems}
                renderItem={renderScreenItem}
            />
            <Divider />
        </Drawer>
    )
}