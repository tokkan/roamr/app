import React, { useState, useEffect } from 'react'
import {
  useColorScheme, StatusBar, Platform,
} from 'react-native'
import {
  ApplicationProvider,
  IconRegistry,
} from '@ui-kitten/components'
import * as eva from '@eva-design/eva'
import { EvaIconsPack } from '@ui-kitten/eva-icons'
import {
  // Fa5IconsPack, 
  MaterialIconPack, MDICommunityPack
} from './icons'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { SplashScreen } from '../login/splash-screen'
import { NavigationContainer } from '@react-navigation/native'
import { AuthenticationNavigator, AppNavigator } from './navigators'
import RNSplashScreen from 'react-native-splash-screen'
// import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
// import SafeAreaView from 'react-native-safe-area-view'
import changeNavigationBarColor, {
  hideNavigationBar,
  showNavigationBar,
} from 'react-native-navigation-bar-color'

// https://akveo.github.io/react-native-ui-kitten/docs/design-system/eva-dark-theme

export const App: React.FC = () => {

  const auth = useSelector((s: IRootState) => s.auth)
  const app_theme = useSelector((s: IRootState) => s.settings.app_theme)
  const language = useSelector((s: IRootState) => s.settings.language)
  const colorScheme = useColorScheme()
  const [theme, setTheme] = useState(eva.dark)
  const [isLoading] = useState(false)

  useEffect(() => {
    RNSplashScreen.hide()
    setApplicationTheme()
  }, [])

  useEffect(() => {
    setApplicationTheme()
  }, [app_theme])

  useEffect(() => {
  }, [language])

  // useEffect(() => {
  //   console.debug('useeffect')
  //   console.debug(auth.mapbox_access_token)
  //   MapboxGL.setAccessToken(auth.mapbox_access_token)

  //   // if (auth.access_token.length > 0) {
  //   //   MapboxGL.setTelemetryEnabled(false)
  //   //   MapboxGL.setConnected(true)
  //   // }
  // }, [auth.mapbox_access_token])

  const getApplicationTheme = () => {
    if (!app_theme.use_system_theme) {
      if (app_theme.theme === 'dark') {
        return 'dark'
      } else {
        return 'light'
      }
    } else {
      if (colorScheme !== null &&
        colorScheme !== undefined &&
        colorScheme === 'dark') {
          return 'dark'
      } else {
        return 'light'
      }
    }
  }

  const setApplicationTheme = () => {
    if (!app_theme.use_system_theme) {
      if (app_theme.theme === 'dark') {
        setTheme(eva.dark)
        setAndroidNavigationTransparency("translucent", false)
      } else {
        setTheme(eva.light)
        // setAndroidNavigationTransparency("translucent", true)
        setAndroidNavigationColor("#F7F9FC", true)
      }
    } else {
      if (colorScheme !== null &&
        colorScheme !== undefined &&
        colorScheme === 'dark') {
          setTheme(eva.dark)
          setAndroidNavigationTransparency("translucent", false)
        } else {
          setTheme(eva.light)
          // setAndroidNavigationTransparency("translucent", true)
          setAndroidNavigationColor("#F7F9FC", true)
      }
    }
  }

  const setNavigationVisibility = (visibility: 'show' | 'hide') => {
    if (Platform.OS === 'android') {
      if (visibility === 'show') {
        showNavigationBar()
      } else if (visibility === 'hide') {
        hideNavigationBar()
      }
    }
  }

  const setAndroidNavigationTransparency = (
    color: 'translucent' | 'transparent',
    darkIcons: boolean,
  ) => {
    if (Platform.OS === 'android') {
      changeNavigationBarColor(color, darkIcons, true)
    }
  }

  const setAndroidNavigationColor = (
    color: string,
    darkIcons: boolean,
  ) => {
    if (Platform.OS === 'android') {
      changeNavigationBarColor(color, darkIcons, true)
    }
  }

  if (isLoading) {
    return (
      <ApplicationProvider {...eva} theme={theme}>
        <SplashScreen />
      </ApplicationProvider>
    )
  } else {
    return (
      <ApplicationProvider {...eva} theme={theme}>
        <IconRegistry icons={[
          EvaIconsPack,
          // Fa5IconsPack,
          MaterialIconPack,
          MDICommunityPack,
        ]} />
        <SafeAreaProvider>
          <SafeAreaView
            style={{
              flex: 1,
              backgroundColor: getApplicationTheme() === 'dark' ? '#222B45' : '#FFFFFF',
            }}
          >
            <StatusBar
              // translucent={true}
              backgroundColor={getApplicationTheme() === 'dark' ? '#222B45' : '#FFFFFF'}
              barStyle={getApplicationTheme() === 'dark' ? 'light-content' : 'dark-content'}
            />
            <NavigationContainer>
              {!auth.isAuthenticated ?
                <AuthenticationNavigator /> :
                <AppNavigator />
              }
            </NavigationContainer>
          </SafeAreaView>
        </SafeAreaProvider>
      </ApplicationProvider >
    )
  }
}