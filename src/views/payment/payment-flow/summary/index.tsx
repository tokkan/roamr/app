export interface IStateFields {
    creditCardNumber: string
    cvc: number
}