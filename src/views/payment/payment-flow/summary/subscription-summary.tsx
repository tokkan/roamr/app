import React from 'react'
import { Layout, Text } from '@ui-kitten/components'
import { TopNavigationComponent } from '../../../components/top-navigation/top-navigation-stack'

export const SubscriptionSummaryScreen = ({ navigation }: any) => {
    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title={'Summary'}
            />
            <Text>
                Summary
            </Text>
        </Layout>
    )
}