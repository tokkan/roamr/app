import React, { useCallback } from 'react'
import { Text, Layout, Card, List, Button } from '@ui-kitten/components'
import { StyleSheet, View, Linking, Platform, Alert } from 'react-native'

const styles = StyleSheet.create({
    container: {
        // maxHeight: 320,
    },
    contentContainer: {
        paddingHorizontal: 8,
        paddingVertical: 4,
    },
    item: {
        marginVertical: 4,
    },
})

const data = new Array(3).fill({
    title: 'Item',
})

export const SelectPlanScreen = () => {

    const renderItemHeader = (headerProps: any, info: any) => (
        <View {...headerProps}>
            <Text category='h6'>
                {info.item.title} {info.index + 1}
            </Text>
        </View>
    );

    const renderItemFooter = (footerProps: any) => (
        <Text {...footerProps}>
            By Wikipedia
        </Text>
    );

    const renderItem = (info: any) => (
        <Card
            style={styles.item}
            status='basic'
            header={headerProps => renderItemHeader(headerProps, info)}
            footer={renderItemFooter}>
            <Text>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged.
            </Text>
        </Card>
    )

    let url = ''
    // let ref = useRef<RefObject<WebView> | null | undefined>()

    if (Platform.OS === 'android') {
        // url = 'http://192.168.1.163:3000'
        url = 'https://192.168.1.163:3000'
    } else if (Platform.OS === 'ios') {
        url = 'http://192.168.1.67:3000'
    }

    // const openWebLink = async () => {
    //     await Linking.openURL(`${url}/payment-subscription`)
    // }

    const openWebLink = useCallback(async () => {
        // Checking if the link is supported for links with custom URL scheme.
        const supported = await Linking.canOpenURL(url)

        if (supported) {
            // Opening the link with some app, if the URL scheme is "http" the web link should be opened
            // by some browser in the mobile
            await Linking.openURL(`${url}/payment-subscription`)
        } else {
            Alert.alert(`Don't know how to open this URL: ${url}`)
        }
    }, [url])

    return (
        <Layout style={{ flex: 1 }}>
            <Text>
                Select Plan
            </Text>
            <Button onPress={openWebLink}>
                Open browser
            </Button>
            <List
                style={styles.container}
                contentContainerStyle={styles.contentContainer}
                data={data}
                renderItem={renderItem}
            />
        </Layout>
    )
}