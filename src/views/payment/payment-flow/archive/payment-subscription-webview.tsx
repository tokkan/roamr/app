import React, { useRef, RefObject } from 'react'
import { Layout, Text } from '@ui-kitten/components'
import { WebView } from 'react-native-webview'
import { Platform } from 'react-native'

export const PaymentSubscriptionScreen = () => {

    let url = ''
    let ref = useRef<RefObject<WebView> | null | undefined>()

    if (Platform.OS === 'android') {
        url = 'http://192.168.1.163:3000'
    } else if (Platform.OS === 'ios') {
        url = 'http://192.168.1.67:3000'
    }

    return (
        <Layout style={{ flex: 1 }}>
            <Text>
                Payment subscription view
            </Text>
            <WebView
                ref={ref.current}
                originWhitelist={['*']}
                source={{ uri: `${url}/payment-subscription` }}
                // source={{ uri: 'https://material-ui.com/' }}
                javaScriptEnabled={true}
                onLoad={(e) => {
                    console.log('loaded')
                    console.log(e)
                    if (ref !== null && ref !== undefined &&
                        ref.current !== null && ref.current !== undefined) {
                        ref.current.current?.reload()
                    }
                }}
                onError={() => {
                    console.log('error')
                }}
                onHttpError={() => {
                    console.log('http error')
                }}
            />
        </Layout>
    )
}