import { PaymentSubscriptionRoutes } from "./../../../models/common/routes"

export type RootStackPaymentSubscriptionRoutesParamList = {
    [PaymentSubscriptionRoutes.SELECT_PLAN]: undefined
    [PaymentSubscriptionRoutes.CHOOSE_PLAN_LENGTH]: {
        latitude: number
        longitude: number
        municipality: {
            municipality_name: string
            municipality_code: string
        }
    }
    [PaymentSubscriptionRoutes.SUMMARY]: {
        business_id: string
    }
    // Feed: { sort: 'latest' | 'top' } | undefined
}