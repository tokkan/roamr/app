import React, { useEffect, useState } from 'react'
import { Layout, Text, Menu, MenuGroup, MenuItem, IndexPath, Divider, Button, Spinner } from '@ui-kitten/components'
import axios from 'axios'
import { TouchableOpacity, ImageProps } from 'react-native'
import { TopNavigationComponent } from '../../../../components/top-navigation/top-navigation-stack'
import { styles } from '.'
import { InfoIcon } from '../../../../models'
import { InformationModal } from '../../../../components/modal/common/info/info-modal'

interface IProduct {
    id: string
    name: string
    description: string
    images: []
}

interface IPriceRecurring {
    aggregate_usage: any
    interval: string
    interval_count: number
    trial_period_days: number | null
    usage_type: string
}

interface IPrice {
    id: string
    product: string
    currency: string
    unit_amount: number
    unit_amount_decimal: string
    recurring: IPriceRecurring
    tiers_mode: string | null
    type: string
    metadata: any
}

interface IPlan {
    product: IProduct
    prices: IPrice[]
}

interface IPlainPlan {
    product_name: string
    description: string
    prices: string[]
}

interface ISelectedPlan {
    product: IProduct
    price: IPrice
}

interface IInfoAction {
    plan: IPlan
    props: Partial<ImageProps> | undefined
}

/**
 * Calculating the price amount by diving by 100, since the price returned
 * is in öre, which is a hundrenth of 1 SEK
 * @param param0 
 */
export const SelectPlanScreen = ({ navigation }: any) => {
    const [selectedIndex, setSelectedIndex] =
        React.useState(new IndexPath(0))
    const [plans, setPlans] = useState<IPlan[]>([])
    const [selectedItem, setSelectedItem] = useState<ISelectedPlan>()
    const [infoSelectedItem, setInfoSelectedItem] = useState<IPlainPlan>()
    const [isLoading, setIsLoading] = useState(false)
    const [isModalVisible, setModalVisible] = useState(false)

    console.log(selectedIndex)

    useEffect(() => {
        getProducts()
    }, [])

    const onSelectIndex = (index: IndexPath) => {
        setSelectedIndex(index)

        if (index.section !== undefined) {
            let product = plans[index.section].product
            let price = plans[index.section].prices[index.row]

            setSelectedItem({
                product,
                price
            })
        }
    }

    const getProducts = async () => {
        try {
            setIsLoading(true)
            let res =
                await axios.get<IPlan[]>('/products/get-subscription-plans')
            setPlans(res.data)
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.error('Error fetching subscription plans')
        }
    }

    const continueToSummary = () => {
    }

    const getSelectedName = (item: ISelectedPlan | undefined) => {
        if (item === undefined) {
            return `No plan has been selected`
        } else {
            return `Selected plan: ${selectedItem?.product.name}`
        }
    }

    /**
     * Calculating the price by dividing by 100, since the price
     * returned from the API is in öre when the currency is in SEK
     * @param item 
     */
    const getPriceName = (item: IPrice) => {
        return `${item.unit_amount / 100} ${item.currency} / ${item.recurring.interval_count} ${item.recurring.interval}(s)`
    }

    const getPricesForPlan = (plan: IPlan) => {
        console.log(getPricesForPlan.name)
        console.log(plan)
        setInfoSelectedItem({
            product_name: plan.product.name,
            description: plan.product.description,
            prices: plan.prices.map(m => {
                return getPriceName(m)
            })
        })
        setModalVisible(true)
    }

    const InfoAction: React.FC<IInfoAction> = ({
        props,
        plan,
    }) => {
        return (
            <TouchableOpacity onPress={() => getPricesForPlan(plan)}>
                <InfoIcon {...props} />
            </TouchableOpacity>
        )
    }

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title={'Select plan'}
            />
            <Layout style={styles.title}>
                <Text
                    category='h6'
                >
                    {getSelectedName(selectedItem)}
                </Text>
                {selectedItem !== undefined &&
                    <Text appearance='hint'>
                        {getPriceName(selectedItem.price)}
                    </Text>
                }
            </Layout>
            <Divider />
            {isLoading ? <Spinner /> :
                <Menu
                    selectedIndex={selectedIndex}
                    // onSelect={index => setSelectedIndex(index)}
                    onSelect={index => onSelectIndex(index)}
                >
                    {plans.map(m => (
                        <MenuGroup
                            key={m.product.id}
                            title={m.product.name}
                            accessoryLeft={props =>
                                <InfoAction props={props} plan={m} />}
                        >
                            {m.prices.map(m2 => (
                                <MenuItem
                                    key={m2.id}
                                    title={getPriceName(m2)} />
                            ))}
                        </MenuGroup>
                    ))}
                </Menu >
            }
            <Divider />
            <Button
                appearance='outline'
                onPress={continueToSummary}
            >
                Continue
            </Button>
            {infoSelectedItem !== undefined &&
                <InformationModal
                    visible={isModalVisible}
                    setVisible={setModalVisible}
                    title={infoSelectedItem.product_name}
                    description={infoSelectedItem.description}
                    list={infoSelectedItem.prices}
                />
            }
        </Layout>
    )
}