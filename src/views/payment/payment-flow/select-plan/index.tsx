import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    title: {
        minHeight: 43,
        marginLeft: 15,
        marginTop: 10,
        marginBottom: 10,
    }
})