import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { RootStackPaymentSubscriptionRoutesParamList } from '.'
import { SubscriptionSummaryScreen } from './summary/subscription-summary'
import { SelectPlanScreen } from './select-plan/select-plan'
import { PaymentSubscriptionRoutes } from './../../../models/common/routes'

const Stack = createStackNavigator<RootStackPaymentSubscriptionRoutesParamList>()

export const PaymentSubscriptionNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={PaymentSubscriptionRoutes.SELECT_PLAN}
            headerMode='none'
        >
            <Stack.Screen
                name={PaymentSubscriptionRoutes.SELECT_PLAN}
                component={SelectPlanScreen}
            />
            <Stack.Screen
                name={PaymentSubscriptionRoutes.SUMMARY}
                component={SubscriptionSummaryScreen}
            />
        </Stack.Navigator>
    )
}