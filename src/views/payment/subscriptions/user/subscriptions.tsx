import React from 'react'
import { Layout, Text } from '@ui-kitten/components'
import { StackNavigationProp } from '@react-navigation/stack'
import {
    RootStackUserAccountRoutesParamList
} from './../../../user/account/account-navigator'
import { UserAccountRoutes } from './../../../../models/common/routes'

type ScreenNavigationProp = StackNavigationProp<
    RootStackUserAccountRoutesParamList,
    UserAccountRoutes.SUBSCRIPTIONS
>

interface IProps {
    navigation: ScreenNavigationProp
}

export const UserSubscriptionsScreen: React.FC<IProps> = ({
    navigation,
}) => {
    return (
        <Layout
            style={{
                flex: 1,
            }}
        >
            <Text>
                User subscriptions
            </Text>
        </Layout>
    )
}