import React from 'react'
import { StyleSheet, ImageProps } from "react-native"
import { Icon } from "@ui-kitten/components"

export const styles = StyleSheet.create({
    layout: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 10,
        paddingTop: 10,
        paddingRight: 10,
    },
    containerItem: {
        paddingTop: 20,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        marginTop: 10,
    },
})

export const AlertIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='alert-circle-outline' />
    )

export enum LocationTypes {
    ALL = 'all',
    STABLE = 'stable',
    SHOP = 'shop',
}

export interface IStateFields {
    search: string
    location_type: LocationTypes
}

export interface IState extends IStateFields {
}

export enum LoginPaths {
    LOGIN = '/auth/login',
    REGISTER = '/auth/register',
    CHECK_NAME = '/auth/check-name',
}

export const initFieldsState: IState = {
    search: '',
    location_type: LocationTypes.ALL,
}

export interface IFieldValidation {
    usernameMinLength: number,
    playerNameMinLength: number,
    passwordMinLength: number,
}

export const initFieldValidation: IFieldValidation = {
    usernameMinLength: 3,
    playerNameMinLength: 3,
    passwordMinLength: 6,
}

export interface IRenderBusinessItem {
    item: any,
    index: number
}