import React, { useState, useEffect } from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'
import { Text, Layout, Icon, Spinner, Input, Button, Select, IndexPath, SelectItem, SelectGroup, Divider, List, Card } from '@ui-kitten/components'
import axios from 'axios'
import { initFieldValidation, styles, IStateFields, IState, initFieldsState, IRenderBusinessItem } from '.'
import { TopNavigationComponent } from './../../components/top-navigation/top-navigation'

const selectInit: IndexPath[] = [
    // new IndexPath(0, 0),
    // new IndexPath(1),
]

interface IBusinessTypeGroup {
    [key: string]: string[]
}

const businessTypeGroups: IBusinessTypeGroup = {
    "business": [
        'stable',
        'shop',
    ]
}

export const SearchScreen = ({ navigation }: any) => {

    const [values, setValues] = useState<IStateFields>(initFieldsState)
    const [isRequsting, setIsRequesting] = useState(false)
    const [businessData, setBusinessData] = useState([])
    const [selectedBusinessTypeIndex, setSelectedBusinessTypeIndex] =
        useState(selectInit)
    const [businessTypeOptions] = useState(businessTypeGroups)
    const [groupDisplayValues, setGroupDisplayValues] = useState<string[]>([])
    const [secureTextEntry, setSecureTextEntry] = useState(true)
    const [isAuthenticating, setIsAuthenticating] = useState(false)
    const [fieldValidation] = useState(initFieldValidation)
    const [source, setSource] = useState(axios.CancelToken.source())

    useEffect(() => {
        const focus = navigation.addListener('focus', () => {
            setSource(axios.CancelToken.source())
        })

        const unsubscribe = navigation.addListener('blur', () => {
            source.cancel()
        })

        return () => {
            focus
            unsubscribe
            source.cancel()
        }
    }, [navigation])

    useEffect(() => {
        if (selectedBusinessTypeIndex.length > 0) {
            let grp = selectedBusinessTypeIndex.map(index => {
                if (index.section !== undefined) {
                    let grpTitle = Object.keys(businessTypeGroups)[index.section]
                    let g = businessTypeGroups[grpTitle][index.row]
                    return capitalize(g)
                } else {
                    return ''
                }
            })

            if (grp !== undefined) {
                setGroupDisplayValues(grp)
            } else {
                setGroupDisplayValues([''])
            }
        } else {
            setGroupDisplayValues([])
        }
    }, [selectedBusinessTypeIndex])

    const onHandleChange = (prop: keyof IState) => (value: string) => {
        setValues({ ...values, [prop]: value })
    }

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry)
    }

    const capitalize = ([first, ...rest]: string) => {
        return [first.toLocaleUpperCase(), ...rest].join('')
    }

    const setBusinessTypeSelect = (index: any) => {
        setSelectedBusinessTypeIndex(index)
    }

    const onSearch = async (values: IStateFields) => {
        try {
            setIsRequesting(true)

            let response =
                await axios.get(`/organizations/business`,
                    {
                        cancelToken: source.token
                    })

            if (response) {
                console.log(response.data)
                setIsRequesting(false)
                setBusinessData(response.data)
            } else {
                setIsRequesting(false)
                source.cancel('Operation cancelled by failing getting response')
            }

        } catch (error) {
            setIsRequesting(false)

            if (axios.isCancel(error)) {
                if (__DEV__) {
                    console.log('HTTP request cancelled', error.message)
                }
            }

            if (__DEV__) {
                console.log(error)
            }
        }
    }

    const eyeIcon = (props: any) => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const LoadingIndicator = (props: any) => (
        <View style={[props.style, styles.indicator]}>
            {isRequsting ?
                <Spinner size='small' /> :
                <Icon {...props} name='search' />
            }
        </View>
    )

    const renderBusinessItem = ({ item, index }: IRenderBusinessItem) => (
        <Card>
            <Text>
                {`${item.name}`}
            </Text>
            <Text>
                {/* {`${item.business.name}`} */}
                Business name
            </Text>
        </Card>
    )

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title='Search'
            />
            <Layout style={styles.layout}>
                <Layout style={{ minWidth: '100%', marginBottom: 10 }}>
                    <Select
                        label='Filter business types'
                        multiSelect={true}
                        value={groupDisplayValues.join(', ')}
                        selectedIndex={selectedBusinessTypeIndex}
                        onSelect={(index: IndexPath | IndexPath[]) => setBusinessTypeSelect(index)}
                    >
                        {Object.keys(businessTypeGroups).map((m, i) => (
                            <SelectGroup key={i} title={capitalize(m)}>
                                {businessTypeGroups[m].map((m2, i2) => (
                                    <SelectItem key={i2} title={capitalize(m2)} />
                                ))}
                            </SelectGroup>
                        ))}
                    </Select>
                </Layout>
                <Input
                    label='Search'
                    placeholder='Search'
                    value={values.search}
                    onChangeText={onHandleChange('search')}
                />
                <Button
                    style={styles.button}
                    appearance='outline'
                    accessoryLeft={LoadingIndicator}
                    onPress={() => onSearch(values)}
                >
                    Search
                </Button>
                <Divider />
                <List
                    style={{ width: '100%', marginTop: 10 }}
                    data={businessData}
                    renderItem={renderBusinessItem}
                    ItemSeparatorComponent={Divider}
                />
            </Layout>
        </Layout>
    )
}