import React, { useEffect, useState } from 'react'
import {
    Layout, Text, Select, SelectItem, IndexPath
} from "@ui-kitten/components"
import { i18n } from './../../locales/localization'
import MapboxGL from '@react-native-mapbox-gl/maps'
import {
    useSelector, useDispatch
} from 'react-redux'
import {
    IRootState, IActionDispatch
} from './../../store'
import {
    ISettingsActionDispatch, SettingsActions
} from './../../store/settings/actions'

export const MapOptions = () => {
    const map_theme = useSelector((s: IRootState) => s.settings.map_theme)
    const dispatch = useDispatch<IActionDispatch<ISettingsActionDispatch>>()
    const [options] = useState(Object.entries(MapboxGL.StyleURL))
    const [selectedIndex, setSelectedIndex] =
        React.useState(new IndexPath(0, 0))

    useEffect(() => {
        let index = options.findIndex(f => f[1] === map_theme)
        if (index <= 0) {
            index = 0
        }
        setSelectedIndex(new IndexPath(index, 0))
    }, [map_theme])

    const onSetIndex = (index: any) => {
        setSelectedIndex(index)
        setMapIndex(index)
    }

    const setMapIndex = (index: any) => {
        dispatch({
            type: SettingsActions.CHANGE_MAP_THEME,
            payload: {
                map_theme: options[index.row][1]
            }
        })
    }

    return (
        <Layout style={{
            flexDirection: 'column',
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 20,
            paddingRight: 20,
            alignItems: 'flex-start',
            minWidth: '100%',
        }}>
            <Text style={{
                paddingBottom: 10,
            }}>
                {i18n.t('settings.map_theme')}
            </Text>
            <Select
                style={{ minWidth: '100%' }}
                value={options[selectedIndex.row][0]}
                selectedIndex={selectedIndex}
                onSelect={index => onSetIndex(index)}
            >
                {options.map((m, i) => (
                    <SelectItem key={i} title={m[0]} />
                ))}
            </Select>
        </Layout>
    )
}