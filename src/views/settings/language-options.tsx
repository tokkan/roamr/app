import React, { useState, useEffect } from 'react'
import {
    Layout, Text, Select, SelectItem, IndexPath
} from "@ui-kitten/components"
import { useSelector, useDispatch } from 'react-redux'
import { i18n } from './../../locales/localization'
import {
    LanguageType, LanguageTypes, ISettingsActionDispatch, SettingsActions
} from "./../../store/settings/actions"
import { IRootState, IActionDispatch } from './../../store'

interface ILanguageMap {
    [key: string]: LanguageType
}

const languageMap: ILanguageMap = {
    English: LanguageTypes.EN,
    Svenska: LanguageTypes.SE,
}

export const LanguageOptions = () => {

    const language = useSelector((s: IRootState) => s.settings.language)
    const dispatch = useDispatch<IActionDispatch<ISettingsActionDispatch>>()
    const [selectedIndex, setSelectedIndex] =
        React.useState(new IndexPath(0, 0))
    const [options] = useState(Object.entries(languageMap))

    useEffect(() => {
        let index = options.findIndex(f => f[1] === language)
        if (index <= 0) {
            index = 0
        }
        setSelectedIndex(new IndexPath(index, 0))
    }, [language])

    const onSetIndex = (index: any) => {
        setSelectedIndex(index)
        setLanguageIndex(index)
    }

    const setLanguageIndex = (index: any) => {
        dispatch({
            type: SettingsActions.CHANGE_LANGUAGE,
            payload: {
                language: options[index.row][1]
            }
        })
    }

    return (
        <Layout style={{
            flexDirection: 'column',
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 20,
            paddingRight: 20,
            alignItems: 'flex-start',
            minWidth: '100%',
        }}>
            <Text style={{
                paddingBottom: 10,
            }}>
                {i18n.t('settings.language')}
            </Text>
            <Select
                style={{ minWidth: '100%' }}
                value={options[selectedIndex.row][0]}
                selectedIndex={selectedIndex}
                onSelect={index => onSetIndex(index)}
            >
                {options.map((m, i) => (
                    <SelectItem key={i} title={m[0]} />
                ))}
            </Select>
        </Layout>
    )
}