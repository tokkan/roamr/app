import React, { useState, useEffect } from 'react'
import { Layout, Divider } from '@ui-kitten/components'
import { TopNavigationComponent } from './../../components/top-navigation/top-navigation-stack'
import axios from 'axios'
import { i18n } from './../../locales/localization'
import { LanguageOptions } from './language-options'
import { MapOptions } from './map-options'
import { ThemeOptions } from './theme-options'
import { IRootState } from './../../store'
import { useSelector } from 'react-redux'

export const SettingsScreen = ({ navigation }: any) => {

    const [source, setSource] = useState(axios.CancelToken.source())
    const language = useSelector((s: IRootState) => s.settings.language)

    useEffect(() => {

        const focus = navigation.addListener('focus', () => {
            setSource(axios.CancelToken.source())
        })

        const unsubscribe = navigation.addListener('blur', () => {
            source.cancel()
        })

        return () => {
            focus
            unsubscribe
            source.cancel()
        }
    }, [navigation])

    useEffect(() => {
    }, [language])

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title={i18n.t('settings.settings_title')}
            />
            <Layout style={{ flex: 1 }}>
                <ThemeOptions />
                <Divider />
                {/* <MapOptions />
                <Divider /> */}
                <LanguageOptions />
                <Divider />
            </Layout>
        </Layout>
    )
}