import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { SettingsScreen } from './settings'
import { SettingsRoutes } from './../../models/common/routes'

const Stack = createStackNavigator()

export const SettingsNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={SettingsRoutes.SETTINGS}
            headerMode='none'
        >
            <Stack.Screen
                name={SettingsRoutes.SETTINGS}
                component={SettingsScreen}
            />
        </Stack.Navigator>
    )
}