import React, { useState, useEffect } from 'react'
import {
    Layout, Text, Toggle, Divider, Icon
} from "@ui-kitten/components"
import { useSelector, useDispatch } from 'react-redux'
import { i18n } from './../../locales/localization'
import {
    ISettingsActionDispatch, SettingsActions, AppThemes
} from "./../../store/settings/actions"
import { IRootState, IActionDispatch } from './../../store'
import { ImageProps } from 'react-native'
import { styles } from '.'

const LightIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} style={styles.icon} name='moon-outline' fill='blue' />
    )

const DarkIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} style={styles.icon} name='moon' fill='yellow' />
    )

export const ThemeOptions = () => {

    const app_theme = useSelector((s: IRootState) => s.settings.app_theme)
    const dispatch = useDispatch<IActionDispatch<ISettingsActionDispatch>>()
    const [isDarkMode, setIsDarkMode] = useState(true)
    const [isDefaultTheme, setIsDefaultTheme] = useState(true)

    useEffect(() => {
        app_theme.theme === AppThemes.DARK ?
            setIsDarkMode(true) :
            setIsDarkMode(false)

        app_theme.use_system_theme === true ?
            setIsDefaultTheme(true) :
            setIsDefaultTheme(false)
    }, [app_theme])

    const setDarkMode = (value: boolean) => {
        setIsDarkMode(value)
        let theme = value ? AppThemes.DARK : AppThemes.LIGHT
        dispatch({
            type: SettingsActions.CHANGE_APP_THEME,
            payload: {
                theme: theme,
            }
        })
    }

    const setDefaultTheme = (value: boolean) => {
        setIsDefaultTheme(value)
        dispatch({
            type: SettingsActions.SET_DEFAULT_APP_THEME,
            payload: {
                use_system_theme: value
            }
        })
    }

    return (
        <>
            <Layout style={{ paddingTop: 20, paddingBottom: 10 }}>
                <Text category='h6' style={{ marginLeft: 20 }}>
                    {i18n.t('settings.theme_title')}
                </Text>
            </Layout>
            <Divider />
            <Layout style={{
                flexDirection: 'row',
                paddingTop: 20,
                paddingBottom: 10,
                alignItems: 'center',
            }}>
                <Toggle
                    style={{ marginLeft: 20, marginRight: 10 }}
                    checked={isDarkMode}
                    onChange={(value) => setDarkMode(value)}
                />
                {isDarkMode ? <DarkIcon /> : <LightIcon />}
            </Layout>
            <Divider />
            <Layout style={{
                flexDirection: 'row',
                paddingTop: 10,
                paddingBottom: 10,
                alignItems: 'center'
            }}>
                <Toggle
                    style={{ marginLeft: 20, marginRight: 10 }}
                    checked={isDefaultTheme}
                    onChange={(value) => setDefaultTheme(value)}
                />
                <Text>
                    {i18n.t('settings.system_theme')}
                </Text>
            </Layout>
        </>
    )
}