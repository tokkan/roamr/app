import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { MapScreen } from './map/map'
import { PinDetailsScreen } from './pin-details/pin-details'
import { RootStackMapRoutesParamList } from '.'
import { CreateBusinessScreen } from '../business/create/create-business'
import { MapRoutes } from './../../models/common/routes'

const Stack = createStackNavigator<RootStackMapRoutesParamList>()

export const MapNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={'Map'}
            // initialRouteName={MapRoutes.MAP}
            headerMode='none'
        >
            <Stack.Screen
                name={'Map'}
                // name={MapRoutes.MAP}
                component={MapScreen}
            />
            <Stack.Screen
                name={'Pin details'}
                // name={MapRoutes.PIN_DETAILS}
                component={PinDetailsScreen}
            />
            <Stack.Screen
                name={'Create business'}
                // name={MapRoutes.CREATE_BUSINESS}
                component={CreateBusinessScreen}
            />
        </Stack.Navigator>
    )
}