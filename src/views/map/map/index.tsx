export enum LayerIds {
    PINS = 'roamr-pins',
    trailS = 'roamr-trails',
    AREAS = 'roamr-areas',
    ZONES = 'roamr-zones',
    MUNICIPALITIES = 'roamr-municipalities',
}

export interface IMapClickData {
    geometry: {
        coordinates: number[],
        type: string,
    },
    properties: {
        screenPointX: number,
        screenPointY: number,
    },
    type: string,
}

export interface IRenderedFeature {
    id: string
    type: string
    geometry: {
        coordinates: number[] | number[][] | number[][][]
    }
    properties: any
}

export interface IRenderedFeaturesAtPoint {
    features: IRenderedFeature[]
}

export interface IPreviewPin {
    id: string
    info: {
        title: string
        sub_title: string
        description: string
    }
}