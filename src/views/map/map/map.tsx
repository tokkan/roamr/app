import React, { useEffect, useState, useRef, useCallback } from 'react'
import {
    View, PermissionsAndroid, Platform, ImageSourcePropType, Easing, StyleProp, ViewStyle
} from 'react-native'
import Geolocation,
{
    GeolocationResponse, GeolocationOptions
} from '@react-native-community/geolocation'
// import Geolocation,
// { GeoPosition } from 'react-native-geolocation-service'
import axios from 'axios'
import MapboxGL, { Expression } from '@react-native-mapbox-gl/maps'
import {
    TopNavigationComponent
} from '../../../components/top-navigation/top-navigation'
import {
    LayerIds, IRenderedFeaturesAtPoint, IMapClickData, IPreviewPin
} from '.'
import { useSelector, useDispatch } from 'react-redux'
import { IRootState, IActionDispatch } from '../../../store'
import {
    IPinLocation, IFocusPinData
} from '..'
import { PinModal } from './modals/map-pin-modal'
import { SearchModal } from './modals/search/search-modal'
import { SearchIcon, GetComponentIcon, UserIcon, CrosshairIcon, PinTypesEnum, FilterIcon, GridIcon, MapIcon, InfoIcon } from './../../../models'
import { MapContextMenu } from './context-menu/context-menu'
import { useFocusEffect } from '@react-navigation/native'
import {
    IAuthActionDispatch, AuthActions, ICurrentMunicipality, ICurrentCountry
} from './../../../store/auth'
import { InformationModal } from './../../../components/modal/common/info/info-modal'
import { styles } from './styles/styles'
import { MapSymbolLayers } from './layers/symbol/symbol-layers'
import { imageMap } from './styles/icon-map'
import { SpeedDialAbsolute } from './../../../components/speed-dial/speed-dial'
import { askForLocationPermission } from './../../../utils/geolocation/geolocation'
import { GroupedSpeedDialAbsolute } from './../../../components/speed-dial/grouped-speed-dial'
import { i18n } from './../../../locales/localization'
import { MapRoutes } from './../../../models/common/routes'

// // @ts-ignore
// import hikingIcon from './../../../icons/hiking.png'

const style: StyleProp<ViewStyle> = {
    height: 50,
    width: 50,
    borderRadius: 200,
    // backgroundColor: 'green',
    backgroundColor: 'rgba(50, 205, 50, 0.9)',
    position: 'absolute',
}

export const MapScreen = ({ navigation }: any) => {
    const auth = useSelector((s: IRootState) => s.auth)
    const dispatch = useDispatch<IActionDispatch<IAuthActionDispatch>>()
    const mapRef = useRef<MapboxGL.MapView>(null)
    const cameraRef = useRef<MapboxGL.Camera>(null)
    const [source, setSource] = useState(axios.CancelToken.source())
    const [isMapLoading, setMapLoading] = useState(true)
    // const map_theme = useSelector((s: IRootState) => s.settings.map_theme)
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [isSearchModalVisible, setIsSearchModalVisible] = useState(false)
    const [isContextMenuVisible, setIsContextMenuVisible] = useState(false)
    const [isSearching, setIsSearching] = useState(false)
    const [modalItem, setModalItem] = useState<IPreviewPin>()
    const [clickPosition, setClickPosition] = useState<{
        latitude: number,
        longitude: number,
        screenPointX: number,
        screenPointY: number,
    }>({ latitude: 0, longitude: 0, screenPointX: 0, screenPointY: 0 })
    const [currentClickMunicipality, setCurrentClickMunicipality] =
        useState<ICurrentMunicipality>({
            municipality_fire_id: '',
            municipality_mongo_id: '',
            municipality_name: '',
            municipality_code: '',
        })
    const [currentClickCountry, setCurrentClickCountry] =
        useState<ICurrentCountry>({
            country: 'sweden',
        })
    const [devicePosition, setDevicePosition] =
        useState<GeolocationResponse>()
    const [userPosition, setUserPosition] =
        useState<GeolocationResponse>({
            coords: {
                accuracy: 0,
                altitude: 0,
                altitudeAccuracy: 0,
                heading: 0,
                latitude: 0,
                longitude: 0,
                speed: 0,
            },
            timestamp: 0,
        })
    // const [devicePosition, setDevicePosition] =
    //     useState<GeoPosition>()
    // const [userPosition, setUserPosition] =
    //     useState<GeoPosition>({
    //         coords: {
    //             accuracy: 0,
    //             altitude: 0,
    //             altitudeAccuracy: 0,
    //             heading: 0,
    //             latitude: 0,
    //             longitude: 0,
    //             speed: 0,
    //         },
    //         timestamp: 0,
    //     })
    const [map_themes] = useState([
        'mapbox://styles/glenolof/ckai75i3y0n6t1io1cqlx59j5',
        MapboxGL.StyleURL.Dark.toString(),
    ])
    const [mapStylesURL, setMapStyelsURL] =
        useState('mapbox://styles/glenolof/ckai75i3y0n6t1io1cqlx59j5')
    const [pinLocations, setPinLocations] = useState<IPinLocation[]>([])
    const [layerIds, setLayerIds] = useState<string[]>([
        'roamr-pins',
        'roamr-trails',
        'roamr-areas',
        'roamr-zones',
        'roamr-municipalities',
    ])
    const [images, setImages] = useState<{
        [key: string]: ImageSourcePropType
        // assets?: string[] | undefined
    }>(imageMap)
    const [filteredPins, setFilteredPins] =
        useState<string[]>([])
    const [infoModalVisible, setInfoModalVisible] = useState(false)
    const [infoModalData, setInfoModalData] = useState({
        title: i18n.t('map.help.title'),
        description: i18n.t('map.help.description'),
        list: [
            i18n.t('map.help.on_press_info'),
            i18n.t('map.help.on_long_press_info'),
            i18n.t('map.help.speed_dial_info'),
        ],
    })

    const [speedDialActions, setSpeedDialActions] = useState([
        {
            style: style,
            icon: "a1-1",
            group_index: 1,
            level: 1,
            accessoryLeft: SearchIcon,
            openGroup: null,
            onPress: () => onNavigate('Search'),
            isAction: false
        },
        {
            style: style,
            icon: "a1-1",
            group_index: 1,
            level: 1,
            accessoryLeft: FilterIcon,
            openGroup: 2,
            onPress: () => console.log('group hey1'),
            isAction: false
        },
        {
            style: style,
            icon: "a1-1",
            group_index: 2,
            level: 1,
            accessoryLeft: FilterIcon,
            openGroup: null,
            onPress: () => console.log('group hey2'),
            isAction: false,
            text: 'Filter action',
        },
        {
            style: style,
            icon: "a1-1",
            group_index: 2,
            level: 1,
            accessoryLeft: FilterIcon,
            openGroup: null,
            onPress: () => console.log('group hey2'),
            isAction: false
        },
        {
            style: style,
            icon: "a1-1",
            group_index: 2,
            level: 1,
            accessoryLeft: FilterIcon,
            openGroup: null,
            onPress: () => console.log('group hey2'),
            isAction: false
        },
        {
            style: style,
            icon: "a1-1",
            group_index: 1,
            level: 1,
            accessoryLeft: CrosshairIcon,
            openGroup: null,
            onPress: () => moveToUserLocation(),
            isAction: false
        },
        {
            style: style,
            icon: "a1-1",
            group_index: 1,
            level: 1,
            accessoryLeft: InfoIcon,
            openGroup: null,
            onPress: () => setInfoModalVisible(true),
            isAction: false
        },
    ])

    useEffect(() => {
        MapboxGL.setAccessToken(auth.mapbox_access_token)

        // if (auth.access_token.length > 0) {
        //     MapboxGL.setTelemetryEnabled(false)
        //     MapboxGL.setConnected(true)
        // }
    }, [auth.mapbox_access_token])

    useFocusEffect(
        useCallback(() => {
            try {
                console.log('usefocusfx')
                let granted = askForLocationPermission()

                if (!granted) {
                    return
                }

                const geoLocationOptions: GeolocationOptions = {
                    timeout: 10000,
                    enableHighAccuracy: true,
                    maximumAge: undefined,
                }

                setTimeout(() => {
                    Geolocation.getCurrentPosition(
                        (pos) => {
                            setDevicePosition(pos)
                            setUserPosition(pos)
                            cameraRef.current?.moveTo([
                                pos.coords.longitude,
                                pos.coords.latitude
                            ], 500)
                        },
                        (err) => {
                            console.debug('Failed to getCurrentPosition')
                            console.debug(err)
                        }, geoLocationOptions
                    )
                }, 2000)
            } catch (error) {
                console.debug(error)
            }
        }, [])
    )

    // useEffect(() => {

    //     const focus = navigation.addListener('focus', () => {
    //         setSource(axios.CancelToken.source())
    //         Geolocation.getCurrentPosition((pos) => {
    //             setDevicePosition(pos)
    //             setUserPosition(pos)
    //             cameraRef.current?.moveTo([
    //                 pos.coords.longitude,
    //                 pos.coords.latitude
    //             ], 500)
    //             console.log(pos)
    //         })
    //     })

    //     const unsubscribe = navigation.addListener('blur', () => {
    //         source.cancel()
    //     })

    //     return () => {
    //         focus
    //         unsubscribe
    //         source.cancel()
    //     }
    // }, [navigation])

    const onNavigate = (
        route: 'Search',
    ) => {
        navigation.navigate(route)
    }

    const onNavigateWithProps = (
        route: MapRoutes,
        props?: any,
    ) => {
        navigation.navigate(route, {
            pin_id: modalItem?.id
        })
    }

    const onNavigateToPin = (route: MapRoutes) => {
        setIsModalVisible(false)
        navigation.navigate(route, {
            pin_id: modalItem?.id
        })
    }

    const setGeolocation = async () => {
        try {
            let granted = await askForLocationPermission()

            if (!granted) {
                return
            }

            Geolocation.getCurrentPosition((pos) => {
                setDevicePosition(pos)
                setUserPosition(pos)
            },
                (err) => {
                    console.debug('Error in getCurrentPosition')
                    console.debug(err)
                })
        } catch (error) {
            console.debug(error)
        }
    }

    const moveToUserLocation = () => {
        setGeolocation()

        if (userPosition.coords.latitude !== 0 ||
            userPosition.coords.longitude !== 0) {
            cameraRef.current?.moveTo([
                userPosition.coords.longitude,
                userPosition.coords.latitude
            ], 500)
        }
    }

    const onUserUpdate = async (data: any) => {
        console.log('on user update')
        console.log(data)
    }

    const onFocusUser = () => {
        moveToUserLocation()
    }

    const setMunicipality = (
        municipality_name: string,
        municipality_code: string
    ) => {
        dispatch({
            type: AuthActions.SET_CURRENT_MUNICIPALITY,
            payload: {
                municipality_name,
                municipality_code,
            }
        })
    }

    const queryFeaturesAtPoint = async (
        screenPointX: number,
        screenPointY: number,
        options?: {
            layerIds?: LayerIds[]
        }
    ): Promise<IRenderedFeaturesAtPoint> => {
        if (options === undefined ||
            options.layerIds === undefined ||
            options.layerIds.length <= 0) {
            layerIds.push(
                LayerIds.PINS,
                LayerIds.MUNICIPALITIES,
                LayerIds.trailS,
                LayerIds.AREAS,
            )
        }

        let features =
            await mapRef.current?.queryRenderedFeaturesAtPoint(
                [
                    screenPointX,
                    screenPointY
                ],
                [
                    '!=', 'type', 'lol'
                ],
                options?.layerIds
            )

        return features
    }

    const onFocusPin = (values: IFocusPinData) => {
        console.log(onFocusPin.name)
        console.log(values)
        setIsSearchModalVisible(false)
        cameraRef.current?.moveTo(values.coordinates, 200)
    }

    const onLongPress = async (feature: IMapClickData) => {
        try {
            console.log('onLongPress')
            console.debug(feature)
            setClickPosition({
                latitude: feature.geometry.coordinates[1],
                longitude: feature.geometry.coordinates[0],
                screenPointX: feature.properties.screenPointX,
                screenPointY: feature.properties.screenPointY,
            })
            let municipality_feature =
                await queryFeaturesAtPoint(
                    feature.properties.screenPointX,
                    feature.properties.screenPointY,
                    {
                        layerIds: [LayerIds.MUNICIPALITIES]
                    }
                )

            console.log('Municipality features')
            console.log(municipality_feature)
            console.log(municipality_feature.features[0].properties)

            if (municipality_feature !== undefined &&
                municipality_feature !== null) {
                setCurrentClickMunicipality({
                    municipality_fire_id: municipality_feature
                        .features[0]
                        .properties
                        .fire_id,
                    municipality_mongo_id: municipality_feature
                        .features[0]
                        .properties
                        ._id,
                    municipality_name: municipality_feature
                        .features[0]
                        .properties
                        .municipality_name,
                    municipality_code: municipality_feature
                        .features[0]
                        .properties
                        .full_municipality_code,
                })

                setCurrentClickCountry({
                    country: municipality_feature
                        .features[0]
                        .properties
                        .country
                })
            }
            setIsContextMenuVisible(true)
        } catch (error) {
            setInfoModalData({
                title: 'Not currently available',
                description: `The region you longpressed isn't available yet`,
                list: [''],
            })
            setInfoModalVisible(true)
        }
    }

    const onMapClick = async (data: IMapClickData) => {
        try {
            console.log('map click')
            console.log(data)
            let sp =
                await mapRef.current?.getPointInView(
                    data.geometry.coordinates
                    // [
                    //     data.properties.screenPointX,
                    //     data.properties.screenPointY
                    // ]
                )
            console.log(sp)

            let bounds = await mapRef.current?.getVisibleBounds()
            console.log('bounds')
            console.log(bounds)

            let res =
                await mapRef.current?.queryRenderedFeaturesAtPoint(
                    [
                        data.properties.screenPointX,
                        data.properties.screenPointY
                    ],
                    [
                        '!=', 'type', 'lol'
                    ],
                    layerIds
                )

            // if (Platform.OS === 'android') {

            // } else if (Platform.OS === 'ios') {
            //     NSExpression
            // }

            let m_data =
                await mapRef.current?.queryRenderedFeaturesAtPoint(
                    [
                        data.properties.screenPointX,
                        data.properties.screenPointY
                    ],
                    [
                        '==', ['get', 'category'], 'Kommun'
                    ],
                    layerIds
                )

            let f_data =
                await mapRef.current?.queryRenderedFeaturesAtPoint(
                    [
                        data.properties.screenPointX,
                        data.properties.screenPointY
                    ],
                    [
                        '==', ['get', 'category'], 'Kommun'
                    ],
                    layerIds
                )

            // setModalItem({
            //     _id: '',
            //     info: {

            //     },
            //     latitude: 
            // })

            if (__DEV__) {
                console.log('Results mdata...')
                console.log(res)
                if (res.features.length > 0) {
                    // console.log(res.features[0].geometry)
                    // console.log(res.features[0].properties)
                    res.features.forEach((f: any) => {
                        console.log(f.properties)
                    })
                }

                // console.log(m_data.features[0].properties)
            }
        } catch (error) {
            console.debug(error)
        }
    }

    const onSearch = async (res: any[]) => {
        console.log('map.tsx on search')
        console.log(res)
    }

    const setMapFilter = () => {
        setFilteredPins([])
    }

    return (
        <>
            {/* <TopNavigationComponent
                navigation={navigation}
                title='Map'
                hasMenuAction={true}
            /> */}
            <View style={styles.page}>
                <View style={styles.mapContainer}>
                    <MapboxGL.MapView
                        ref={mapRef}
                        compassEnabled={true}
                        onDidFailLoadingMap={() => {
                            console.log('failed loading map')
                        }}
                        onWillStartLoadingMap={() => {
                            console.log('onWillStartLoadingMap')
                            // setMapLoading(true)
                        }}
                        onWillStartRenderingMap={() => {
                            console.log('onWillStartRenderingMap')
                            setMapLoading(true)
                        }}
                        onDidFinishLoadingMap={() => {
                            console.log('on did finished loading map')
                            setMapLoading(false)
                        }}
                        onDidFinishLoadingStyle={() => {
                            console.log('onDidFinishLoadingStyle')
                        }}
                        onDidFinishRenderingMap={() => {
                            console.log('onDidFinishRenderingMap')
                            setMapLoading(false)
                        }}
                        onLayout={() => {
                            console.log('on layout')
                        }}
                        // styleURL={map_theme}
                        // styleURL={`mapbox://styles/glenolof/ckai75i3y0n6t1io1cqlx59j5`}
                        styleURL={mapStylesURL}
                        // styleURL={mapStylesURL}
                        // styleURL={MapboxGL.StyleURL.Dark}
                        style={styles.map}
                        onPress={onMapClick}
                        onLongPress={onLongPress}
                    >
                        <MapboxGL.Camera
                            ref={cameraRef}
                            zoomLevel={12}
                            centerCoordinate={[
                                userPosition?.coords.longitude,
                                userPosition?.coords.latitude
                            ]}
                        />
                        <MapboxGL.Images
                            images={images}
                        // onImageMissing={(image_key) => setImageMap(...imageMap, [imageKey]: hikingIcon)}
                        />
                        <MapSymbolLayers
                            stylesURL={mapStylesURL}
                            filteredPinTypes={filteredPins}
                        />
                        {modalItem !== undefined &&
                            <PinModal
                                navigation={navigation}
                                isModalVisible={isModalVisible}
                                setIsModalVisible={setIsModalVisible}
                                modalItem={modalItem}
                            />
                        }
                        <SearchModal
                            navigation={navigation}
                            isSearching={isSearching}
                            setIsSearching={setIsSearching}
                            isModalVisible={isSearchModalVisible}
                            setIsModalVisible={setIsSearchModalVisible}
                            mapRef={mapRef}
                            onSearch={onSearch}
                            onFocusPin={onFocusPin}
                        // onSearch={onSearch}
                        />
                        <MapContextMenu
                            navigation={navigation}
                            visible={isContextMenuVisible}
                            setVisible={setIsContextMenuVisible}
                            deviceLocation={{
                                latitude: devicePosition !== undefined ? devicePosition.coords.latitude : clickPosition.latitude,
                                longitude: devicePosition !== undefined ? devicePosition.coords.longitude : clickPosition.longitude
                            }}
                            location={clickPosition}
                            municipality={currentClickMunicipality}
                            country={currentClickCountry.country}
                        />
                        <InformationModal
                            title={infoModalData.title}
                            description={infoModalData.description}
                            list={infoModalData.list}
                            visible={infoModalVisible}
                            setVisible={setInfoModalVisible}
                        />
                    </MapboxGL.MapView>
                    <GroupedSpeedDialAbsolute
                        xIsLeft={false}
                        yIsTop={false}
                        initialPosition={{
                            x: 20,
                            y: 20,
                        }}
                        positionIncrementY={60}
                        positionIncrementX={0}
                        duration={250}
                        staggerDelay={250}
                        easing={Easing.linear}
                        dialIcon={GridIcon}
                        components={speedDialActions}
                    />
                    {/* <SpeedDialAbsolute
                        xIsLeft={false}
                        yIsTop={false}
                        initialPosition={{
                            x: 20,
                            y: 20,
                        }}
                        positionIncrementY={60}
                        positionIncrementX={0}
                        duration={250}
                        staggerDelay={100}
                        easing={Easing.ease}
                        dialIcon={GridIcon}
                        components={[
                            {
                                style: {
                                    position: 'absolute',
                                    elevation: 5,
                                    // bottom: 170,
                                    // left: 20,
                                    borderRadius: 500,
                                    height: 50,
                                    width: 50,
                                    // backgroundColor: 'rgba(0, 0, 0, 0)',
                                    backgroundColor: 'green',
                                    borderColor: 'green',
                                },
                                accessoryLeft: SearchIcon, openGroup: 1,
                                onPress: () => setIsSearchModalVisible(true)
                            },
                            {
                                style: {
                                    position: 'absolute',
                                    elevation: 5,
                                    // bottom: 170,
                                    // left: 20,
                                    borderRadius: 500,
                                    height: 50,
                                    width: 50,
                                    backgroundColor: 'green',
                                    borderColor: 'green',
                                },
                                accessoryLeft: FilterIcon, openGroup: 1,
                                onPress: () => moveToUserLocation(),
                            },
                            {
                                style: {
                                    position: 'absolute',
                                    elevation: 5,
                                    // bottom: 170,
                                    // left: 20,
                                    borderRadius: 500,
                                    height: 50,
                                    width: 50,
                                    backgroundColor: 'green',
                                    borderColor: 'green',
                                },
                                accessoryLeft: CrosshairIcon,
                                onPress: () => moveToUserLocation(),
                            },
                        ]}
                    /> */}
                </View>
            </View>
        </>
    )
}