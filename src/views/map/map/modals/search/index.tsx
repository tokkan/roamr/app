import React from 'react'
import { StyleSheet, ImageProps } from "react-native"
import { Icon } from "@ui-kitten/components"
import { PinTypesEnum } from './../../../../../models'

export const styles = StyleSheet.create({
    layout: {
        // flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    selectContainer: {
        minWidth: '100%',
        marginBottom: 10,
    },
    paddedContainer: {
        // height: 200,
        // maxHeight: '50%',
        paddingLeft: 10,
        paddingTop: 10,
        paddingRight: 10,
    },
    containerItem: {
        paddingTop: 20,
    },
    listItem: {
        padding: 0,
        paddingTop: 5,
        paddingBottom: 5,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        marginTop: 10,
    },
})

export const AlertIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='alert-circle-outline' />
    )

export enum LocationTypes {
    ALL = 'all',
    STABLE = 'stable',
    SHOP = 'shop',
}

export interface IStateFields {
    search: string
    location_type: LocationTypes
}

export interface IState extends IStateFields {
}

export enum LoginPaths {
    LOGIN = '/auth/login',
    REGISTER = '/auth/register',
    CHECK_NAME = '/auth/check-name',
}

export const initFieldsState: IState = {
    search: '',
    location_type: LocationTypes.ALL,
}

export interface IFieldValidation {
    usernameMinLength: number,
    playerNameMinLength: number,
    passwordMinLength: number,
}

export const initFieldValidation: IFieldValidation = {
    usernameMinLength: 3,
    playerNameMinLength: 3,
    passwordMinLength: 6,
}

export interface IRenderBusinessItem {
    item: any,
    index: number
}

export interface ISearchItemGeometry {
    coordinates: number[]
    type: string
}

export interface ISearchItemProperties {
    title: string
    sub_title: string
    description: string
    is_business: boolean
    pin_type: PinTypesEnum
    zone_id: string
}

export interface ISearchItemData {
    id: string
    type: string
    geometry: ISearchItemGeometry
    properties: ISearchItemProperties
}

export interface IRenderSearchItemData {
    index: number
    item: ISearchItemData
}