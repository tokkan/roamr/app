import React from "react"
import {
    View
} from "react-native"
import {
    Text, Card, Button, Modal
} from "@ui-kitten/components"
import { modalStyles } from "../../styles/styles"
import { SearchComponent } from "./search-component"
import { IStateFields } from "."
import MapboxGL, { Expression } from "@react-native-mapbox-gl/maps"
import { CloseIcon } from "./../../../../../models"
import { IFocusPinData } from "./../../../"
import { circle, bbox } from "@turf/turf"
import { LayerIds } from "../.."

export interface IModalProps {
    navigation: any
    isSearching: boolean
    setIsSearching: React.Dispatch<React.SetStateAction<boolean>>
    isModalVisible: boolean
    setIsModalVisible: Function
    mapRef: React.RefObject<MapboxGL.MapView>
    // onSearch: (values: IStateFields) => void
    onSearch: (res: any[]) => void
    onFocusPin: (values: IFocusPinData) => void
}

export const SearchModal: React.FC<IModalProps> =
    ({
        navigation,
        isSearching,
        setIsSearching,
        isModalVisible,
        setIsModalVisible,
        mapRef,
        // onSearch,
        onSearch,
        onFocusPin,
    }) => {

        const Header = (props: any) => {

            const CloseAction = () => (
                <Button
                    appearance='ghost'
                    accessoryLeft={CloseIcon}
                    onPress={() => setIsModalVisible(false)}
                />
            )

            return (
                <View {...props}
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignContent: 'center',
                        alignItems: 'center',
                        paddingLeft: 35,
                    }}
                >
                    <Text category='h6'>
                        Search
                    </Text>
                    <CloseAction />
                </View>
            )
        }

        const Footer = (props: any) => (
            <View {...props} style={[props.style, modalStyles.footerContainer]}>
                <Button
                    style={modalStyles.footerControl}
                    size='small'
                    appearance='ghost'
                    onPress={() => setIsModalVisible(false)}
                >
                    Close
                </Button>
            </View>
        )

        return (
            <Modal
                style={{
                    height: '100%'
                }}
                visible={isModalVisible}
                backdropStyle={modalStyles.backdrop}
                onBackdropPress={() => setIsModalVisible(false)}
            >
                <Card
                    header={Header}
                    // footer={Footer}
                    // footer={undefined}
                    disabled={true}
                    style={{
                        height: '100%'
                    }}
                >
                    <SearchComponent
                        mapRef={mapRef}
                        onFocusPin={onFocusPin}
                        onSearch={onSearch}
                    />
                </Card>
            </Modal>
        )
    }