import React, { useState, useEffect } from 'react'
import {
    View, FlatList
} from 'react-native'
import {
    Layout, Icon, Spinner, Input, Button, IndexPath, Divider, ListItem
} from '@ui-kitten/components'
import {
    styles, IStateFields, IState, initFieldsState, ISearchItemData, IRenderSearchItemData
} from '.'
import MapboxGL, { Expression } from '@react-native-mapbox-gl/maps'
import { bbox, circle } from '@turf/turf'
import { IFocusPinData } from 'src/views/map'
import { modalStyles } from '../../styles/styles'
import { PinTypesEnum } from './../../../../../models'

const selectInit: IndexPath[] = [
    // new IndexPath(0, 0),
    // new IndexPath(1),
]

interface IBusinessTypeGroup {
    [key: string]: string[]
}

const businessTypeGroups: IBusinessTypeGroup = {
    'food': [
        'cafe',
        'restaurant',
    ],
    'outdoors': [
        'camping'
    ],
    'trail': [
        'hiking',
        'swimming',
        'fishing',
        'biking',
    ]
}

export interface IProps {
    mapRef: React.RefObject<MapboxGL.MapView>
    // onSearch: (values: IStateFields) => void
    onFocusPin: (values: IFocusPinData) => void
}

export const SearchComponent: React.FC<IProps> = ({
    // { navigation }: any
    mapRef,
    // onSearch,
    onFocusPin,
}) => {

    const [values, setValues] = useState<IStateFields>(initFieldsState)
    const [isRequsting, setIsRequesting] = useState(false)
    const [businessData, setBusinessData] = useState([])
    const [selectedBusinessTypeIndex, setSelectedBusinessTypeIndex] =
        useState(selectInit)
    const [businessTypeOptions] = useState(businessTypeGroups)
    const [groupDisplayValues, setGroupDisplayValues] = useState<string[]>([])
    const [isSearching, setIsSearching] = useState(false)
    const [layerIds, setLayerIds] = useState<string[]>([
        'tiveden-cafe',
        'tiveden-store',
        'tiveden-restaurant',
        'tiveden-trails',
        'tiveden-areas',
    ])
    const [items, setItems] = useState<ISearchItemData[]>([])

    // useEffect(() => {
    //     const focus = navigation.addListener('focus', () => {
    //         setSource(axios.CancelToken.source())
    //     })

    //     const unsubscribe = navigation.addListener('blur', () => {
    //         source.cancel()
    //     })

    //     return () => {
    //         focus
    //         unsubscribe
    //         source.cancel()
    //     }
    // }, [navigation])

    useEffect(() => {
        if (selectedBusinessTypeIndex.length > 0) {
            let grp = selectedBusinessTypeIndex.map(index => {
                if (index.section !== undefined) {
                    let grpTitle = Object.keys(businessTypeGroups)[index.section]
                    let g = businessTypeGroups[grpTitle][index.row]
                    return capitalize(g)
                } else {
                    return ''
                }
            })

            if (grp !== undefined) {
                setGroupDisplayValues(grp)
            } else {
                setGroupDisplayValues([''])
            }
        } else {
            setGroupDisplayValues([])
        }
    }, [selectedBusinessTypeIndex])

    const onHandleChange = (prop: keyof IState) => (value: string) => {
        setValues({ ...values, [prop]: value })
    }

    const capitalize = ([first, ...rest]: string) => {
        return [first.toLocaleUpperCase(), ...rest].join('')
    }

    const setBusinessTypeSelect = (index: any) => {
        setSelectedBusinessTypeIndex(index)
    }

    const onSearchMapFeatures = async (values: IStateFields) => {
        try {
            // TODO: Make request to MongoDB to search
            setItems([
                {
                    id: 'fakemongoid',
                    type: 'Feature',
                    properties: {
                        title: 'title',
                        sub_title: 'sub tits',
                        description: 'desc',
                        is_business: true,
                        pin_type: PinTypesEnum.event,
                        zone_id: 'fakezoneid',
                    },
                    geometry: {
                        type: 'Point',
                        coordinates: [14, 60]
                    }
                }
            ])
        } catch (error) {
            console.debug(error)
            setItems([])
        }
    }

    // const onSearch = async (values: IStateFields) => {

    //     console.log('search component')

    //     try {
    //         setIsSearching(true)
    //         console.log('on search')
    //         console.log(values)

    //         let center: number[] = await mapRef.current?.getCenter()
    //         // let viewCenter: number[] =
    //         //     await mapRef.current?.getPointInView(center)
    //         console.log(center)
    //         // console.log(viewCenter)
    //         let area = circle(center, 5, {
    //             units: 'kilometers',
    //             steps: 10,
    //         })

    //         // console.log(area.geometry?.coordinates)

    //         let tbbox = bbox(area)
    //         console.log('tbbox')
    //         console.log(tbbox)

    //         let b1 =
    //             await mapRef.current?.getPointInView([tbbox[0], tbbox[1]])
    //         let b2: number[] =
    //             await mapRef.current?.getPointInView([tbbox[2], tbbox[3]])

    //         // let b1: number[] =
    //         //     await mapRef.current?.getPointInView([tbbox[1], tbbox[0]])
    //         // let b2: number[] =
    //         //     await mapRef.current?.getPointInView([tbbox[3], tbbox[2]])

    //         console.log('getpointinview')
    //         console.log(b1)
    //         console.log(b2)

    //         let plain_bbox = [
    //             b1[0] * 3, b1[1] * 3, b2[0] * 3, b2[1] * 3
    //         ]
    //         console.log(plain_bbox)

    //         let expr: Expression = [
    //             // '!=', 'type', 'lol', 
    //             'all',
    //             ['!=', 'pin_type', 'AREA'],
    //             ['!=', 'pin_type', 'trail'],
    //         ]

    //         let res = await mapRef.current?.queryRenderedFeaturesInRect(
    //             plain_bbox,
    //             expr,
    //             layerIds
    //         )

    //         console.log('Result...')
    //         console.log(res)
    //         if (res.features.length > 0) {
    //             res.features.map((f: any) => {
    //                 console.log(f.geometry)
    //                 console.log(f.properties)
    //             })

    //             setItems(res.features)
    //         }

    //         // setIsSearchModalVisible(false)
    //         setIsSearching(false)
    //     } catch (error) {
    //         console.debug(error)
    //         // setIsSearchModalVisible(false)
    //         setIsSearching(false)
    //     }
    // }

    const LoadingIndicator = (props: any) => (
        <View style={[props.style, styles.indicator]}>
            {isRequsting ?
                <Spinner size='small' /> :
                <Icon {...props} name='search' />
            }
        </View>
    )

    const MapAction = (item: ISearchItemData) => {
        return (
            <Button
                appearance='ghost'
                size='small'
                status='basic'
                accessoryLeft={LoadingIndicator}
                onPress={() => onFocusPin({
                    coordinates: item.geometry.coordinates
                })}
            />
        )
    }

    const renderItems = ({ item, index }: IRenderSearchItemData) => {
        console.log('renderitem')
        console.log(item)
        return (
            <ListItem
                key={index}
                style={styles.listItem}
                // title={item.comment}
                description={item.properties.title}
                // accessoryLeft={() => commentAvatar(item)}
                accessoryRight={() => MapAction(item)}
            />
        )
    }

    const Header = (props: any) => {
        return (
            <View {...props}>
                <Divider />
                {/* <Text category='h6'>
                    Search
                </Text> */}
                {/* <Divider /> */}
            </View>
        )
    }

    const Footer = (props: any) => (
        <View {...props} style={[
            props.style,
            // modalStyles.footerContainer
        ]}>
            <Button
                style={modalStyles.footerControl}
                size='small'
                appearance='ghost'
            // onPress={() => setIsModalVisible(false)}
            >
                Close
            </Button>
        </View>
    )

    return (
        <Layout style={styles.layout}>
            {/* <Layout style={[styles.paddedContainer, styles.selectContainer]}>
                <Select
                    label='Filter by pin types'
                    multiSelect={true}
                    value={groupDisplayValues.join(', ')}
                    selectedIndex={selectedBusinessTypeIndex}
                    onSelect={(index: IndexPath | IndexPath[]) => setBusinessTypeSelect(index)}
                >
                    {Object.keys(businessTypeGroups).map((m, i) => (
                        <SelectGroup key={i} title={capitalize(m)}>
                            {businessTypeGroups[m].map((m2, i2) => (
                                <SelectItem key={i2} title={capitalize(m2)} />
                            ))}
                        </SelectGroup>
                    ))}
                </Select>
            </Layout> */}
            <Layout
                style={[
                    {
                        paddingBottom: 15,
                    },
                    styles.paddedContainer,
                    styles.selectContainer
                ]}
            >
                <Input
                    label='Search'
                    placeholder='Search'
                    value={values.search}
                    onChangeText={onHandleChange('search')}
                />
                <Button
                    style={styles.button}
                    appearance='outline'
                    accessoryLeft={LoadingIndicator}
                    onPress={() => onSearchMapFeatures(values)}
                >
                    Search
                </Button>
            </Layout>
            {/* <Divider /> */}
            <Layout style={[
                // styles.paddedContainer,
                // styles.selectContainer,
            ]}>
                <FlatList
                    style={[
                        styles.selectContainer,
                        {
                            width: '100%',
                            flex: 1,
                            // height: 200,
                            // maxHeight: 200,
                        }
                    ]}
                    data={items}
                    renderItem={renderItems}
                    ItemSeparatorComponent={Divider}
                    keyExtractor={items => items.id}
                    ListHeaderComponent={Header}
                // ListFooterComponent={Footer}
                />
                <Divider />
            </Layout>
        </Layout>
    )
}