import React from "react"
import {
    View
} from "react-native"
import {
    Text, Card, Button, Modal
} from "@ui-kitten/components"
import { modalStyles } from "../styles/styles"
import { MapRoutes } from "./../../../../models/common/routes"

export interface IModalProps {
    navigation: any
    isModalVisible: boolean
    setIsModalVisible: Function
    modalItem: any
}

export const PinAreaModal: React.FC<IModalProps> =
    (
        { navigation, isModalVisible, setIsModalVisible, modalItem }
    ) => {

        const onNavigate = (route: MapRoutes) => {
            setIsModalVisible(false)
            navigation.navigate(route, {
                pin_id: modalItem?._id
            })
        }

        const Header = (props: any) => {
            return (
                <View {...props}>
                    <Text category='h6'>
                        {modalItem?.info.title}
                    </Text>
                    <Text category='s1'>
                        {modalItem?.info.sub_title}
                    </Text>
                </View>
            )
        }

        const Footer = (props: any) => (
            <View {...props} style={[props.style, modalStyles.footerContainer]}>
                <Button
                    style={modalStyles.footerControl}
                    size='small'
                    status='basic'
                    onPress={() => setIsModalVisible(false)}
                >
                    Close
                </Button>
                <Button
                    style={modalStyles.footerControl}
                    size='small'
                    onPress={() => onNavigate('Pin details')}
                    // onPress={() => onNavigate(MapRoutes.PIN_DETAILS)}
                >
                    More info
                </Button>
                <Button
                    style={modalStyles.footerControl}
                    size='small'
                >
                    Directions
                </Button>
            </View>
        )

        return (
            <Modal
                visible={isModalVisible}
                backdropStyle={modalStyles.backdrop}
                onBackdropPress={() => setIsModalVisible(false)}
            >
                <Card
                    header={Header}
                    footer={Footer}
                    disabled={true}
                >
                    <Text>
                        {`${modalItem?.info.description} 😻`}
                    </Text>
                    // TODO: List with nearby pins
                </Card>
            </Modal>
        )
    }