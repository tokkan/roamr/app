import React, { useState, useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import {
    Button, Card, Modal, IndexPath, ListItem, List, Divider
} from '@ui-kitten/components'
import { showLocation } from 'react-native-map-link'
import { i18n } from './../../../../locales/localization'
import { StoreIcon } from './../../../../models'
import { ICurrentMunicipality, ICurrentCountry } from 'src/store/auth'

interface IListItem {
    title: string
    function: () => void
}

export interface IProps {
    navigation: any
    visible: boolean
    setVisible: Function
    deviceLocation: {
        latitude: number,
        longitude: number,
    }
    location: {
        latitude: number,
        longitude: number,
        screenPointX: number,
        screenPointY: number,
    },
    municipality: ICurrentMunicipality
    country: string
}

export const MapContextMenu: React.FC<IProps> = ({
    navigation,
    visible,
    setVisible,
    deviceLocation,
    location,
    municipality,
    country,
}) => {
    const [listData, setListData] = useState<IListItem[]>([])

    useEffect(() => {
        setListData([
            {
                title: 'map.menu.get_nearby_pins',
                function: onNavigateToCreateBusiness,
            },
            {
                title: 'map.menu.suggest_pin',
                function: onNavigateToCreateBusiness,
            },
            {
                title: 'map.menu.create_pin_item',
                function: onNavigateToCreatePin,
            },
            {
                title: 'map.menu.create_business_item',
                function: onNavigateToCreateBusiness,
            },
            {
                title: 'map.menu.create_event_item',
                function: onNavigateToCreateZoneEvent,
            },
            {
                title: 'map.menu.copy_coordinates',
                function: onNavigateToCreateZoneEvent,
            },
            {
                // title: 'map.menu.open_navigation_app',
                title: 'map.menu.open_external_nav_app',
                function: openNavigation,
            },
        ])
    }, [])

    const onNavigateToCreatePin = () => {
        setVisible(false)
        navigation.navigate('Create pin', {
        // navigation.navigate(MapRoutes.CREATE_PIN, {
            latitude: location.latitude,
            longitude: location.longitude,
            municipality: municipality,
        })
    }

    const onNavigateToCreateBusiness = () => {
        setVisible(false)
        navigation.navigate('Create business', {
        // navigation.navigate(MapRoutes.CREATE_BUSINESS, {
            latitude: location.latitude,
            longitude: location.longitude,
            municipality: municipality,
            country: country,
        })
    }

    const onNavigateToCreateZoneEvent = () => {
        setVisible(false)
        navigation.navigate('Create zone event', {
        // navigation.navigate(MapRoutes.CREATE_ZONE_EVENT, {
            latitude: location.latitude,
            longitude: location.longitude,
        })
    }

    const openNavigation = () => {
        showLocation({
            latitude: location.latitude,
            longitude: location.longitude,
            sourceLatitude: deviceLocation.latitude,  // optionally specify starting location for directions
            sourceLongitude: deviceLocation.longitude,  // not optional if sourceLatitude is specified
            // title: 'The White House',  // optional
            googleForceLatLon: false,  // optionally force GoogleMaps to use the latlon for the query instead of the title
            // googlePlaceId: 'ChIJGVtI4by3t4kRr51d_Qm_x58',  // optionally specify the google-place-id
            // alwaysIncludeGoogle: true, // optional, true will always add Google Maps to iOS and open in Safari, even if app is not installed (default: false)
            dialogTitle: 'This is the dialog Title', // optional (default: 'Open in Maps')
            dialogMessage: 'This is the amazing dialog Message', // optional (default: 'What app would you like to use?')
            cancelText: 'This is the cancel button text', // optional (default: 'Cancel')
            // appsWhiteList: ['google-maps'] // optionally you can set which apps to show (default: will show all supported apps installed on device)
            // // appTitles: { 'google-maps': 'My custom Google Maps title' } // optionally you can override default app titles
            // // app: 'uber'  // optionally specify specific app to use
        })
        setVisible(false)
    }

    const renderItem: React.FC<{
        item: {
            title: string
            function: any
        }
        index: number
    }> = ({
        item,
        index,
    }) => (
                <ListItem
                    title={i18n.t(item.title)}
                    // description={`${item.description} ${index + 1}`}
                    accessoryLeft={StoreIcon}
                    onPress={item.function}
                />
            )

    return (
        <View style={styles.container}>
            <Modal
                visible={visible}
                style={{
                    minWidth: '60%',
                    width: '100%',
                }}
                backdropStyle={styles.backdrop}
                onBackdropPress={() => setVisible(false)}
            >
                <Card
                    disabled={true}
                    status='primary'
                >
                    <List
                        style={styles.listContainer}
                        data={listData}
                        ItemSeparatorComponent={Divider}
                        renderItem={renderItem}
                    />
                    <Button
                        size='tiny'
                        appearance='outline'
                        onPress={() => setVisible(false)}
                    >
                        {i18n.t('common.dismiss')}
                    </Button>
                </Card>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        minWidth: '60%',
        width: '100%',
    },
    listContainer: {
        // marginTop: 20,
        marginBottom: 20,
        width: '100%',
        // minHeight: 192,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
});