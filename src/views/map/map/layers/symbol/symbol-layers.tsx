import React, { useState, useEffect } from 'react'
import MapboxGL, { Expression } from '@react-native-mapbox-gl/maps'
// import { PinTypes } from 'src/models'

interface IProps {
    stylesURL: string
    filteredPinTypes: string[]
}

export const MapSymbolLayers: React.FC<IProps> = ({
    stylesURL,
    filteredPinTypes,
}) => {
    const [filteredPinsExpression, setFilteredPinsExpressions] =
        useState<Expression>(['!=', 'pin_type', 'match_all'])

    useEffect(() => {
        if (filteredPinTypes.length <= 0) {
            setFilteredPinsExpressions(['!=', 'pin_type', 'match_all'])
        } else {
            let exp: Expression = ['any']
            filteredPinTypes
                .forEach(f =>
                    exp.push(['==', 'pin_type', f]))
            setFilteredPinsExpressions(exp)
        }

        console.log('filteredPinsExpression')
        console.log(filteredPinsExpression)
    }, [filteredPinTypes])

    return (
        <MapboxGL.VectorSource
            url={stylesURL}
        >
            <MapboxGL.LineLayer
                id='streetFill'
                sourceLayerID='road' // road is a layer defined on the map already
                // style={layerStyles.streetLine}
                // filter={['in', 'type', 'street']} 
                style={{
                    lineColor: 'yellow',
                    lineWidth: 1,
                    visibility: 'visible',
                }}
            // filter={['any', 'type', 'street']}
            />
            <MapboxGL.FillLayer
                id='water-fill-layer'
                sourceLayerID='water' // road is a layer defined on the map already
                // style={layerStyles.streetLine}
                // filter={['in', 'type', 'street']} 
                style={{
                    fillColor: 'blue',
                    visibility: 'none',
                }}
            // filter={['any', 'type', 'street']}
            />
            <MapboxGL.SymbolLayer
                id='poi-label-layer'
                sourceLayerID='poi_label'
                style={{
                    iconOpacity: 1,
                    iconAllowOverlap: true,
                    // iconImage: './../icons/hiking2.png',
                    // iconImage: hikingIcon,
                    // iconImage: ['get', 'icon'],
                    // iconImage: ['get', 'hiking'],
                    // iconImage: ['get', 'airport-15'],
                    // iconImage: 'airport-15',
                    // iconImage: ['get', 'hiking_local'],
                    iconImage: 'hikingpng',
                    iconIgnorePlacement: true,
                    iconColor: 'blue',
                    iconSize: 1,
                    // textField: 'red label',
                    // textColor: 'red',
                    textAllowOverlap: true,
                    textOffset: [0, 2],
                    visibility: 'visible',
                }}
            />
            {/* <MapboxGL.FillLayer
                                id='roamr-pins-fill-layer'
                                sourceLayerID='natural-point-label'
                                style={{
                                    // iconOpacity: 0,
                                    fillColor: 'red',
                                    visibility: 'visible',
                                }}
                            // filter={['==', 'pin_type', 'camping']}
                            /> */}
            {/* <MapboxGL.SymbolLayer
                                id='roamr-pins-symbol-layer'
                                // sourceID={MapboxGL.StyleSource.DefaultSourceID}
                                sourceLayerID='roamr_pins'
                                style={{
                                    // iconOpacity: 0,
                                    textColor: 'green',
                                    iconColor: 'green',
                                    visibility: 'visible',
                                }}
                            // filter={['==', 'pin_type', 'camping']}
                            /> */}
            <MapboxGL.SymbolLayer
                id='roamr-pins-symbol-layer'
                // sourceID={MapboxGL.StyleSource.DefaultSourceID}
                sourceLayerID='roamr_pins'
                // aboveLayerID='poi_label'
                style={{
                    iconOpacity: 1,
                    iconAllowOverlap: true,
                    // iconImage: './../icons/hiking2.png',
                    // iconImage: ['get', 'hiking'],
                    // iconImage: 'airport-15',
                    // iconImage: 'hiking2',
                    iconImage: [
                        'match',
                        ['string', ['get', 'pin_type']],
                        'camping',
                        'caravan',
                        'trail',
                        // 'hiking2',
                        'hikingpng',
                        'airport-15'
                    ],
                    // iconImage: ['get', 'hikingIcon'],
                    iconSize: 1,
                    // iconSize: [
                    //     'match',
                    //     ['get', 'hikingIcon'],
                    //     'hiking',
                    //     2,
                    //     2,
                    // ],
                    iconColor: 'blue',
                    iconIgnorePlacement: true,
                    // textField: 'blue',
                    // textColor: 'blue',
                    textAllowOverlap: true,
                    textOffset: [0, 2],
                    textOpacity: 1,
                    visibility: 'visible',
                }}
                // filter={['any',
                //     ['==', 'pin_type', 'camping'],
                //     ['==', 'pin_type', 'trail'],
                // ]}
                filter={filteredPinsExpression}
            // filter={['==', 'pin_type', 'camping']}
            />
        </MapboxGL.VectorSource>
    )
}