import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        textAlign: 'center',
    },
    likeButton: {
        marginVertical: 16,
    },
    page: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF"
    },
    mapContainer: {
        // height: 300,
        // width: 300,
        height: '100%',
        width: '100%',
    },
    map: {
        flex: 1
    },
    mapPinIcon: {
        width: 0,
        height: 0,
        margin: 0,
        padding: 0,
        paddingTop: 0,
        paddingBottom: 0,
    },
    popoverContent: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 4,
        paddingVertical: 8,
    },
    avatar: {
        marginHorizontal: 4,
    },

})

export const modalStyles = StyleSheet.create({
    card: {
        // flex: 1,
        margin: 2,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    footerContainer: {
        maxHeight: 100,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
})