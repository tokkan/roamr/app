// @ts-ignore
import hikingIcon from './../../../../icons/hiking.png'
import campingcar from './../../../../icons/campingcar.png'
import bnb from './../../../../icons/bed_breakfast1-2.png'

export const imageMap = {
    hikingpng: hikingIcon,
    campingcarpng: campingcar,
    bnbpng: bnb,
}