import MapboxGL from "@react-native-mapbox-gl/maps"

// TODO: Update remove MapboxGL.StyleSheet.create, and use normal function that instead :returnType which is the stylesheet return function, should be possible to see the correct one by hovering styles in mapbox components
export const layerStyles = MapboxGL.StyleSheet.create({
    streetLine: {
        lineColor: 'blue',
        lineWidth: 2
    }
})