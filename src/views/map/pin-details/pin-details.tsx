import React, { useEffect, useState } from 'react'
import { View } from 'react-native'
import {
    Layout, Text, Button, Divider
} from '@ui-kitten/components'
import { TopNavigationComponent } from './../../../components/top-navigation/top-navigation-stack'
import axios from 'axios'
import { IPinDetails, samplePinFullInfo } from '..'
import { PinCarousel } from './carousel'
import {
    styles
} from '.'
import { FilledLikeIcon, LikeIcon, CommentIcon, MessageIcon, MapIcon } from './../../../models'

export const PinDetailsScreen = ({ route, navigation }: any) => {

    const [source, setSource] = useState(axios.CancelToken.source())
    const [pinId, setPinId] = useState(route.params.pin_id)
    const [pinItem, setPinItem] = useState<IPinDetails>()
    const [isLiked, setIsLiked] = useState(false)

    useEffect(() => {
        const focus = navigation.addListener('focus', async () => {
            setSource(axios.CancelToken.source())
            // const { pin_id } = route.params
            // setPinId(pin_id)
            // await getPinInfo()

            await getPinInfo(pinId)
        })

        const unsubscribe = navigation.addListener('blur', () => {
            source.cancel()
        })

        return () => {
            focus
            unsubscribe
            source.cancel()
        }
    }, [navigation])

    useEffect(() => {
    }, [])

    const getPinInfo = async (id: string) => {
        setTimeout(() => {
            let response = samplePinFullInfo.filter(f => f._id === id)
            console.log(response)
            setPinItem(response[0])
        }, 1000)
    }

    const Header = (props: any) => {
        return (
            <View {...props}
                style={styles.headerContainer}>
                <View style={{
                    flexDirection: 'column'
                }}>
                    <Text category='h6'>
                        {pinItem?.info.title}
                    </Text>
                    <Text category='s1'>
                        {pinItem?.info.sub_title}
                    </Text>
                </View>
                <View>
                    <Button
                        // style={styles.footerControl}
                        // appearance='ghost'
                        size='medium'
                        // status='basic'
                        // accessoryLeft={CommentIcon}
                        onPress={() => console.log('Follow icon click')}
                    >
                        Follow
                    </Button>
                </View>
            </View>
        )
    }

    const Footer = (props: any) => (
        <View>
            {pinItem?.info.likes !== undefined &&
                pinItem.info.likes > 0 ?
                <Layout style={styles.cardLikesContainer}>
                    <Text category='p2' appearance='hint'>
                        {`${pinItem.info.likes} has liked`}
                    </Text>
                </Layout>
                : <View></View>}
            <View {...props} style={[props.style, styles.footerContainer]}>
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={isLiked ? FilledLikeIcon : LikeIcon}
                    onPress={() => setIsLiked(!isLiked)}
                />
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={CommentIcon}
                    onPress={() => console.log('Comment icon click')}
                />
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={MessageIcon}
                />
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={MapIcon}
                />
            </View>
            <View {...props} style={
                [
                    {
                        paddingLeft: 25,
                    },
                    props.style,
                    styles.footerContainer
                ]}>
                <Text>
                    {pinItem?.info.description}
                </Text>
            </View>
            <View {...props} style={[
                props.style,
                styles.footerContainer,
                styles.footerLastContainer,
            ]}>
                <Button
                    style={{
                        marginHorizontal: 2,
                    }}
                    size='small'
                    status='basic'
                >
                    Events
                </Button>
                <Button
                    style={{
                        marginHorizontal: 2,
                    }}
                    size='small'
                // onPress={() => onNavigate(MapRoutes.PIN_DETAILS)}
                >
                    News
                </Button>
                <Button
                    style={{
                        marginHorizontal: 2,
                    }}
                    size='small'
                >
                    Directions
                </Button>
            </View>
        </View>
    )

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title='Pin details'
            />
            <Layout style={{
                flex: 1,
                // justifyContent: 'flex-start', 
                // alignItems: 'center'
            }}>
                {/* <Card
                    style={styles.card}
                    header={Header}
                    footer={ModalFooter}
                    disabled={true}
                >
                    {pinItem === undefined ||
                        <PinCarousel
                            data={pinItem.info.images}
                            // sliderWidth={300}
                            divideItemWidthBy={1.5}
                        />
                    }
                    <Text>
                        {`${pinItem?.info.description} 😻`}
                    </Text>
                </Card> */}
                <Header />
                <Divider />
                {pinItem === undefined ||
                    <PinCarousel
                        data={pinItem.info.images}
                        // sliderWidth={300}
                        divideItemWidthBy={1.5}
                    />
                }
                <Divider />
                <Footer />
            </Layout>
        </Layout>
    )
}