import React from 'react'
import { StyleSheet, ImageProps } from "react-native"
import { Icon } from '@ui-kitten/components'

export const styles = StyleSheet.create({
    card: {
        // width: '100%',
        padding: 0,
        margin: 0,
    },
    cardContentContainer: {
        width: '100%'
    },
    cardLikesContainer: {
        paddingLeft: 10,
        paddingTop: 10,
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 10,
        paddingTop: 10,
        paddingLeft: 25,
        paddingRight: 25,
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        // width: 5,
        // height: 20,
        paddingLeft: 15,
        paddingRight: 0,
        paddingTop: 2,
        paddingBottom: 2,
    },
    footerLastContainer: {
        paddingBottom: 10,
        paddingTop: 10,
        paddingLeft: 25,
    },
    footerControl: {
        // marginHorizontal: 2,
        width: 5,
        margin: 0,
        paddingTop: 2,
        paddingBottom: 2,
    },
})

export const modalStyles = StyleSheet.create({
    card: {
        flex: 1,
        margin: 2,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
})

export const carouselStyles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        paddingTop: 10,
        alignItems: 'center',
        // alignSelf: 'center',
        // maxHeight: 350,
    },
    carousel: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        height: 250,
        width: '100%',
    },
    itemContainer: {
        height: 250,
        width: '100%',
        // flex: 1,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        // height: 200,
        // width: 200,
    }
})