import { AllPinTypes, PinTypesEnum } from "./../../models"
import { CountryTypes } from "./../../models/common/common"
import { MapRoutes } from "./../../models/common/routes"

export type RootStackMapRoutesParamList = {
    'Map': undefined
    'Create business': {
        latitude: number
        longitude: number
        municipality: {
            municipality_mongo_id: string
            municipality_fire_id: string
            municipality_name: string
            municipality_code: string
        },
        country: CountryTypes
    }
    'Pin details': {
        pin_id: string
    }
    // Feed: { sort: 'latest' | 'top' } | undefined
    
    // [MapRoutes.MAP]: undefined
    // [MapRoutes.CREATE_BUSINESS]: {
    //     latitude: number
    //     longitude: number
    //     municipality: {
    //         municipality_name: string
    //         municipality_code: string
    //     },
    //     country: CountryTypes
    // }
    // [MapRoutes.PIN_DETAILS]: {
    //     pin_id: string
    // }
    // // Feed: { sort: 'latest' | 'top' } | undefined
}

export interface IPinAddress {
    address: string
}

export interface IFocusPinData {
    // pin_id: string
    coordinates: number[]
}

export interface IPinFeature {
    name: string
    description: string
    tag: AllPinTypes
}

export interface IPinInfo {
    title: string
    sub_title: string
    description: string
    // address: IPinAddress
    type: AllPinTypes
    features: IPinFeature[]
    likes: number
    attendance: number
}

export interface IPinImage {
    source: string
    title: string
    description: string
}

export interface IPinFullInfo extends IPinInfo {
    images: IPinImage[]
    feed: string[]
}

export interface IPinLocation {
    _id: string,
    // pin_fire_id: string,
    // pin_mongo_id: string,
    latitude: number
    longitude: number
    info: IPinInfo
}

export interface IPinDetails {
    _id: string
    latitude: number
    longitude: number
    info: IPinFullInfo
}

export const samplePinFullInfo: IPinDetails[] = [
    {
        _id: '1',
        latitude: 58.9818,
        longitude: 14.6239,
        info: {
            title: 'Camping Tiveden',
            sub_title: 'Camping vid Vättern',
            description: 'Some information about this place',
            attendance: 0,
            features: [],
            likes: 0,
            type: PinTypesEnum.camping,
            feed: [],
            images: [
                {
                    description: '',
                    title: 'Image A',
                    source: 'http://placekitten.com/200/300'
                },
                {
                    description: '',
                    title: 'Image B',
                    source: 'http://placekitten.com/200/300'
                },
                {
                    description: '',
                    title: 'Image C',
                    source: 'http://placekitten.com/200/300'
                },
                {
                    description: '',
                    title: 'Image D',
                    source: 'http://placekitten.com/200/300'
                }
            ],
        },        
    },
    {
        _id: '2',
        latitude: 58.98289,
        longitude: 14.62391,
        info: {
            title: 'Kafferosteriet',
            sub_title: 'Kafe vid Tivedstorp',
            attendance: 0,
            description: 'Some information about this place',
            features: [
                {
                    description: 'Coffee shop',
                    name: 'Coffee shop',
                    tag: PinTypesEnum.store,
                }
            ],
            likes: 0,
            type: PinTypesEnum.cafe,
            feed: [],
            images: [
                {
                    description: '',
                    title: '',
                    source: 'http://placekitten.com/200/300'
                },
                {
                    description: '',
                    title: '',
                    source: 'http://placekitten.com/200/300'
                },
                {
                    description: '',
                    title: '',
                    source: 'http://placekitten.com/200/300'
                },
                {
                    description: '',
                    title: '',
                    source: 'http://placekitten.com/200/300'
                }
            ]
        },
    }
]