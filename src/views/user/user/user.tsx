import React, { useState } from 'react'
import { Layout, Text, Avatar, Divider } from '@ui-kitten/components'
import { TopNavigationComponent } from '../../../components/top-navigation/top-navigation'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackUserAccountRoutesParamList } from '../account/account-navigator'

type ScreenNavigationProp = StackNavigationProp<
    // RootStackUserAccountRoutesParamList,
    // UserAccountRoutes.ACCOUNT
    {},
    never
>

interface IProps {
    navigation: ScreenNavigationProp
}

export const UserScreen: React.FC<IProps> = ({
    navigation,
}) => {
    // TODO: Add Top bar with username and brief info as following etc.
    // TODO: Create actions to get to messages etc
    // TODO: Add posts or something in the body

    const auth = useSelector((s: IRootState) => s.auth)

    console.log('user screen')
    console.log(auth.avatar)

    const getAvatar = () => {
        if (auth.avatar === undefined ||
            auth.avatar === null ||
            auth.avatar.length <= 0) {
            return require('./../../../icons/hiking.png')
        } else {
            return {
                uri: auth.avatar
            }
        }
    }

    return (
        <Layout
            style={{
                flex: 1,
                height: '100%',
            }}
        >
            <TopNavigationComponent
                title={'User'}
                navigation={navigation}
                hasMenuAction={false}
            />
            <Layout style={{
                flexDirection: 'row',
            }}>
                <Layout
                    style={{
                        flexDirection: 'row',
                    }}
                >
                    <Avatar
                        style={{
                            margin: 8,
                        }}
                        size='giant'
                        source={getAvatar()}
                    />
                </Layout>
                <Layout style={{
                    flexDirection: 'row',
                    // alignItems: 'center',
                    // alignSelf: 'center',
                    justifyContent: 'space-evenly',
                    flex: 1,
                }}>
                    <Layout style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <Text>
                            0
                        </Text>
                        <Text>
                            Posts
                        </Text>
                    </Layout>
                    <Layout style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <Text>
                            0
                        </Text>
                        <Text>
                            Following
                        </Text>
                    </Layout>
                    <Layout style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <Text>
                            0
                        </Text>
                        <Text>
                            Followers
                        </Text>
                    </Layout>
                </Layout>
            </Layout>
            <Layout style={{
                marginLeft: 15,
                marginBottom: 10,
            }}>
                <Text>
                    {auth.username}
                </Text>
            </Layout>
            <Divider />
            <Layout
                style={{
                    flex: 1,
                }}
                level='2'
            >
                <Layout>
                    <Text>
                        Subscriptions
                    </Text>
                </Layout>
            </Layout>
        </Layout >
    )
}