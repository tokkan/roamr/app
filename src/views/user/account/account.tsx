import React from 'react'
import { Layout, Text, Card } from '@ui-kitten/components'
import { TopNavigationComponent } from './../../../components/top-navigation/top-navigation-stack'
import { RootStackUserAccountRoutesParamList } from './account-navigator'
import { StackNavigationProp } from '@react-navigation/stack'
import { UserAccountRoutes } from './../../../models/common/routes'

// type ScreenRouteProp = RouteProp<
//     RootStackUserAccountRoutesParamList,
//     UserAccountRoutes.ACCOUNT
// >

type ScreenNavigationProp = StackNavigationProp<
    RootStackUserAccountRoutesParamList,
    UserAccountRoutes.ACCOUNT
>

interface IProps {
    navigation: ScreenNavigationProp
}

export const UserAccountScreen: React.FC<IProps> = ({
    navigation,
}) => {

    // TODO: Add list or grid with items to navigate to, e.g. UserSubscriptions
    const navigateTo = (route: UserAccountRoutes) => {
        navigation.navigate(route)
    }

    const renderItem = () => {
        return (
            <Card>
                <Text></Text>
            </Card>
        )
    }

    return (
        <Layout
            style={{
                flex: 1,
            }}
        >
            <TopNavigationComponent
                title=''
                navigation={navigation}
            />
            <Text>
                Account screen
            </Text>
        </Layout>
    )
}