import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { UserAccountScreen } from './account'
import { UserSubscriptionsScreen } from './../../payment/subscriptions/user/subscriptions'
import { UserAccountRoutes } from './../../../models/common/routes'

const Stack = createStackNavigator<RootStackUserAccountRoutesParamList>()

export type RootStackUserAccountRoutesParamList = {
    [UserAccountRoutes.ACCOUNT]: undefined
    [UserAccountRoutes.BUSINESS]: undefined
    [UserAccountRoutes.SUBSCRIPTIONS]: undefined
    [UserAccountRoutes.GEO_OVERVIEW]: undefined
}

export const UserAccountNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={UserAccountRoutes.ACCOUNT}
            headerMode='none'
        >
            <Stack.Screen
                name={UserAccountRoutes.ACCOUNT}
                component={UserAccountScreen}
            />
            <Stack.Screen
                name={UserAccountRoutes.SUBSCRIPTIONS}
                component={UserSubscriptionsScreen}
            />
        </Stack.Navigator>
    )
}