import React from 'react'
import { Text, Layout } from '@ui-kitten/components'
import { FlatList } from 'react-native'

let data = [
    {
        _id: '1',
        content: 'item a',
    },
    {
        _id: '2',
        content: 'item b',
    },
    {
        _id: '3',
        content: 'item c',
    },
]

export const NotificationsScreen = () => {

    const renderItems = ({ item, index }: any) => {
        return (
            <Text>
                {item.content}
            </Text>
        )
    }

    return (
        <Layout style={{ flex: 1 }}>
            <Layout>
                <Text>Notifications</Text>
            </Layout>
            <Layout
                style={{
                    flex: 1,
                }}
                level='2'
            >
                <FlatList
                    style={{
                        width: '100%',
                    }}
                    data={data}
                    renderItem={renderItems}
                    keyExtractor={item => item._id}
                />
            </Layout>
        </Layout>
    )
}