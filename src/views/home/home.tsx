import React from 'react'
import {
    Layout
} from '@ui-kitten/components'
import { TopNavigationComponent } from '../../components/top-navigation/top-navigation'
import { HomeTopNavigator, HomeNavigator } from './home-top-tabs-navigator'

export const HomeScreen = ({ navigation }: any) => {

    const navigateDetails = () => {
        navigation.navigate('Details')
    }

    return (
        <Layout style={{ flex: 1 }}>
            {/* <TopNavigationComponent
                navigation={navigation}
                title='Explore'
                hasMenuAction={true}
            /> */}
            {/* <ExploreNavigator /> */}
            {/* <HomeTopNavigator /> */}
            <HomeNavigator />
        </Layout>
    )
}