import React, { useState, useRef } from 'react'
import { View, FlatList } from 'react-native'
import {
    Button, Layout, Text, Avatar, Input, ListItem, Divider
} from '@ui-kitten/components'
import {
    IFeedItem, IRenderCommentItem, IFeedComment
} from '..'
import { FilledLikeIcon, LikeIcon, CommentIcon, MessageIcon, MapIcon } from './../../../models'
import { useSelector } from 'react-redux'
import { IRootState } from './../../../store'
import { styles } from '../styles'

export const FeedItemFooter = (item: IFeedItem, props: any) => {

    const auth = useSelector((s: IRootState) => s.auth)
    const [isLiked, setIsLiked] = useState(false)

    const getAvatar = () => {
        if (auth.avatar === undefined ||
            auth.avatar === null ||
            auth.avatar.length <= 0) {
            return require('./../../../icons/hiking.png')
        } else {
            return {
                uri: auth.avatar
            }
        }
    }

    const LikeAction = (props: any) => {

        const [isLiked, setIsLiked] = useState(false)
        const pulseIconRef = useRef()

        return (
            <Button
                appearance='ghost'
                size='small'
                status='basic'
                accessoryLeft={isLiked ? FilledLikeIcon : LikeIcon}
                onPress={() => setIsLiked(!isLiked)}
            />
        )
    }

    const commentAvatar = (item: IFeedComment) => (
        // <Avatar
        //     style={styles.cardAvatar}
        //     size='tiny'
        //     source={{
        //         uri: 'http://placekitten.com/200/300'
        //     }}
        // />
        <Text category='p2' appearance='hint'>
            {item.username}
        </Text>
    )

    const renderCommentItems = ({ item, index }: IRenderCommentItem) => {
        return (
            <ListItem
                key={index}
                style={styles.cardCommentListItem}
                // title={item.comment}
                description={item.comment}
                accessoryLeft={() => commentAvatar(item)}
                accessoryRight={LikeAction}
            />
        )
    }

    return (
        <View>
            {item.likes.length > 0 ?
                <Layout style={styles.cardLikesContainer}>
                    <Text category='p2' appearance='hint'>
                        {`${item.likes.length} has liked`}
                    </Text>
                </Layout>
                : <View></View>}
            <View {...props} style={[props.style, styles.footerContainer]}>
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={isLiked ? FilledLikeIcon : LikeIcon}
                    onPress={() => setIsLiked(!isLiked)}
                />
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={CommentIcon}
                    onPress={() => console.log('Comment icon click')}
                />
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={MessageIcon}
                />
                <Button
                    style={styles.footerControl}
                    appearance='ghost'
                    size='medium'
                    status='basic'
                    accessoryLeft={MapIcon}
                />
            </View>
            <Layout style={styles.cardContentContainer}>
                <Text appearance='hint' style={{ justifyContent: 'space-between' }}>
                    <Text
                        category='p2'
                        style={{
                            marginRight: 5, paddingRight: 5
                        }}
                        onPress={() => console.log('click on username')}
                    >
                        {`${item.username} `}
                    </Text>
                    <Text appearance='hint'>
                        {item.content}
                    </Text>
                </Text>
            </Layout>
            <Layout style={styles.cardCommentContainer}>
                <FlatList
                    style={{ width: '100%' }}
                    data={item.comments}
                    renderItem={renderCommentItems}
                    ItemSeparatorComponent={Divider}
                    keyExtractor={item => item._id}
                />
                <Layout style={{
                    flexDirection: 'row',
                    // justifyContent: 'center',
                    // alignContent: 'center',
                    alignItems: 'center',
                    marginTop: 10
                }}>
                    <Avatar
                        style={styles.cardAvatar}
                        size='tiny'
                        source={getAvatar()}
                    />
                    <Input
                        style={{ flex: 1 }}
                        placeholder='Write comment...'
                    // value={values.search}
                    // onChangeText={onHandleChange('search')}
                    />
                </Layout>
            </Layout>
        </View>
    )
}