import React, { useState } from 'react'
import { Layout, Card, Avatar, Text, Divider, Button } from '@ui-kitten/components'
import { FlatList, Image, View } from 'react-native'
import { IFeedItem, sampleFeedItems, IRenderFeedItem } from '..'
import { FeedItemFooter } from './feed-item-footer'
import { VerticalDotsIcon } from './../../../models'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { styles } from '../styles'

const VerticalDotsAction = () => {
    return (
        <Button
            style={{
                padding: 0,
                margin: 0,
                width: 10,
            }}
            appearance='ghost'
            accessoryLeft={VerticalDotsIcon}
        />
    )
}

export const FeedScreen = () => {
    const auth = useSelector((s: IRootState) => s.auth)
    const [feedItems, setFeedItems] = useState<IFeedItem[]>(sampleFeedItems)

    const renderFeedItems = ({ item, index }: IRenderFeedItem) => {
        return (
            <>
                <View style={{
                    flexDirection: 'row',
                    paddingTop: 0,
                    paddingBottom: 0,
                    paddingLeft: 10,
                }}>
                    <Avatar
                        style={styles.cardAvatar}
                        size='tiny'
                        source={{
                            uri: item.avatar
                        }}
                    />
                    <Text category='s1' style={{ alignSelf: 'center' }}>
                        {item.username}
                    </Text>
                    <Layout
                        style={{
                            marginLeft: 'auto',
                        }}
                    >
                        <VerticalDotsAction />
                    </Layout>
                </View>
                <Layout
                    // style={styles.imageContainer}
                    style={{
                        flex: 1,
                        width: '100%',
                        // height: '100%',
                    }}
                    level='2'
                >
                    <Image
                        style={styles.cardImage}
                        source={{
                            uri: 'http://placekitten.com/200/300'
                            // uri: item.images[0]
                        }}
                        resizeMode='contain'
                    />
                </Layout>
                <FeedItemFooter
                    {...item}
                />
                <Divider />
            </>
        )
    }

    return (
        <Layout
            style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
            }}
        >
            <FlatList
                style={{
                    width: '100%',
                }}
                data={feedItems}
                renderItem={renderFeedItems}
                keyExtractor={item => item._id}
            />
        </Layout>
    )
}