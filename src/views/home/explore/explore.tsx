import React, { useState, useCallback, useContext, useEffect } from 'react'
import { Layout, Text, Divider, Button } from '@ui-kitten/components'
import { SuperCarousel } from './components/carousel/super-carousel'
import { CategoryCarousel } from './components/carousel/category-carousel'
import { AllPinTypes, PinTypesEnum, SearchIcon } from '../../../models'
import { i18n } from './../../../locales/localization'
import axios from 'axios'
import Geolocation, { GeolocationResponse, GeolocationOptions } from '@react-native-community/geolocation'
import { useFocusEffect, NavigationProp } from '@react-navigation/native'
import {
    askForLocationPermission
} from './../../../utils/geolocation/geolocation'
import { TouchableOpacity } from 'react-native'
import { ISocialEvent } from 'src/models/social/social-event/social-event'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'

export interface IPinCoordinate {
    latitude: number
    longitude: number
}

export interface IPinEvent {
    _id: string
    title: string
    sub_title: string
    description: string
    pin_type: AllPinTypes
    source: string
    location: IPinCoordinate
}

const samplePinEvents: IPinEvent[] = [
    {
        _id: 'a',
        title: 'Kafferosteriet',
        sub_title: 'Kafe vid Tivedstorp',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.cafe,
        location: {
            latitude: 58.98289,
            longitude: 14.62391,
        },
    },
    {
        _id: 'b',
        title: 'Camping Tiveden',
        sub_title: 'Camping vid Vättern',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.camping,
        location: {
            latitude: 58.9818,
            longitude: 14.6239,
        },
    },
    {
        _id: 'c',
        title: 'Kafferosteriet B',
        sub_title: 'Kafe vid Tivedstorp',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.cafe,
        location: {
            latitude: 58.98289,
            longitude: 14.62391,
        },
    },
    {
        _id: 'd',
        title: 'Kafferosteriet C',
        sub_title: 'Kafe vid Tivedstorp',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.cafe,
        location: {
            latitude: 58.98289,
            longitude: 14.62391,
        },
    },
    {
        _id: 'e',
        title: 'Kafferosteriet D',
        sub_title: 'Kafe vid Tivedstorp',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.cafe,
        location: {
            latitude: 58.98289,
            longitude: 14.62391,
        },
    }
]

export interface IPinSuggestion {
    pin_type: PinTypesEnum
    events: []
}

const category_suggestions: IPinSuggestion[] = [
    {
        pin_type: PinTypesEnum.camping,
        events: []
    },
    {
        pin_type: PinTypesEnum.nature_reserve,
        events: []
    },
    {
        pin_type: PinTypesEnum.restaurant,
        events: []
    },
    {
        pin_type: PinTypesEnum.store,
        events: []
    },
    {
        pin_type: PinTypesEnum.cafe,
        events: []
    },
]

type ScreenNavigationProp = NavigationProp<
    // RootStackUserAccountRoutesParamList,
    // UserAccountRoutes.ACCOUNT
    {},
    never
>

interface IProps {
    navigation: ScreenNavigationProp,
    action: any,
}

export const ExploreScreen = () => {
    // export const ExploreScreen: React.FC<IProps> = ({
    //     navigation,
    // }) => {
    // const [themeContext] = useContext(ThemeContext)
    const auth = useSelector((s: IRootState) => s.auth)
    const [pinEvents, setPinEvents] = useState<IPinEvent[]>(samplePinEvents)
    const [devicePosition, setDevicePosition] =
        useState<GeolocationResponse>()
    const [initialType] = useState('municipality')
    const [chosenZoneType, setChosenZoneType] =
        useState<'zone' | 'municipality'>('municipality')
    const [currentPlace, setCurrentPlace] = useState<{
        id: string,
        name: string,
        type: 'zone' | 'municipality'
    }>({ id: '', name: '', type: 'municipality', })
    const [eventItems, setEventItems] = useState<ISocialEvent[]>([])

    // TODO: Use animation when clicking a event or suggestion. Might be a good npm package for that, e.g. perhaps shared-element or something

    useFocusEffect(
        useCallback(() => {
            try {
                console.log('usefocusfx')
                let granted = askForLocationPermission()

                if (!granted) {
                    return
                }

                const geoLocationOptions: GeolocationOptions = {
                    timeout: 10000,
                    enableHighAccuracy: true,
                    maximumAge: undefined,
                }

                setTimeout(() => {
                    Geolocation.getCurrentPosition(
                        async (pos) => {
                            setDevicePosition(pos)
                            let res =
                                await getCurrentMunicipality(pos.coords.latitude,
                                    pos.coords.longitude)

                            setCurrentPlace({
                                id: res._id,
                                name: res.municipality_name,
                                type: 'municipality',
                            })
                        },
                        (err) => {
                            console.debug('Failed to getCurrentPosition')
                            console.debug(err)
                        }, geoLocationOptions
                    )
                }, 2000)
            } catch (error) {
                console.debug(error)
            }
        }, [])
    )

    useEffect(() => {
        if (currentPlace !== undefined &&
            currentPlace !== null &&
            currentPlace.id.length > 0) {
            getEvents()
        }
    }, [currentPlace])

    const getCurrentMunicipality = async (
        latitude: number,
        longitude: number,
    ) => {
        const type = 'municipality'
        try {
            let res =
                await axios.get(`/world/municipality-or-zone-by-coordinates?latitude=${latitude}&longitude=${longitude}&type=${type}`, {
                    headers: {
                        user_id: auth.id,
                        access_token: auth.facebook_access_token,
                        social_provider: auth.social_provider,
                    },
                })

            console.log('get municipality')
            console.log(res.data)

            if (res) {
                return {
                    ...res.data,
                    municipality_name: res.data.properties.municipality_name,
                    type: 'municipality',
                }
            }
            return {
                _id: '',
                municipality_name: 'Unknown',
                type: 'municipality',
            }
        } catch (error) {
            return {
                _id: '',
                municipality_name: 'Unknown',
                type: 'municipality',
            }
        }
    }

    const getEvents = async () => {
        try {
            let res =
                await axios.get(`social-events?geo_id=${currentPlace.id}&type=${currentPlace.type}`, {
                    headers: {
                        user_id: auth.id,
                        access_token: auth.facebook_access_token,
                        social_provider: auth.social_provider,
                    },
                })

            console.log(getEvents.name)
            console.log(res.data)

            if (res.data) {
                setEventItems(res.data)
            }
        } catch (error) {
            console.debug(error)
            return
        }
    }

    const onEventClick = async (id: string) => {
        console.log('event click')
        console.log(id)

        // TODO: Show detailed info about the event in modal or in new screen
    }

    const onCategoryClick = async (pin_type: PinTypesEnum) => {
        console.log('category click')
        console.log(pin_type)

        // TODO: Retrieve pins and events from API in the specified category, and open modal or new screen with the data
    }

    return (
        <Layout
            style={{
                flex: 1,
                height: '100%',
            }}
        >
            <Layout>
                <Layout style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    // marginTop: 4,
                }}>
                    <Text style={{
                        paddingTop: 10,
                        paddingBottom: 10,
                        marginRight: 10,
                        textAlign: 'center',
                    }}>
                        {`${i18n.t('explore.current_events')}`}
                    </Text>
                    {/* <Button style={{
                        // position: 'absolute',
                        // elevation: 5,
                        // borderRadius: 20,
                        // height: 5,
                        // paddingTop: 0,
                        // paddingBottom: 0,
                        marginTop: 5,
                        marginBottom: 5,
                        // width: 0,
                        // backgroundColor: 'green',
                        // borderColor: 'green',
                    }}
                        // accessoryLeft={SearchIcon}
                        appearance='ghost'
                    // onPress={toggle}
                    // title={currentPlace.name}
                    // title='hey'
                    >
                        {currentPlace.name}
                    </Button> */}
                    <TouchableOpacity>
                        <Text style={{
                            color: 'cornflowerblue',
                        }}>
                            {currentPlace.name}
                        </Text>
                    </TouchableOpacity>
                </Layout>
                <SuperCarousel
                    onItemClick={onEventClick}
                    // data={pinEvents}
                    data={eventItems}
                    divideItemWidthBy={1}
                />
            </Layout>
            {/* <Divider /> */}
            <Layout>
                <Text style={{
                    paddingTop: 10,
                    paddingBottom: 10,
                    textAlign: 'center',
                }}>
                    {i18n.t('explore.suggestions')}
                </Text>
                <CategoryCarousel
                    onItemClick={onCategoryClick}
                    data={category_suggestions}
                    divideItemWidthBy={3}
                />
            </Layout>
        </Layout>
    )
}