import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
    card: {
        // width: '100%',
        padding: 0,
        margin: 0,
    },
    cardContentContainer: {
        width: '100%'
    },
    cardLikesContainer: {
        paddingLeft: 10,
        paddingTop: 10,
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 10,
        paddingTop: 10,
        paddingLeft: 25,
        paddingRight: 25,
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        // width: 5,
        // height: 20,
        paddingLeft: 15,
        paddingRight: 0,
        paddingTop: 2,
        paddingBottom: 2,
    },
    footerLastContainer: {
        paddingBottom: 10,
        paddingTop: 10,
        paddingLeft: 25,
    },
    footerControl: {
        // marginHorizontal: 2,
        width: 5,
        margin: 0,
        paddingTop: 2,
        paddingBottom: 2,
    },
})

export const modalStyles = StyleSheet.create({
    card: {
        flex: 1,
        margin: 2,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
})