import React, { useState } from 'react'
import { AutocompleteItem, Autocomplete } from '@ui-kitten/components'

const data = [
    { title: 'Star Wars' },
    { title: 'Back to the Future' },
    { title: 'The Matrix' },
    { title: 'Inception' },
    { title: 'Interstellar' },
]

const filter = (item: any, query: string) => item.title.toLowerCase().includes(query.toLowerCase());

export const AutocompletePlace = () => {

    const [value, setValue] = useState('')
    const [autocompleteData, setAutocompleteData] =
        useState(data)
    const [autocompleteFilteredData, setAutocompleteFilteredData] =
        useState(autocompleteData)
        
    const onSelect = (index: number) => {
        setValue(autocompleteFilteredData[index].title)
    }

    const onChangeText = (query: string) => {
        setValue(query)
        setAutocompleteFilteredData(
            autocompleteData.filter(item => filter(item, query))
        )
    }

    const renderOption = (item: any, index: number) => (
        <AutocompleteItem
            key={index}
            title={item.title}
        />
    )

    return (
        <Autocomplete
            placeholder='Place your Text'
            value={value}
            onSelect={onSelect}
            onChangeText={onChangeText}>
            {autocompleteFilteredData.map(renderOption)}
        </Autocomplete>
    )
}