import React, { useRef, useState, useEffect } from 'react'
import Carousel, { ParallaxImage, Pagination } from 'react-native-snap-carousel'
import { Image, ImageBackground, Dimensions, View, TouchableOpacity } from 'react-native'
import { Text, Layout, Button } from '@ui-kitten/components'
import { railCarouselStyles } from './styles'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { AppThemes } from '../../../../../store/settings/actions'
import {
    GetComponentIcon, PinTypesEnum
} from '../../../../../models'

interface ICarouselRenderItem {
    item: IData
    index: number
}

interface IData {
    // title: string
    // description: string
    // source: string
    pin_type: PinTypesEnum,
    events: [],
}

interface IProps {
    onItemClick: (pin_type: PinTypesEnum) => void
    data: IData[]
    sliderContainerWidth?: number
    divideItemWidthBy?: number
}

export const CategoryCarousel: React.FC<IProps> = (
    {
        onItemClick,
        data,
        sliderContainerWidth,
        divideItemWidthBy = 1
    }
) => {

    const app_theme = useSelector((s: IRootState) => s.settings.app_theme)
    const ref = useRef()
    const [activeSlide, setActiveSlide] = useState(0)
    const [screenWidth, setScreenWidth] =
        useState(Dimensions.get('window').width)
    const [sliderWidth, setSliderWidth] =
        useState(sliderContainerWidth || screenWidth)
    const [itemWidth, setItemWidth] =
        useState((sliderContainerWidth || screenWidth) / divideItemWidthBy)
    const [dotColor, setDotColor] = useState<string>('black')

    useEffect(() => {
        setScreenWidth(Dimensions.get('window').width)
        setSliderWidth(sliderContainerWidth || screenWidth)
        setItemWidth((sliderContainerWidth || screenWidth)
            / divideItemWidthBy)
    }, [Dimensions])

    useEffect(() => {
        if (app_theme.use_system_theme ||
            app_theme.theme === AppThemes.LIGHT) {
            setDotColor('#000000')
        } else {
            setDotColor('#E6E6E6')
        }
    }, [app_theme])

    const renderItem = ({ item, index, ...rest }: ICarouselRenderItem) => {

        return (
            <Layout
                key={index}
                style={railCarouselStyles.itemContainer}
            >
                <Button
                    style={{
                        height: '100%',
                        width: '100%',
                    }}
                    appearance='ghost'
                    onPress={() => onItemClick(item.pin_type)}
                    accessoryLeft={() => GetComponentIcon(item.pin_type)}
                />
            </Layout>
        )
    }

    return (
        <Layout>
            <View style={railCarouselStyles.container}>
                <Carousel
                    style={railCarouselStyles.carousel}
                    layout='default'
                    ref={ref.current}
                    data={data}
                    renderItem={renderItem}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    hasParallaxImages={true}
                    onSnapToItem={(index) => setActiveSlide(index)}
                    loop={true}
                    enableSnap={true}
                />
                {/* <Pagination
                    carouselRef={ref.current}
                    dotsLength={data.length}
                    activeDotIndex={activeSlide}
                    dotColor={dotColor}
                    inactiveDotColor={dotColor}
                /> */}
            </View>
        </Layout>
    )
}