import React, { useRef, useState, useEffect } from 'react'
import Carousel, { ParallaxImage, Pagination } from 'react-native-snap-carousel'
import { Image, ImageBackground, Dimensions, View, TouchableOpacity } from 'react-native'
import { Text, Layout } from '@ui-kitten/components'
import { carouselStyles } from './styles'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { AppThemes } from '../../../../../store/settings/actions'

interface ICarouselRenderItem {
    item: IData
    index: number
}

interface IData {
    _id: string
    title: string
    description: string
    source: string
}

interface IProps {
    onItemClick: (id: string) => void
    data: IData[]
    sliderContainerWidth?: number
    divideItemWidthBy?: number
}

export const SuperCarousel: React.FC<IProps> = (
    {
        onItemClick,
        data,
        sliderContainerWidth,
        divideItemWidthBy = 1 
    }
) => {
    const app_theme = useSelector((s: IRootState) => s.settings.app_theme)
    const ref = useRef()
    const [activeSlide, setActiveSlide] = useState(0)
    const [screenWidth, setScreenWidth] =
        useState(Dimensions.get('window').width)
    const [sliderWidth, setSliderWidth] =
        useState(sliderContainerWidth || screenWidth)
    const [itemWidth, setItemWidth] =
        useState((sliderContainerWidth || screenWidth) / divideItemWidthBy)
    const [dotColor, setDotColor] = useState<string>('black')

    useEffect(() => {
        setScreenWidth(Dimensions.get('window').width)
        setSliderWidth(sliderContainerWidth || screenWidth)
        setItemWidth((sliderContainerWidth || screenWidth)
            / divideItemWidthBy)
    }, [Dimensions])

    useEffect(() => {
        if (app_theme.use_system_theme ||
            app_theme.theme === AppThemes.LIGHT) {
            setDotColor('#000000')
        } else {
            setDotColor('#E6E6E6')
        }
    }, [app_theme])

    const renderItem = ({ item, index, ...rest }: ICarouselRenderItem) => {
        return (
            <Layout
                key={index}
                style={carouselStyles.itemContainer}
            >
                {/* <ParallaxImage
                    containerStyle={{
                        flex: 1,
                    }}
                    style={carouselStyles.image}
                    parallaxFactor={0.35}
                    showSpinner={true}
                    spinnerColor={`rgba(0, 0, 0, 0.25)`}
                    source={{ uri: item.source }}
                    {...rest}
                /> */}
                <TouchableOpacity
                    style={{
                        height: '100%',
                        width: '100%',
                    }}
                    onPress={() => onItemClick(item._id)}
                >
                    <ImageBackground
                        style={carouselStyles.image}
                        source={{ uri: item.source }}
                        borderRadius={2}
                    >
                        <Layout
                            style={{
                                flexDirection: 'column',
                                backgroundColor: `rgba(0, 0, 0, 0.4)`,
                                // height: 100,
                                paddingTop: 10,
                                paddingLeft: 10,
                                paddingBottom: 5,
                                // shadowRadius: 120,
                                // shadowColor: 'black',
                                // shadowOffset: {
                                //     height: 10,
                                //     width: 10,
                                // },
                                // shadowOpacity: 1,
                            }}>
                            <Text>
                                {item.title}
                            </Text>
                        </Layout>
                    </ImageBackground>
                </TouchableOpacity>
            </Layout>
        )
    }

    return (
        <Layout>
            <View style={carouselStyles.container}>
                <Carousel
                    style={carouselStyles.carousel}
                    layout='default'
                    ref={ref.current}
                    data={data}
                    renderItem={renderItem}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    hasParallaxImages={true}
                    onSnapToItem={(index) => setActiveSlide(index)}
                />
                <Pagination
                    containerStyle={{
                        // height: 10,
                        paddingBottom: 15,
                        paddingTop: 15,
                    }}
                    carouselRef={ref.current}
                    dotsLength={data.length}
                    activeDotIndex={activeSlide}
                    dotColor={dotColor}
                    inactiveDotColor={dotColor}
                />
            </View>
        </Layout>
    )
}