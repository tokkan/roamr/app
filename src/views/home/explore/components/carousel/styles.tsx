import { StyleSheet } from "react-native"

export const carouselStyles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        paddingTop: 10,
        alignItems: 'center',
        // alignSelf: 'center',
        // maxHeight: 350,
    },
    carousel: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        height: 225,
        width: '100%',
    },
    itemContainer: {
        height: 225,
        width: '100%',
        // flex: 1,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        // height: 200,
        // width: 200,
    }
})

export const railCarouselStyles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        paddingTop: 10,
        alignItems: 'center',
        // alignSelf: 'center',
        // maxHeight: 350,
    },
    carousel: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        height: 200,
        width: '100%',
    },
    itemContainer: {
        height: 150,
        // height: '20%',
        width: '100%',
        // flex: 1,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        // height: 200,
        // width: 200,
    }
})