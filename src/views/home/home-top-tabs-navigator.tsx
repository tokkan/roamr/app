import React from 'react'
import { Tab, TabBar, Divider } from '@ui-kitten/components'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { TopNavigationComponent } from './../../components/top-navigation/top-navigation'
import { AddIcon } from './../../models'
import { StackNavigationProp, createStackNavigator } from '@react-navigation/stack'
import { RootStackExploreRoutesParamList } from '.'
import { NavigationProp } from '@react-navigation/native'
import { CreateEventScreen } from '../social/event/create-event'
import { CreatePostScreen } from '../social/posts/create/create-post'
import { FeedScreen } from './feed/feed'
import { ExploreScreen } from './explore/explore'
import { ExploreRoutes } from './../../models/common/routes'

const { Navigator, Screen } = createMaterialTopTabNavigator()
const Stack = createStackNavigator<RootStackExploreRoutesParamList>()

// type ScreenNavigationProp = StackNavigationProp<
type ScreenNavigationProp = NavigationProp<
    RootStackExploreRoutesParamList,
    ExploreRoutes
>

interface IProps {
    navigation: ScreenNavigationProp
    state: any
}

export const TopTabBar: React.FC<IProps> = ({
    navigation,
    state,
}) => {

    const TopNavigation = () => {
        if (state.index === 0) {
            return (
                <TopNavigationComponent
                    navigation={navigation}
                    title='Explore'
                    hasMenuAction={true}
                    rightAccessory={{
                        icon: AddIcon,
                        action: () => {
                            navigation.navigate('Create event')
                            // navigation.navigate(ExploreRoutes.CREATE_EVENT)
                        }
                    }}
                />
            )
        } else {
            return (
                <TopNavigationComponent
                    navigation={navigation}
                    title='Timeline'
                    hasMenuAction={true}
                    rightAccessory={{
                        icon: AddIcon,
                        action: () => navigation.navigate('Create post')
                        // action: () => navigation.navigate(ExploreRoutes.CREATE_POST)
                    }}
                />
            )
        }
    }

    return (
        <>
            <TopNavigation />
                <TabBar
                    // indicatorStyle={{
                    //     backgroundColor: 'yellow',
                    // }}
                    // style={{
                    //     backgroundColor: 'red',
                    // }}
                    selectedIndex={state.index}
                    // shouldLoadComponent={shouldLoadComponent}
                    onSelect={index => navigation.navigate(state.routeNames[index])}
                >
                    <Tab title='Explore' />
                    <Tab title='Feed' />
                </TabBar>
            <Divider />
        </>
    )
}

export const HomeNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={'Explore'}
            // initialRouteName={ExploreRoutes.EXPLORE}
            headerMode='none'
        >
            <Stack.Screen
                name={'Explore'}
                // name={ExploreRoutes.EXPLORE}
                component={HomeTopNavigator}
            />
            <Stack.Screen
                name={'Create event'}
                // name={ExploreRoutes.CREATE_EVENT}
                component={CreateEventScreen}
            />
            <Stack.Screen
                name={'Create post'}
                // name={ExploreRoutes.CREATE_POST}
                component={CreateEventScreen}
            />
        </Stack.Navigator>
    )
}

export const HomeTopNavigator = () => {
    return (
        <Navigator tabBar={(props: any) => <TopTabBar {...props} />}>
            <Screen name='Explore' component={ExploreScreen} />
            <Screen name='Feed' component={FeedScreen} />
        </Navigator>
    )
}