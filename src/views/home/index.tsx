export type RootStackExploreRoutesParamList = {
    ['Explore']: undefined
    ['Feed']: undefined
    ['Create event']: undefined
    ['Create post']: undefined
}

export interface IFeedComment {
    _id: string
    username: string
    avatar: any
    comment: string
    likes: number
    replies: []
}

export interface IFeedItem {
    _id: string
    username: string
    avatar: any
    content: string
    images: any[],
    comments: IFeedComment[],
    likes: any[]
    reshares: number
}

export interface IRenderFeedItem {
    item: IFeedItem
    index: number
}

export interface IRenderCommentItem {
    item: IFeedComment
    index: number
}

export const sampleFeedItems: IFeedItem[] = [
    {
        _id: '1',
        avatar: 'http://placekitten.com/200/300',
        comments: [
            {
                _id: '1',
                avatar: 'http://placekitten.com/200/300',
                comment: 'This is a comment',
                likes: 0,
                replies: [],
                username: 'User B'
            },
            {
                _id: '2',
                avatar: 'http://placekitten.com/200/300',
                comment: 'This is another comment',
                likes: 0,
                replies: [],
                username: 'User C'
            }
        ],
        content: 'My first post of a cat thinking about another new plan to conquer and dominate the planet we call earth',
        images: [
            'http://placekitten.com/200/300'
        ],
        likes: [
            {
                _id: 'u1',
                name: 'User A',
            },
            {
                _id: 'u2',
                name: 'User B',
            },
        ],
        reshares: 0,
        username: 'Super poster',
    },
    {
        _id: '2',
        avatar: 'http://placekitten.com/200/300',
        comments: [
            {
                _id: '3',
                avatar: 'http://placekitten.com/200/300',
                comment: 'This is a comment',
                likes: 0,
                replies: [],
                username: 'User B'
            },
            {
                _id: '4',
                avatar: 'http://placekitten.com/200/300',
                comment: 'This is another comment',
                likes: 0,
                replies: [],
                username: 'User C'
            }
        ],
        content: 'CAT POST',
        images: [
            'http://placekitten.com/200/300'
        ],
        likes: [],
        reshares: 0,
        username: 'Hyper poster',
    },
    {
        _id: '3',
        avatar: 'http://placekitten.com/200/300',
        comments: [
            {
                _id: '5',
                avatar: 'http://placekitten.com/200/300',
                comment: 'This is a comment',
                likes: 0,
                replies: [],
                username: 'User B'
            },
            {
                _id: '6',
                avatar: 'http://placekitten.com/200/300',
                comment: 'This is another comment',
                likes: 0,
                replies: [],
                username: 'User C'
            }
        ],
        content: 'KAT POST',
        images: [
            'http://placekitten.com/200/300'
        ],
        likes: [],
        reshares: 0,
        username: 'Ultima poster',
    }
]