import React, { useState } from 'react'
import { TabView, Tab, Layout } from '@ui-kitten/components'

export interface ITabComponent {
    title: string
    component: JSX.Element
}

interface IProps {
    tab_items: ITabComponent[]
}

// NOT USED CURRENTLY
// TODO: REMOVE THIS COMPONENT?

export const HomeTopTabs: React.FC<IProps> = ({ tab_items }) => {

    const [selectedIndex, setSelectedIndex] = useState(0)
    const shouldLoadComponent = (index: number) => index === selectedIndex

    return (
        // <TopNavigationComponent
        //     navigation={navigation}
        //     title='Explore'
        //     hasMenuAction={true}
        //     rightAccessory={{
        //         icon: SearchIcon,
        //         action: () => console.log('open search click')
        //     }}
        // />
        <TabView
            indicatorStyle={{
                // backgroundColor: 'yellow',
            }}
            tabBarStyle={{
                // backgroundColor: 'red',
                // borderBottomColor: 'yellow',
            }}
            selectedIndex={selectedIndex}
            // shouldLoadComponent={shouldLoadComponent}
            onSelect={index => setSelectedIndex(index)}
        >
            {tab_items.map((m, i) => (
                <Tab
                    key={i}
                    title={m.title}
                >
                    <Layout>
                        {m.component}
                    </Layout>
                </Tab>
            ))}
        </TabView>
    )
}