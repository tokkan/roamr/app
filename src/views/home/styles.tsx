import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
    topContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    card: {
        // backgroundColor: 'red',
        // flex: 1,
        // margin: 2,
        width: '100%',
        padding: 0,
        margin: 0,
    },
    imageContainer: {
        // backgroundColor: 'red',
        // width: '100%',
        // flex: 1,
        // justifyContent: 'center',
        // alignContent: 'stretch',
        // alignSelf: 'stretch',
        // alignItems: 'stretch',
        // width: 200,
        // height: 100,
    },
    cardImage: {
        width: '100%',
        // flex: 1,
        minHeight: 300,
        // height: 300,
    },
    cardAvatar: {
        marginRight: 10,
        alignSelf: 'center',
    },
    cardContentContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: 'row',
        textAlignVertical: 'center',
    },
    cardCommentContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
    },
    cardCommentListItem: {
        paddingTop: 0,
        paddingBottom: 0,
    },
    cardCommentListItemText: {
        // flex: 1,
    },
    cardLikesContainer: {
        paddingLeft: 10,
        paddingTop: 10,
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        // width: 5,
        // height: 20,
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 2,
        paddingBottom: 2,
    },
    footerControl: {
        // marginHorizontal: 2,
        width: 5,
        margin: 0,
        paddingTop: 2,
        paddingBottom: 2,
    },
})