import React, { useEffect, useState, useRef } from 'react'
import {
    View, TouchableOpacity,
} from 'react-native'
import {
    Button,
    Modal,
    Text,
    Card,
    Layout,
} from '@ui-kitten/components'
import Geolocation,
{
    GeolocationResponse
} from '@react-native-community/geolocation'
import axios from 'axios'
import MapboxGL, { Expression } from '@react-native-mapbox-gl/maps'
import {
    TopNavigationComponent
} from '../../../components/top-navigation/top-navigation'
import {
    styles, samplePinEvents, IPinEvent
} from '.'
import { useSelector } from 'react-redux'
import { IRootState } from '../../../store'
import {
    IPinLocation, IFocusPinData
} from '.'
// import { PinModal } from './modals/map-modal';
// import { SearchModal } from './modals/search/search-modal'
// import { initFieldsState, IStateFields } from './modals/search'
// MapboxGL.setAccessToken('pk.eyJ1IjoiZ2xlbm9sb2YiLCJhIjoiY2thNWc5NWN4MDk3czNmb2R5a201amczNSJ9.ifzXeflR-yHbIph8i_gJxg')
// MapboxGL.setAccessToken('pk.eyJ1IjoiZ2xlbm9sb2YiLCJhIjoiY2p3cXJpZ2tnMDVwdDN6cDZ0ZmVuZXo3YiJ9.WM8RjFIRFnKgHDKnPqul9A')
import { circle, bbox } from '@turf/turf'
import { SearchIcon, GetComponentIcon } from '../../../models'
import { ExploreCarousel } from './explore-carousel'
import { MapRoutes } from './../../../models/common/routes'

export const MapScreen = ({ navigation }: any) => {

    const auth = useSelector((s: IRootState) => s.auth)
    const mapRef = useRef<MapboxGL.MapView>(null)
    const cameraRef = useRef<MapboxGL.Camera>(null)
    const [source, setSource] = useState(axios.CancelToken.source())
    // const map_theme = useSelector((s: IRootState) => s.settings.map_theme)
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [isSearchModalVisible, setIsSearchModalVisible] = useState(false)
    const [isSearching, setIsSearching] = useState(false)
    const [modalItem, setModalItem] = useState<IPinLocation>()
    const [userPosition, setUserPosition] =
        useState<GeolocationResponse>()
    const [map_themes] = useState([
        'mapbox://styles/glenolof/ckai75i3y0n6t1io1cqlx59j5',
        MapboxGL.StyleURL.Dark.toString(),
    ])
    // const [mtheme, setmtheme] = useState(MapboxGL.StyleURL.Dark.toString())
    const [mtheme, setmtheme] =
        useState('mapbox://styles/glenolof/ckai75i3y0n6t1io1cqlx59j5')
    const [recommendedPins, setRecommendedPins] = useState<IPinEvent[]>([])
    const [layerIds, setLayerIds] = useState<string[]>([
        'roamr-icons',
        'tiveden-cafe',
        'tiveden-store',
        'tiveden-restaurant',
        'tiveden-trails',
        'tiveden-areas',
    ])

    useEffect(() => {
        MapboxGL.setAccessToken(auth.mapbox_access_token)

        // if (auth.access_token.length > 0) {
        //     MapboxGL.setTelemetryEnabled(false)
        //     MapboxGL.setConnected(true)
        // }
    }, [auth.mapbox_access_token])

    useEffect(() => {
        setRecommendedPins(samplePinEvents)
        onFocusPin({
            coordinates: [
                samplePinEvents[0].location.longitude,
                samplePinEvents[0].location.latitude,
            ]
        })
        Geolocation.getCurrentPosition((pos) => {
            setUserPosition(pos)
            console.log(pos)
        })
    }, [])

    useEffect(() => {

        const focus = navigation.addListener('focus', () => {
            setSource(axios.CancelToken.source())
        })

        const unsubscribe = navigation.addListener('blur', () => {
            source.cancel()
        })

        return () => {
            focus
            unsubscribe
            source.cancel()
        }
    }, [navigation])

    // const forceUpdateMap = () => {
    //     // let style = mapRef.current?.props.styleURL
    //     // console.log(style)
    //     // mapRef.current?.setSourceVisibility(true, 'composite', 'tiveden-cafe')
    //     // // mapRef.current?.render()
    //     // mapRef.current?.forceUpdate(() => {
    //     //     console.log('force update')
    //     // })
    //     setmtheme(map_themes[0])
    // }

    const onNavigate = (route: MapRoutes) => {
        setIsModalVisible(false)
        navigation.navigate(route, {
            pin_id: modalItem?._id
        })
    }

    const onUserUpdate = async (data: any) => {
        console.log('on user update')
        console.log(data)
    }

    const onMapClick = async (data: {
        geometry: {
            coordinates: number[],
            type: string,
        },
        properties: {
            screenPointX: number,
            screenPointY: number,
        },
        type: string,
    }) => {
        try {
            console.log('map click')
            console.log(data)
            let sp =
                await mapRef.current?.getPointInView(
                    data.geometry.coordinates
                    // [
                    //     data.properties.screenPointX,
                    //     data.properties.screenPointY
                    // ]
                )
            console.log(sp)

            let bounds = await mapRef.current?.getVisibleBounds()
            console.log('bounds')
            console.log(bounds)

            let res =
                await mapRef.current?.queryRenderedFeaturesAtPoint(
                    // data.geometry.coordinates,
                    [
                        data.properties.screenPointX,
                        data.properties.screenPointY
                    ],
                    [
                        '!=', 'type', 'lol'
                    ],
                    layerIds
                )

            // setLemonBool(!lemonBool)

            // disables all and can't flip them back for some reason
            // mapRef.current?.setSourceVisibility(
            //     lemonBool,
            //     'composite',
            //     'tiveden-restaurant'
            // )

            console.log('Results...')
            console.log(res)
            if (res.features.length > 0) {
                console.log(res.features[0].geometry)
                console.log(res.features[0].properties)
            }
        } catch (error) {
            console.error(error)
        }
    }

    // const onSearch = async (values: IStateFields) => {
    //     try {
    //         setIsSearching(true)
    //         console.log('on search')
    //         console.log(values)

    //         let center: number[] = await mapRef.current?.getCenter()
    //         // let viewCenter: number[] =
    //         //     await mapRef.current?.getPointInView(center)
    //         console.log(center)
    //         // console.log(viewCenter)
    //         let area = circle(center, 5, {
    //             units: 'kilometers',
    //             steps: 10,
    //         })

    //         // console.log(area.geometry?.coordinates)

    //         let tbbox = bbox(area)
    //         console.log('tbbox')
    //         console.log(tbbox)

    //         let b1 =
    //             await mapRef.current?.getPointInView([tbbox[0], tbbox[1]])
    //         let b2: number[] =
    //             await mapRef.current?.getPointInView([tbbox[2], tbbox[3]])

    //         // let b1: number[] =
    //         //     await mapRef.current?.getPointInView([tbbox[1], tbbox[0]])
    //         // let b2: number[] =
    //         //     await mapRef.current?.getPointInView([tbbox[3], tbbox[2]])

    //         console.log('getpointinview')
    //         console.log(b1)
    //         console.log(b2)

    //         let plain_bbox = [
    //             b1[0] * 3, b1[1] * 3, b2[0] * 3, b2[1] * 3
    //         ]
    //         console.log(plain_bbox)

    //         let expr: Expression = [
    //             // '!=', 'type', 'lol', 
    //             'all',
    //             ['!=', 'pin_type', 'AREA'],
    //             ['!=', 'type', 'lol'],
    //         ]

    //         let res = await mapRef.current?.queryRenderedFeaturesInRect(
    //             plain_bbox,
    //             expr,
    //             layerIds
    //         )

    //         console.log('Result...')
    //         console.log(res)
    //         if (res.features.length > 0) {
    //             res.features.map((f: any) => {
    //                 console.log(f.geometry)
    //                 console.log(f.properties)
    //             })
    //         }

    //         setIsSearchModalVisible(false)
    //         setIsSearching(false)
    //     } catch (error) {
    //         console.debug(error)
    //         setIsSearchModalVisible(false)
    //         setIsSearching(false)
    //     }
    // }

    // const onSearch = async (values: IStateFields) => {
    //     try {
    //         setIsRequesting(true)

    //         let response =
    //             await axios.get(`/organizations/business`,
    //                 {
    //                     cancelToken: source.token
    //                 })

    //         if (response) {
    //             console.log(response.data)
    //             setIsRequesting(false)
    //             setBusinessData(response.data)
    //         } else {
    //             setIsRequesting(false)
    //             source.cancel('Operation cancelled by failing getting response')
    //         }

    //     } catch (error) {
    //         setIsRequesting(false)

    //         if (axios.isCancel(error)) {
    //             if (__DEV__) {
    //                 console.log('HTTP request cancelled', error.message)
    //             }
    //         }

    //         if (__DEV__) {
    //             console.log(error)
    //         }
    //     }
    // }

    const onFocusPin = (values: IFocusPinData) => {
        console.log(onFocusPin.name)
        console.log(values)
        setIsSearchModalVisible(false)
        cameraRef.current?.moveTo(values.coordinates, 200)
    }

    const MapPin = (item: IPinLocation) => {
        return (
            <MapboxGL.PointAnnotation
                id={`pin-${item._id}`}
                key={item._id}
                coordinate={[
                    item.longitude,
                    item.latitude,
                ]}
                onSelected={() => {
                    setModalItem(item)
                    setIsModalVisible(true)
                }}
            >
                {GetComponentIcon(item.info.type)}
            </MapboxGL.PointAnnotation>
        )
    }

    const SearchFAB = () => {
        return (
            <Button style={{
                position: 'absolute',
                bottom: 50,
                left: 20,
                borderRadius: 500,
                height: 50,
                width: 50,
                backgroundColor: 'green',
                borderColor: 'green',
            }}
                accessoryLeft={SearchIcon}
                onPress={() => setIsSearchModalVisible(true)}
            />
        )
    }

    // const UpdateFAB = () => {
    //     return (
    //         <Button style={{
    //             position: 'absolute',
    //             bottom: 120,
    //             left: 20,
    //             borderRadius: 500,
    //             height: 50,
    //             width: 50,
    //             backgroundColor: 'green',
    //             borderColor: 'green',
    //         }}
    //             accessoryLeft={SearchIcon}
    //             onPress={forceUpdateMap}
    //         />
    //     )
    // }

    return (
        <>
            <TopNavigationComponent
                navigation={navigation}
                title='Explore'
                hasMenuAction={true}
            />
            <View style={styles.page}>
                <View style={styles.mapContainer}>
                    <MapboxGL.MapView
                        ref={mapRef}
                        onDidFailLoadingMap={() => {
                            console.log('failed loading map')
                        }}
                        onDidFinishLoadingMap={() => {
                            console.log('on did finished loading map')
                        }}
                        onDidFinishLoadingStyle={() => {
                            console.log(3)
                        }}
                        onLayout={() => {
                            console.log('on layout')
                        }}
                        // styleURL={map_theme}
                        // styleURL={`mapbox://styles/glenolof/ckai75i3y0n6t1io1cqlx59j5`}
                        // styleURL='mapbox://styles/glenolof/ckai75i3y0n6t1io1cqlx59j5'
                        styleURL={mtheme}
                        style={styles.map}
                        onPress={onMapClick}
                    >
                        <MapboxGL.Camera
                            ref={cameraRef}
                            zoomLevel={12}
                            centerCoordinate={[
                                userPosition?.coords.longitude,
                                userPosition?.coords.latitude
                            ]}
                            // bounds={{
                            //     ne: [0,0], 
                            //     sw: [0,0], 
                            //     paddingLeft: 0,
                            //     paddingRight: 0,
                            //     paddingTop: 0,
                            //     paddingBottom: 1500,
                            // }}
                            // alignment={[]}
                        />
                        {/* <PinModal
                            navigation={navigation}
                            isModalVisible={isModalVisible}
                            setIsModalVisible={setIsModalVisible}
                            modalItem={modalItem}
                        />
                        <SearchModal
                            navigation={navigation}
                            isModalVisible={isSearchModalVisible}
                            setIsModalVisible={setIsSearchModalVisible}
                            mapRef={mapRef}
                            onFocusPin={onFocusPin}
                        // onSearch={onSearch}
                        /> */}
                    </MapboxGL.MapView>
                    <Layout
                        style={{
                            backgroundColor: 'transparent',
                            position: 'absolute',
                            bottom: 50,
                            // left: 30,
                            // right: 30,
                        }}
                    >
                        <ExploreCarousel
                            data={recommendedPins}
                            divideItemWidthBy={1}
                            onFocusPin={onFocusPin}
                        />
                    </Layout>
                    {/* <UpdateFAB />
                    <SearchFAB /> */}
                </View>
            </View>
        </>
    )
}