import React from 'react'
import { StyleSheet, ImageProps } from "react-native"
import { Icon } from '@ui-kitten/components'
import { AllPinTypes, PinTypesEnum } from '../../../models'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        textAlign: 'center',
    },
    likeButton: {
        marginVertical: 16,
    },
    page: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF"
    },
    mapContainer: {
        // height: 300,
        // width: 300,
        height: '100%',
        width: '100%',
    },
    map: {
        flex: 1
    },
    mapPinIcon: {
        width: 0,
        height: 0,
        margin: 0,
        padding: 0,
        paddingTop: 0,
        paddingBottom: 0,
    },
    popoverContent: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 4,
        paddingVertical: 8,
    },
    avatar: {
        marginHorizontal: 4,
    },

})

export const modalStyles = StyleSheet.create({
    card: {
        flex: 1,
        margin: 2,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
})

// export const carouselStyles = StyleSheet.create({
//     container: {
//         backgroundColor: 'rgba(0, 0, 0, 0.1)',
//         paddingTop: 10,
//         alignItems: 'center',
//         // alignSelf: 'center',
//         // maxHeight: 350,
//     },
//     carousel: {
//         flex: 1,
//         justifyContent: 'center',
//         flexDirection: 'row',
//         height: 225,
//         width: '100%',
//     },
//     itemContainer: {
//         height: 225,
//         width: '100%',
//         // flex: 1,
//     },
//     image: {
//         ...StyleSheet.absoluteFillObject,
//         resizeMode: 'contain',
//         flexDirection: 'column',
//         justifyContent: 'flex-end',
//         // height: 200,
//         // width: 200,
//     }
// })

export const carouselStyles = StyleSheet.create({
    container: {
        // backgroundColor: 'rgba(0, 0, 0, 0.1)',
        backgroundColor: 'transparent',
        // backgroundColor: 'red',
        // paddingTop: 10,
        // alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        // alignSelf: 'center',
        // maxHeight: 350,
    },
    carousel: {
        // backgroundColor: 'transparent',
        backgroundColor: 'blue',
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        height: 200,
        width: '100%',
    },
    itemContainer: {
        width: '60%',
        alignSelf: 'center',
        borderRadius: 3,
    },
    itemImageContainer: {
        backgroundColor: 'transparent',
        height: 200,
        // height: '20%',
        // width: '60%',
        width: '100%',
        // alignSelf: 'center',
        // justifyContent: 'center',
        // flex: 1,
    },
    itemActionContainer: {
        flexDirection: 'row',
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        backgroundColor: 'transparent',
        // height: 200,
        // width: 200,
    }
})

export interface IFocusPinData {
    // pin_id: string
    coordinates: number[]
}

export interface IPinFeature {
    name: string
    description: string
    tag: AllPinTypes
}

export interface IPinInfo {
    title: string
    sub_title: string
    description: string
    // address: IPinAddress
    type: AllPinTypes
    features: IPinFeature[]
    likes: number
    attendance: number
}

export interface IPinLocation {
    _id: string,
    latitude: number
    longitude: number
    info: IPinInfo
}

export interface IPinCoordinates {
    latitude: number
    longitude: number
}

export interface IPinEvent {
    title: string
    sub_title: string
    description: string
    pin_type: AllPinTypes
    source: string
    location: IPinCoordinates
}

export const samplePinEvents: IPinEvent[] = [
    {
        title: 'Kafferosteriet',
        sub_title: 'Kafe vid Tivedstorp',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.cafe,
        location: {
            latitude: 58.77199803484994,
            longitude: 14.53233316540718,
        },
    },
    {
        title: 'Camping Tiveden',
        sub_title: 'Camping vid Vättern',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.camping,
        location: {
            latitude: 58.9818,
            longitude: 14.6239,
        },
    },
    {
        title: 'Luripompa',
        sub_title: 'Restaurang vid Tivedstorp',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.restaurant,
        location: {
            latitude: 58.7717129652278,
            longitude: 14.532787799835203,
        },
    },
    {
        title: 'Tivedens Nationalpark',
        sub_title: 'Nationalpark i Tiveden',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.nature_reserve,
        location: {
            latitude: 58.715696642275425,
            longitude: 14.614954590797424,
        },
    },
    {
        title: 'Lanthandel Tiveden',
        sub_title: 'Lanthandel vid Tivedstorp',
        description: 'Some information about this place',
        source: 'http://placekitten.com/200/300',
        pin_type: PinTypesEnum.store,
        location: {
            latitude: 58.77218854348848,
            longitude: 14.532455205917357,
        },
    }
]