import React, { useRef, useState, useEffect } from 'react'
import Carousel, { ParallaxImage, Pagination } from 'react-native-snap-carousel'
import { Image, ImageBackground, Dimensions, View, TouchableOpacity } from 'react-native'
import { Text, Layout, Button } from '@ui-kitten/components'
import { carouselStyles, IFocusPinData, IPinCoordinates, IPinEvent } from '.'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { AppThemes } from '../../../store/settings/actions'
import { LikeIcon, PinIcon } from '../../../models'

interface ICarouselRenderItem {
    // item: IData
    item: IPinEvent
    index: number
}

interface IData {
    title: string
    description: string
    source: string
    coordinates: IPinCoordinates
}

interface IProps {
    data: IPinEvent[]
    sliderContainerWidth?: number
    divideItemWidthBy?: number
    onFocusPin: (values: IFocusPinData) => void
}

export const ExploreCarousel: React.FC<IProps> = (
    {
        data,
        sliderContainerWidth,
        divideItemWidthBy = 1,
        onFocusPin,
    }
) => {

    const app_theme = useSelector((s: IRootState) => s.settings.app_theme)
    const ref = useRef()
    const [activeSlide, setActiveSlide] = useState(0)
    const [screenWidth, setScreenWidth] =
        useState(Dimensions.get('window').width)
    const [sliderWidth, setSliderWidth] =
        useState(sliderContainerWidth || screenWidth)
    const [itemWidth, setItemWidth] =
        useState((sliderContainerWidth || screenWidth) / divideItemWidthBy)
    const [dotColor, setDotColor] = useState<string>('black')

    useEffect(() => {
        setScreenWidth(Dimensions.get('window').width)
        setSliderWidth(sliderContainerWidth || screenWidth)
        setItemWidth((sliderContainerWidth || screenWidth)
            / divideItemWidthBy)
    }, [Dimensions])

    useEffect(() => {
        if (app_theme.use_system_theme ||
            app_theme.theme === AppThemes.LIGHT) {
            setDotColor('#000000')
        } else {
            setDotColor('#E6E6E6')
        }
    }, [app_theme])

    const focusPin = (index: any) => {
        console.debug('focuspin')
        // console.debug(event)
        console.debug(data[index])
        setActiveSlide(index)
        onFocusPin({
            coordinates: [
                data[index].location.longitude,
                data[index].location.latitude,
            ]
        })
    }

    const renderItem = ({ item, index, ...rest }: ICarouselRenderItem) => {

        return (
            <Layout style={carouselStyles.itemContainer}>
                <Layout
                    key={index}
                    style={[
                        // carouselStyles.itemContainer,
                        carouselStyles.itemImageContainer,
                    ]}
                >
                    {/* <Button
                    style={{
                        height: '100%',
                        width: '100%',
                    }}
                    appearance='ghost'
                    onPress={() => console.log('click')}
                    accessoryLeft={LikeIcon}
                /> */}
                    <ImageBackground
                        style={carouselStyles.image}
                        source={{ uri: item.source }}
                        borderRadius={2}
                    >
                        <Layout
                            style={{
                                flexDirection: 'column',
                                backgroundColor: `rgba(0, 0, 0, 0.4)`,
                                // height: 100,
                                paddingTop: 10,
                                paddingLeft: 10,
                                paddingBottom: 5,
                                // shadowRadius: 120,
                                // shadowColor: 'black',
                                // shadowOffset: {
                                //     height: 10,
                                //     width: 10,
                                // },
                                // shadowOpacity: 1,
                            }}>
                            <Text>
                                {item.title}
                            </Text>
                        </Layout>
                    </ImageBackground>
                </Layout>
                <Layout
                    style={[
                        carouselStyles.itemActionContainer
                    ]}
                >
                    <Button
                        style={{
                            marginLeft: 10,
                            width: 15,
                            // height: '100%',
                            // width: '100%',
                        }}
                        appearance='ghost'
                        onPress={() => focusPin(item)}
                        accessoryLeft={PinIcon}
                    />
                    <Button
                        style={{
                            width: 15,
                            // height: '100%',
                            // width: '100%',
                        }}
                        appearance='ghost'
                        onPress={() => console.log('click')}
                        accessoryLeft={LikeIcon}
                    />
                </Layout>
            </Layout>
        )
    }

    return (
        // <Layout
        //     style={{
        //         // backgroundColor: 'transparent',
        //         // backgroundColor: 'red',
        //         flexDirection: 'column',
        //         // justifyContent: 'center',
        //         // maxWidth: '100%',
        //     }}
        // >
        <Layout style={carouselStyles.container}>
            <Carousel
                style={carouselStyles.carousel}
                layout='default'
                ref={ref.current}
                data={data}
                renderItem={renderItem}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                hasParallaxImages={true}
                loop={true}
                enableSnap={true}
                onSnapToItem={(index) => focusPin(index)}
            // onSnapToItem={(index) => setActiveSlide(index)}
            // onScrollEndDrag={(event) => focusPin(event)}
            />
            <Layout
                style={{
                    width: '60%',
                    // width: itemWidth,
                    flexDirection: 'column',
                    // justifyContent: 'center',
                    alignSelf: 'center',
                }}
            >
                <Pagination
                    dotContainerStyle={{
                        // backgroundColor: 'blue',
                        // width: sliderWidth,
                        // maxWidth: sliderWidth,
                    }}
                    containerStyle={{
                        // backgroundColor: 'blue',
                        // width: sliderWidth,
                        // maxWidth: sliderWidth,
                        paddingTop: 15,
                        paddingBottom: 15,
                    }}
                    carouselRef={ref.current}
                    dotsLength={data.length}
                    activeDotIndex={activeSlide}
                    dotColor={dotColor}
                    inactiveDotColor={dotColor}
                />
            </Layout>
        </Layout>
        // </Layout>
    )
}