import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
// import { MapScreen } from './../../map/map/map'
import { MapScreen } from './explore-map'
import { PinDetailsScreen } from '../../map/pin-details/pin-details'
import { MapRoutes } from 'src/models/common/routes'

const Stack = createStackNavigator<Record<MapRoutes, object | undefined>>()

export const ExploreNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName='Map'
            headerMode='none'
        >
            <Stack.Screen
                name={'Map'}
                // name={MapRoutes.MAP}
                component={MapScreen}
            />
            <Stack.Screen
                name={'Pin details'}
                component={PinDetailsScreen}
            />
        </Stack.Navigator>
    )
}