import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    containerItem: {
        paddingTop: 20,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        marginTop: 20,
    },
    registerButton: {
        marginTop: 50,
    },
    radioContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingLeft: 10,
        paddingTop: 15,
        paddingBottom: 10,
    },
    radio: {
        margin: 2,
    },
})