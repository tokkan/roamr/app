import React, { useState, useEffect } from 'react'
import { View, ScrollView, TouchableOpacity, Pressable } from 'react-native'
import {
    Layout, Text, Input, Button, Spinner, Icon, IndexPath, Select, SelectItem, Card, RadioGroup, Radio, RangeDatepicker, CalendarRange
} from '@ui-kitten/components'
// import { object, string, mixed } from 'yup'
import {
    object as yupobject,
    mixed as yupmixed,
    string as yupstring,
    number as yupnumber,
    array as yuparray,
    string,
} from 'yup'
import { Formik } from 'formik'
import { i18n } from '../../../locales/localization'
import { useSelector } from 'react-redux'
import axios from 'axios'
import { StackNavigationProp } from '@react-navigation/stack'
import { RouteProp } from '@react-navigation/native'
import {
    TopNavigationComponent
} from '../../../components/top-navigation/top-navigation-stack'
import { InfoIcon, AllPinTypes, SocialMediaTypes, PinTypesEnum, AddIcon, CloseIcon, EditIcon, TrashIcon, CalendarIcon } from '../../../models'
import { IRootState } from 'src/store'
import { RootStackExploreRoutesParamList } from '../../home'
import { styles } from './styles'
import { ImageModal } from '../posts/create/image-modal'
import { Image } from 'react-native-image-crop-picker'
import { ImagePreviewCarousel } from '../posts/create/image-carousel-preview'
import { EditStringModal } from '../../../components/modal/common/edit/edit-string-modal'
import { ExploreRoutes } from './../../../models/common/routes'

interface ICreateBusinessPayload {
    zone_id: string
    businessName: string
    pin_type: AllPinTypes
    sub_pin_types: AllPinTypes[]
    sub_title: string
    description: string
    latitude: number
    longitude: number
    emailAddress: string
    phoneNumber: string
    socialMediaHandle: string
    socialMediaProvider: SocialMediaTypes
}

export interface IStateFields {
    title: string
    // content: string
    // description: string[]
    // images: any[]
    pin_type: PinTypesEnum
}

export const initStateFields: IStateFields = {
    title: '',
    // content: '',
    // description: [],
    // images: [],
    pin_type: PinTypesEnum.poi,
}

const validationSchema = yupobject().shape<IStateFields>({
    title: yupstring()
        .min(3)
        .required(),
    // description: yup.array().of(
    //     yup.string()
    //         .max(100)
    //         .optional()),
    // content: yupstring()
    //     .min(0)
    //     .required(),
    // description: yuparray(
    //     yupstring()
    //         .required())
    //     .required(),
    // images: yuparray(
    //     yupmixed()
    //         .required())
    //     .required(),
    pin_type: yupmixed()
        .oneOf(Object.values(PinTypesEnum))
        .required(),
})

const options = Object.entries(PinTypesEnum)

type ScreenRouteProp = RouteProp<
    RootStackExploreRoutesParamList,
    // ExploreRoutes.CREATE_EVENT
    'Create event'
>

type ScreenNavigationProp = StackNavigationProp<
    RootStackExploreRoutesParamList,
    // ExploreRoutes.CREATE_EVENT
    'Create event'
>

interface IProps {
    route: ScreenRouteProp
    navigation: ScreenNavigationProp
}

// TODO: Change first select depending on if radio button says business, zone or municipality
export const CreateEventScreen: React.FC<IProps> = ({
    route,
    navigation,
}) => {
    const auth = useSelector((s: IRootState) => s.auth)
    // const [source, setSource] = useState(axios.CancelToken.source())
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [selectedProvider, setSelectedProvider] = useState(0)
    const [isCheckboxDisabled, setIsCheckboxDisabled] = useState({
        business: true,
        zone: false,
        municipality: false,
    })
    const [businesses, setBusinesses] = useState<{
        _id: string,
        name: string,
    }[]>([])
    const [content, setContent] = useState('')
    const [contentArray, setContentArray] = useState<string[]>([])
    const [currentContentArrayItem, setCurrentContentArrayItem] =
        useState<{
            id: string | number
            text: string
        }>({
            id: 0,
            text: '',
        })
    const [isEditModalVisible, setIsEditModalVisible] = useState(false)
    const [isImageModalVisible, setIsImageModalVisible] = useState(false)
    const [assets, setAssets] = useState<Image[]>([])
    const [businessIndex, setBusinessIndex] = useState(new IndexPath(0))
    const [selectedBusiness, setSelectedBusiness] = useState<{
        _id: string
        name: string
    }>({
        _id: '',
        name: '',
    })
    const [pinTypeIndex, setPinTypeIndex] =
        useState(new IndexPath(0))
    const [pinTypeOptions] = useState(options)
    const pinTypeDisplayValue = pinTypeOptions[pinTypeIndex.row]
    const [range, setRange] = useState<CalendarRange<Date>>({})

    useEffect(() => {
        checkUserProviders()
        getBusinesses()
    }, [])

    const checkUserProviders = async () => {
        setIsCheckboxDisabled({
            business: false,
            zone: false,
            municipality: false,
        })
    }

    const getBusinesses = async () => {
        try {
            let response =
                await axios.get(`business/${auth.id}`,
                    {
                        headers: {
                            user_id: auth.id,
                            access_token: auth.facebook_access_token,
                            social_provider: auth.social_provider,
                        },
                        // cancelToken: source.token
                    })

            if (response.data &&
                response.data.length > 0) {
                let b: {
                    _id: string
                    name: string
                }[] = response.data.map((m: any) => {
                    return {
                        _id: m._id,
                        name: m.name,
                    }
                })

                console.log(response.data)

                setBusinesses(b)
                setSelectedBusiness(b[0])
            } else {
                navigation.goBack()
                setBusinesses([])
            }
        } catch (error) {
            setBusinesses([])
            return
        }
    }

    const isFieldValid = (
        fieldValue: keyof IStateFields,
        errorValue: string | undefined,
        touched: boolean | undefined
    ) => {
        if (errorValue === undefined) {
            return true
        } else if (fieldValue.length > 0 && errorValue !== undefined) {
            return false
        } else {
            return false
        }
    }

    const setBusinessSelect = (index: any) => {
        setBusinessIndex(index)
        setSelectedBusiness(businesses[index - 1])
    }

    const setPinTypeSelect = (index: any) => {
        setPinTypeIndex(index)
    }

    const addText = (content: string) => {
        if (contentArray.length < 3 &&
            content.length > 0) {
            setContentArray([...contentArray, content])
            setContent('')
        }
    }

    const removeAsset = (item: Image) => {
        setAssets(assets.filter(f => f.path !== item.path))
    }

    const openPicker = (files: Image[]) => {
        if (assets.length + files.length > 3) {
            return
        } else {
            setAssets([...assets, ...files])
        }
    }

    const openPickerModal = () => {
        if (assets.length > 3) {
            return
        } else {
            setIsImageModalVisible(true)
        }
    }

    const onSubmit = async (values: IStateFields) => {
        try {
            setIsSubmitting(true)

            if (contentArray.length <= 0 || assets.length <= 0) {
                setIsSubmitting(false)
                // TODO: Show modal of the reason why it errored
                return
            }

            let assetsPayload =
                assets.filter(m => {
                    if (m.data !== null) {
                        return {
                            base64: m.data,
                            width: m.width,
                            height: m.height,
                            size: m.size,
                            mime: m.mime,
                        }
                    }
                })

            let form_data = new FormData()

            form_data.append('payload', JSON.stringify({
                business_id: selectedBusiness._id,
                poster_id: auth._id,
                title: values.title,
                description: contentArray,
                pin_type: pinTypeDisplayValue[1],
                assets: JSON.stringify(assetsPayload),
            }))

            let response =
                await axios.post('social-events/business-event',
                    form_data,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            user_id: auth.id,
                            access_token: auth.facebook_access_token,
                            social_provider: auth.social_provider,
                        },
                        // cancelToken: source.token
                    })

            setIsSubmitting(false)
            navigation.goBack()

            if (response) {
            }
        } catch (error) {
            setIsSubmitting(false)
            console.debug(error)
        }
    }

    const AddTextAction = (props: any) => {
        return (
            <TouchableOpacity
                disabled={content.length > 0 ? false : true}
                onPress={() => addText(content)}
            >
                <AddIcon {...props} />
            </TouchableOpacity>
        )
    }

    const ContentTextBoxFooter: React.FC<{ text: string, index: number }> = ({
        text,
        index,
    }) => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                }}
            >
                <Button
                    style={{
                        height: 20,
                        width: 20,
                    }}
                    appearance='ghost'
                    accessoryLeft={EditIcon}
                    onPress={() => {
                        setCurrentContentArrayItem({
                            text,
                            id: index,
                        })
                        setIsEditModalVisible(true)
                    }}
                />
                <Button
                    style={{
                        height: 20,
                        width: 20,
                    }}
                    appearance='ghost'
                    accessoryLeft={TrashIcon}
                    onPress={() => {
                        let arr = contentArray.filter((f, i) => i !== index)
                        setContentArray(arr)
                    }}
                />
            </View>
        )
    }

    const ContentTextBox: React.FC<{ text: string, index: number }> = ({
        text,
        index,
    }) => {
        return (
            <Layout
                style={{
                    flexDirection: 'column',
                    maxWidth: '30%',
                    paddingTop: 5,
                    paddingHorizontal: 20,
                    marginRight: 15,
                    borderRadius: 5,
                    // borderWidth: 0.3,
                    minHeight: 50,
                }}
                level='2'
            >
                <Text numberOfLines={1}>
                    {text}
                </Text>
                <ContentTextBoxFooter
                    text={text}
                    index={index}
                />
            </Layout>
        )
    }

    const ProviderSelect = () => {
        switch (selectedProvider) {
            default:
            case 0:
                return (
                    <Select
                        style={[
                            // styles.containerItem,
                            {
                                width: '100%',
                            }
                        ]}
                        label={i18n.t('business.model.name')}
                        selectedIndex={businessIndex}
                        value={selectedBusiness.name}
                        onSelect={index => setBusinessSelect(index)}
                    >
                        {businesses.map((m, i) => (
                            <SelectItem key={i} title={m.name} />
                        ))}
                    </Select>
                )
            case 1:
                return (
                    <Text>
                        Not implemented select zone
                    </Text>
                )
            case 2:
                return (
                    <Text>
                        Not implemented select municipality
                    </Text>
                )
        }
    }

    const LoadingIndicator = (props: any) => (
        <View style={[props.style, styles.indicator]}>
            {isSubmitting ?
                <Spinner size='small' /> :
                <Icon {...props} name='plus-square-outline' />
            }
        </View>
    )

    return (
        <Layout
            style={{
                flex: 1,
            }}
        >
            <TopNavigationComponent
                navigation={navigation}
                title='Create event'
            />
            <Layout style={{ flex: 1 }}>
                <ScrollView>
                    <RadioGroup
                        style={styles.radioContainer}
                        selectedIndex={selectedProvider}
                        onChange={index => setSelectedProvider(index)}
                    >
                        <Radio
                            style={styles.radio}
                            disabled={isCheckboxDisabled.business}
                        // checked={activeChecked}
                        // onChange={nextChecked => setActiveChecked(nextChecked)}
                        >
                            Business
                        </Radio>
                        <Radio
                            style={styles.radio}
                            disabled={isCheckboxDisabled.zone}>
                            Zone
                        </Radio>
                        <Radio
                            style={styles.radio}
                            checked={true}
                            disabled={isCheckboxDisabled.municipality}>
                            Municipality
                        </Radio>
                    </RadioGroup>
                    <Layout style={styles.container}>
                        <ProviderSelect />
                    </Layout>
                    <Formik
                        initialValues={initStateFields}
                        validationSchema={validationSchema}
                        // onSubmit={values => onSubmit(values)}
                        onSubmit={values => onSubmit(values)}
                        // isInitialValid={false}
                        initialErrors={{
                            title: undefined
                        }}
                    // initialTouched={{
                    //     name: false
                    // }}
                    // validateOnMount={true}
                    >
                        {({
                            values,
                            errors,
                            isValid,
                            touched,
                            handleChange,
                            handleBlur,
                            handleReset,
                            handleSubmit,
                        }) => (
                                <Layout
                                    style={styles.container}
                                >
                                    <RangeDatepicker
                                        style={styles.containerItem}
                                        placeholder=''
                                        caption='Pick a date'
                                        captionIcon={InfoIcon}
                                        min={new Date()}
                                        range={range}
                                        accessoryRight={CalendarIcon}
                                        onSelect={nextRange => setRange(nextRange)}
                                    />
                                    <Select
                                        style={[
                                            styles.containerItem,
                                            {
                                                width: '100%',
                                            }
                                        ]}
                                        label={i18n.t('explore.create_event.pin_type')}
                                        selectedIndex={pinTypeIndex}
                                        value={pinTypeDisplayValue[1]}
                                        onSelect={index => setPinTypeSelect(index)}
                                    >
                                        {pinTypeOptions.map((m, i) => (
                                            <SelectItem key={i} title={m[1]} />
                                        ))}
                                    </Select>
                                    <Input
                                        style={{
                                            marginTop: 20,
                                        }}
                                        label={i18n.t('explore.create_event.title')}
                                        caption={i18n.t('common.required')}
                                        captionIcon={InfoIcon}
                                        status={
                                            isFieldValid(
                                                'title',
                                                errors.title,
                                                touched.title
                                            ) ? 'basic' : 'danger'
                                        }
                                        value={values.title}
                                        onChangeText={handleChange('title')}
                                    />
                                    <Input
                                        style={styles.containerItem}
                                        multiline={true}
                                        label={i18n.t('explore.create_event.content')}
                                        // caption={i18n.t('common.required')}
                                        caption={`Click the add sign to the right to add text`}
                                        captionIcon={InfoIcon}
                                        // status={
                                        //     isFieldValid(
                                        //         'content',
                                        //         errors.content,
                                        //         touched.content
                                        //     ) ? 'basic' : 'danger'
                                        // }
                                        // value={values.description}
                                        // value={values.content}
                                        // onChangeText={handleChange('content')}
                                        value={content}
                                        onChangeText={value => setContent(value)}
                                        accessoryRight={AddTextAction}
                                    />
                                    <Text
                                        style={{
                                            marginTop: 15,
                                        }}
                                    >
                                        Text contents
                                    </Text>
                                    <Layout
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'flex-start',
                                            width: '100%',
                                            minHeight: 70,
                                            marginTop: 15,
                                        }}
                                    // level='2'
                                    >
                                        {contentArray.map((m, i) => (
                                            <ContentTextBox
                                                key={i}
                                                text={m}
                                                index={i}
                                            />
                                        ))}
                                    </Layout>
                                    <ImagePreviewCarousel
                                        data={assets}
                                        // data={values.assets}
                                        // data={carousel_data}
                                        divideItemWidthBy={3}
                                        removeItem={removeAsset}
                                    />
                                    <Button
                                        style={{
                                            marginTop: 10,
                                        }}
                                        appearance='outline'
                                        onPress={openPickerModal}
                                    >
                                        {i18n.t('post.model.asset_title')}
                                    </Button>
                                    <Layout style={{
                                        marginBottom: 20,
                                        flexDirection: 'row',
                                    }}>
                                        <Button
                                            style={[
                                                styles.button,
                                                {
                                                    marginRight: 20,
                                                }
                                            ]}
                                            appearance='outline'
                                            onPress={() => {
                                                setContent('')
                                                setContentArray([])
                                                setAssets([])
                                                handleReset()
                                            }}
                                        >
                                            {i18n.t('common.reset')}
                                        </Button>
                                        <Button
                                            style={styles.button}
                                            disabled={!isValid}
                                            appearance='outline'
                                            accessoryLeft={LoadingIndicator}
                                            onPress={handleSubmit}
                                        >
                                            {i18n.t('common.post')}
                                        </Button>
                                    </Layout>
                                </Layout>
                            )}
                    </Formik>
                    <ImageModal
                        visible={isImageModalVisible}
                        setVisible={setIsImageModalVisible}
                        openPicker={openPicker}
                    />
                </ScrollView>
            </Layout>
            <EditStringModal
                visible={isEditModalVisible}
                setVisible={setIsEditModalVisible}
                item={currentContentArrayItem}
                onConfirm={(item) => {
                    let arr = contentArray.map((m, i) => {
                        if (i === item.id) {
                            m = item.text
                        }
                        return m
                    })
                    setContentArray(arr)
                    setIsEditModalVisible(false)
                }}
            />
        </Layout>
    )
}