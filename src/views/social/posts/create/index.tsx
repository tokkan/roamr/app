import { StyleSheet } from "react-native"
import { PosterTypes } from '../../../../models'

export const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    containerItem: {
        paddingTop: 20,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        marginTop: 20,
    },
    registerButton: {
        marginTop: 50,
    },
})

// export interface IState extends IStateFields {
//     showPassword: boolean
// }

export interface IAsset {
    source: string
    // type: 'image' | 'video'
}

export interface IStateFields {
    title: string | undefined
    content: string | undefined
    // assets: IAsset[]
}

export const initStateFields: IStateFields = {
    title: '',
    content: '',
    // assets: [],
}

export interface IFieldValidation {
    nameMinLength: number
    nameMaxLength: number
    contentMinLength: number
    contentMaxLength: number
}

export const initFieldValidation: IFieldValidation = {
    nameMinLength: 3,
    nameMaxLength: 25,
    contentMinLength: 1,
    contentMaxLength: 100,
}