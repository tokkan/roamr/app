import React, { useRef, useState, useEffect } from 'react'
import Carousel, { ParallaxImage, Pagination } from 'react-native-snap-carousel'
import { ImageBackground, Dimensions, View, TouchableOpacity, StyleSheet } from 'react-native'
import { Text, Layout, Button } from '@ui-kitten/components'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import { AppThemes } from '../../../../store/settings/actions'
import { CloseIcon } from '../../../../models'
import { Image } from 'react-native-image-crop-picker'

export const styles = StyleSheet.create({
    container: {
        minHeight: 150,
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        marginTop: 10,
        // alignItems: 'center',
        // alignSelf: 'center',
        // maxHeight: 350,
    },
    carousel: {
        // flex: 1,
        // justifyContent: 'center',
        // flexDirection: 'row',
        height: 200,
        width: '100%',
    },
    itemContainer: {
        height: 150,
        // height: '20%',
        width: '100%',
        // flex: 1,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        // height: 200,
        // width: 200,
    }
})

interface ICarouselRenderItem {
    item: Image
    index: number
}

interface IData {
    // title: string
    // description: string
    data: string
}

interface IProps {
    // data: IData[]
    data: Image[]
    sliderContainerWidth?: number
    divideItemWidthBy?: number
    removeItem: (item: Image) => void
}

export const ImagePreviewCarousel: React.FC<IProps> = (
    { 
        data,
        sliderContainerWidth,
        divideItemWidthBy = 1,
        removeItem,
    }
) => {
    const app_theme = useSelector((s: IRootState) => s.settings.app_theme)
    const ref = useRef()
    const [activeSlide, setActiveSlide] = useState(0)
    const [screenWidth, setScreenWidth] =
        useState(Dimensions.get('window').width)
    const [sliderWidth, setSliderWidth] =
        useState(sliderContainerWidth || screenWidth)
    const [itemWidth, setItemWidth] =
        useState((sliderContainerWidth || screenWidth) / divideItemWidthBy)
    const [dotColor, setDotColor] = useState<string>('black')

    useEffect(() => {
        setScreenWidth(Dimensions.get('window').width)
        setSliderWidth(sliderContainerWidth || screenWidth)
        setItemWidth((sliderContainerWidth || screenWidth)
            / divideItemWidthBy)
    }, [Dimensions])

    useEffect(() => {
        if (app_theme.use_system_theme ||
            app_theme.theme === AppThemes.LIGHT) {
            setDotColor('#000000')
        } else {
            setDotColor('#E6E6E6')
        }
    }, [app_theme])

    const renderItem = ({ item, index, ...rest }: ICarouselRenderItem) => {

        return (
            <Layout
                key={index}
                style={styles.itemContainer}
            >
                {/* <Button
                    style={{
                        height: '100%',
                        width: '100%',
                    }}
                    appearance='ghost'
                    onPress={() => console.log('click')}
                    accessoryLeft={() => GetComponentIcon(item.pin_type)}
                /> */}
                <ImageBackground
                    style={styles.image}
                    // source={{ uri: item.path }}
                    source={{
                        uri: `data:image/jpg;base64,${item.data}`
                    }}
                    borderRadius={2}
                >
                    <Layout
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            backgroundColor: `rgba(0, 0, 0, 0.0)`,
                            // height: 100,
                            // paddingTop: 10,
                            // paddingLeft: 10,
                            // paddingBottom: 5,
                            // shadowRadius: 120,
                            // shadowColor: 'black',
                            // shadowOffset: {
                            //     height: 10,
                            //     width: 10,
                            // },
                            // shadowOpacity: 1,
                        }}>
                        <Button
                            style={{
                                width: 0,
                                height: 0,
                                marginTop: 0,
                                marginRight: 0,
                                backgroundColor: 'rgba(0, 0, 0, 0.1)',
                            }}
                            appearance='ghost'
                            accessoryLeft={CloseIcon}
                            onPress={() => removeItem(item)}
                        />
                    </Layout>
                </ImageBackground>
            </Layout>
        )
    }

    return (
        <Layout>
            <View style={styles.container}>
                <Carousel
                    style={styles.carousel}
                    layout='default'
                    ref={ref.current}
                    data={data}
                    renderItem={renderItem}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    hasParallaxImages={true}
                    onSnapToItem={(index) => setActiveSlide(index)}
                    // loop={true}
                    enableSnap={true}
                />
                {/* <Pagination
                    carouselRef={ref.current}
                    dotsLength={data.length}
                    activeDotIndex={activeSlide}
                    dotColor={dotColor}
                    inactiveDotColor={dotColor}
                /> */}
            </View>
        </Layout>
    )
}