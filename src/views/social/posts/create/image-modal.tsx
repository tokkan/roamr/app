import React, { useState } from 'react'
import { Layout, Text, Modal, Button, Card } from '@ui-kitten/components'
import ImageCropPicker, { Options, Image } from 'react-native-image-crop-picker'
import { StyleSheet } from 'react-native'
import { RNFetchBlob, RNFetchBlobFile } from 'rn-fetch-blob'

const styles = StyleSheet.create({
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
});

interface IProps {
    visible: boolean,
    setVisible: (value: boolean) => void
    openPicker: (values: any[]) => void
}

export const ImageModal: React.FC<IProps> = ({
    visible,
    setVisible,
    openPicker,
}) => {

    const [options] = useState<Options>({
        multiple: true,
        // maxFiles: 3, // ios only
        includeBase64: true,
        compressImageMaxWidth: 1080,
        compressImageMaxHeight: 1920,
        compressImageQuality: 0.7,
        cropping: true,
    })

    const openFolderPicker = async () => {
        let response =
            await ImageCropPicker.openPicker(options)

        if (Array.isArray(response)) {
            if (response.some(s => s.size > 5e6)) {
                // image to big
            }
        }

        // console.debug(response)
        await returnFiles(response)
    }

    const openCamera = async () => {
        let response =
            await ImageCropPicker.openCamera(options)

        // console.debug(response)
        await returnFiles(response)
    }

    const cleanupAssetPicker = async () => {
        await ImageCropPicker.clean()
        setVisible(false)
    }

    const convertFileToBlob = async (path: string) => {
        try {
            console.debug(convertFileToBlob.name)
            console.debug(path)
            // return await RNFetchBlob.fs.readFile(path, 'base64')
            // let res = await RNFetchBlob.fetch('GET', path)
            // let file = await res.readFile('base64')
            console.debug(path.substring(8))
            let file =
                await RNFetchBlob.fs
                .readFile(path.substring(8), 'ascii')
            console.debug(file)
        } catch (error) {
            console.debug('RNFetchBlob error converting')
            console.debug(error)
        }
    }

    const returnFiles = async (files: Image | Image[]) => {
        if (Array.isArray(files)) {
            // let res = files.map(async m => await convertFileToBlob(m.path))
            // let res = files.map(async m => await convertFileToBlob(m.path))
            openPicker(files)
        } else {
            // let res = await convertFileToBlob(files.path)
            openPicker([files])
        }

        await cleanupAssetPicker()
    }

    return (
        <Modal
            backdropStyle={styles.backdrop}
            visible={visible}
            onBackdropPress={() => setVisible(false)}
        >
            <Card disabled={false}>
                <Text>
                    Image modal
                </Text>
                <Button
                    onPress={openFolderPicker}
                >
                    Open folder
                </Button>
                <Button
                    onPress={openCamera}
                >
                    Open camera
                </Button>
            </Card>
        </Modal>
    )
}