import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import {
    Layout, Text, Input, Button, Spinner, Icon, IndexPath, Select, SelectItem
} from '@ui-kitten/components'
import { TopNavigationComponent } from '../../../../components/top-navigation/top-navigation-stack'
import { object, string, array } from 'yup'
import { Formik } from 'formik'
import {
    IStateFields, styles, initFieldValidation, initStateFields
} from '.'
import { i18n } from '../../../../locales/localization'
import { InfoIcon, AllPinTypes, SocialMediaTypes } from '../../../../models'
import { ScrollView } from 'react-native-gesture-handler'
import { useSelector } from 'react-redux'
import { IRootState } from 'src/store'
import axios from 'axios'
import { ImageModal } from './image-modal'
import { ImagePreviewCarousel } from './image-carousel-preview'
import { Image } from 'react-native-image-crop-picker'

interface ICreateBusinessPayload {
    zone_id: string
    businessName: string
    pin_type: AllPinTypes
    sub_title: string
    description: string
    latitude: number
    longitude: number
    emailAddress: string
    phoneNumber: string
    socialMediaHandle: string
    socialMediaProvider: SocialMediaTypes
}

const validationSchema = object().shape<IStateFields>({
    // name: string()
    //     .min(3)
    //     .required(),
    // description: string()
    //     .max(25)
    //     .optional(),
    // email: string()
    //     .max(30)
    //     .optional(),
    // phone: string()
    //     .max(30)
    //     .optional(),
    // social_media_handle: string()
    //     .max(30)
    //     .optional(),
    // social_media_type: string()
    //     .oneOf(['Facebook', 'Instagram', 'Twitter'])
    //     .required(),
    // sub_title: string()
    //     .max(30)
    //     .optional(),
    title: string()
        .min(1)
        .required(),
    content: string()
        .min(1)
        .required(),
    // assets: array<any>()
    //     .min(1)
    //     .max(3)
    //     .required(),
})

interface IProps {
    route: any
    navigation: any
}

export const CreatePostScreen = ({
    route,
    navigation,
}: any) => {
    // const { latitude, longitude } = route.params
    const { business_id } = route.params
    const auth = useSelector((s: IRootState) => s.auth)
    const [source, setSource] = useState(axios.CancelToken.source())
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [isImageModalVisible, setIsImageModalVisible] = useState(false)
    const [socialProviderIndex, setSocialProviderIndex] =
        useState(new IndexPath(0))
    const [socialProviderOptions] = useState([
        'Facebook',
        'Instagram',
        'Twitter',
    ])
    const [assets, setAssets] = useState<Image[]>([])

    const socialMediaProviderDisplayValue =
        socialProviderOptions[socialProviderIndex.row]

    useEffect(() => {
        const focus = navigation.addListener('focus', () => {
            if (business_id === undefined) {
                navigation.goBack()
            }

            setSource(axios.CancelToken.source())
        })

        const unsubscribe = navigation.addListener('blur', () => {
            source.cancel()
        })

        return () => {
            focus
            unsubscribe
            source.cancel()
        }
    }, [navigation])

    const removeAsset = (item: Image) => {
        setAssets(assets.filter(f => f.path !== item.path))
    }

    const openPicker = (files: Image[]) => {
        if (assets.length + files.length > 3) {
            return
        } else {
            setAssets([...assets, ...files])
        }
    }

    const openPickerModal = () => {
        if (assets.length > 3) {
            return
        } else {
            setIsImageModalVisible(true)
        }
    }

    const onSubmit = async (values: IStateFields) => {
        try {
            setIsSubmitting(true)

            if (assets.length <= 0) {
                setIsSubmitting(false)
                return
            }

            let assetsPayload =
                assets.filter(m => {
                    if (m.data !== null) {
                        return {
                            base64: m.data,
                            width: m.width,
                            height: m.height,
                            size: m.size,
                            mime: m.mime,
                        }
                    }
                })

            let form_data = new FormData()
            
            form_data.append('payload', JSON.stringify({
                poster_id: business_id,
                title: values.title,
                content: values.content,
                assets: JSON.stringify(assetsPayload),
            }))

            let response =
                await axios.post('business/create-post',
                    form_data,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                            //     'Content-Type': 'gzip',
                            //     'Accept-Encoding': 'gzip',
                            user_id: auth.id,
                            access_token: auth.facebook_access_token,
                            social_provider: auth.social_provider,
                        },
                        cancelToken: source.token
                    })

            setIsSubmitting(false)

            if (response) {
                console.debug('response succeeded')
                console.debug(response)
            }
        } catch (error) {
            setIsSubmitting(false)
            console.debug(error)
        }
    }

    const setSocialMediaTypeSelect = (index: any) => {
        setSocialProviderIndex(index)
    }

    const isFieldValid = (
        fieldValue: keyof IStateFields,
        errorValue: string | undefined,
        touched: boolean | undefined
    ) => {
        if (errorValue === undefined) {
            return true
        } else if (fieldValue.length > 0 && errorValue !== undefined) {
            return true
        } else {
            return false
        }
    }

    const LoadingIndicator = (props: any) => (
        <View style={[props.style, styles.indicator]}>
            {isSubmitting ?
                <Spinner size='small' /> :
                <Icon {...props} name='plus-square' pack='fa5' />
            }
        </View>
    )

    return (
        <Layout style={{ flex: 1 }}>
            <TopNavigationComponent
                navigation={navigation}
                title=''
            />
            <Layout style={{ flex: 1 }}>
                <ScrollView>
                    <Layout style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        marginTop: 20,
                        marginBottom: 30,
                    }}>
                        <Text category='h5'>
                            {i18n.t('post.create.title')}
                        </Text>
                    </Layout>
                    <Formik
                        initialValues={initStateFields}
                        validationSchema={validationSchema}
                        onSubmit={values => onSubmit(values)}
                        // isInitialValid={false}
                        // initialErrors={{
                        //     name: undefined
                        // }}
                        // initialTouched={{
                        //     title: false,
                        //     content: false,
                        // }}
                        initialErrors={{
                            title: undefined,
                            content: undefined,
                        }}
                    // validateOnMount={true}
                    >
                        {({
                            values,
                            errors,
                            isValid,
                            touched,
                            handleChange,
                            handleBlur,
                            handleReset,
                            handleSubmit,
                        }) => (
                                <Layout style={styles.container}>
                                    <Input
                                        label={i18n.t('post.model.title')}
                                        caption={i18n.t('common.required')}
                                        captionIcon={InfoIcon}
                                        status={
                                            isFieldValid(
                                                'title',
                                                errors.title,
                                                touched.title
                                            ) ? 'basic' : 'danger'
                                        }
                                        value={values.title}
                                        onChangeText={handleChange('title')}
                                    // onBlur={handleBlur('title')}
                                    />
                                    <Input
                                        style={styles.containerItem}
                                        multiline={true}
                                        label={i18n.t('post.model.content')}
                                        // caption={`Should contain at least ${initFieldValidation.contentMinLength} symbols`}
                                        caption={i18n.t('common.required')}
                                        captionIcon={InfoIcon}
                                        // accessoryRight={renderIcon}
                                        status={
                                            isFieldValid(
                                                'content',
                                                errors.content,
                                                touched.content
                                            ) ? 'basic' : 'danger'
                                        }
                                        value={values.content}
                                        onChangeText={handleChange('content')}
                                    // onBlur={handleBlur('content')}
                                    />
                                    {/* <Select
                                        style={[
                                            styles.containerItem,
                                            {
                                                width: '100%',
                                            }
                                        ]}
                                        label={i18n.t('business.model.social_media_type')}
                                        selectedIndex={socialProviderIndex}
                                        value={socialMediaProviderDisplayValue}
                                        onSelect={index => setSocialMediaTypeSelect(index)}
                                    >
                                        {socialProviderOptions.map((m, i) => (
                                            <SelectItem key={i} title={m} />
                                        ))}
                                    </Select> */}
                                    <ImagePreviewCarousel
                                        data={assets}
                                        // data={values.assets}
                                        // data={carousel_data}
                                        divideItemWidthBy={3}
                                        removeItem={removeAsset}
                                    />
                                    <Button
                                        style={{
                                            marginTop: 10,
                                        }}
                                        appearance='outline'
                                        onPress={openPickerModal}
                                    >
                                        {i18n.t('post.model.asset_title')}
                                    </Button>
                                    <Layout style={{
                                        marginBottom: 20,
                                        flexDirection: 'row',
                                        justifyContent: 'flex-end',
                                    }}>
                                        <Button
                                            style={styles.button}
                                            disabled={!isValid ||
                                                assets.length <= 0}
                                            appearance='outline'
                                            accessoryLeft={LoadingIndicator}
                                            onPress={handleSubmit}
                                        >
                                            {i18n.t('common.create')}
                                        </Button>
                                    </Layout>
                                </Layout>
                            )}
                    </Formik>
                    <ImageModal
                        visible={isImageModalVisible}
                        setVisible={setIsImageModalVisible}
                        openPicker={openPicker}
                    />
                </ScrollView>
            </Layout>
        </Layout>
    )
}