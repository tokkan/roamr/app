export enum SocialMediaTypes {
    TWITTER = 'Twitter',
    INSTAGRAM = 'Instagram',
    FACEBOOK = 'Facebook',
}

export enum OtherProviderTypes {
    LOCAL = 'Local',
}

export type ProviderTypes =
    SocialMediaTypes |
    OtherProviderTypes

export enum PinTypesEnum {
    camping = 'Camping',
    cafe = 'Cafe',
    restaurant = 'Restaurant',
    trail = 'Trail',
    swimming = 'Swimming',
    fishing = 'Fishing',
    bnb = 'BNB',
    rest_area = 'Rest area',
    store = 'Store',
    heritage = 'Heritage',
    nature_reserve = 'Nature reserve',
    poi = 'POI',
    event = 'Event',
}

export enum TrailTypes {
    HIKING = 'Hiking',
    BIKING = 'Biking',
}

export enum StoreTypes {
    PHYSICAL_ITEMS = 'Physical items',
    CONSUMABLES = 'Consumables',
    MEDICINE = 'Medicine',
}

export type AllPinTypes =
    PinTypesEnum |
    TrailTypes |
    StoreTypes