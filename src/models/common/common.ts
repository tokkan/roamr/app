export enum LanguageTypes {
    se = 'Svenska',
    en = 'English',
    dk = 'German',
    nl = 'Dutch',
}

export type CountryTypes =
    'sweden' |
    'england' |
    'germany' |
    'netherlands' |
    'norway' |
    'iceland' |
    'finland' |
    'denmark'