export type AllRoutes =
    AppRoutes

export enum SettingsRoutes {
    SETTINGS = 'Settings',
}

// export enum AppRoutes {
//     APP_HOME = 'Home',
//     APP_MAP = 'Map',
//     APP_SEARCH = 'Search',
//     APP_SETTINGS = 'Settings',
//     APP_USER = 'User',
//     APP_USER_ACCOUNT = 'User account',
//     APP_BUSINESS = 'Business',
//     // APP_BUSINESS_REQUESTS = 'Business requests',
//     APP_MUNICIPALITY = 'Municipality',
//     // APP_LOGOUT = 'Logout',
//     APP_NOTIFICATIONS = 'Notifications',
// }

export type AppRoutes =
    'Home' |
    'Map' |
    'Search' |
    'Settings' |
    'User' |
    'User account' |
    'Business' |
    'Municipality' |
    'Notifications' |
    BusinessRoutes

// export enum AppRoutes {
//     HOME = 'Home',
//     MAP = 'Map',
//     SEARCH = 'Search',
//     USER = 'User',
//     // LOGOUT = 'Logout',
// }

export type BusinessRoutes =
    'Details' |
    'Post' |
    'Business overview' |
    'Create business' |
    'Edit business' |
    'Edit business tags' |
    'Business requests'


// export enum BusinessRoutes {
//     DETAILS = 'Details',
//     POST = 'Post',
//     BUSINESS = 'Business overview',
//     CREATE = 'Create business',
//     EDIT = 'Edit business',
//     EDIT_BUSINESS_TAGS = 'Edit business tags',
// }

export type ExploreRoutes =
    'Explore' |
    'Create event' |
    'Feed' |
    'Create post'

// export enum ExploreRoutes {
//     EXPLORE = 'explore',
//     CREATE_EVENT = 'Create event',
//     FEED = 'Feed',
//     CREATE_POST = 'Create post',
// }

export type MapRoutes =
    'Map' |
    'Pin details' |
    'Create pin' |
    'Create business' |
    'Create zone event'

// export enum MapRoutes {
//     MAP = 'Map',
//     PIN_DETAILS = 'Pin details',
//     CREATE_PIN = 'Create pin',
//     CREATE_BUSINESS = 'Create business',
//     CREATE_ZONE_EVENT = 'Create zone event',
// }

export type UserAccountRoutes =
    'Account' |
    'Business' |
    'Subscriptions' |
    'Geo overview'

// export enum UserAccountRoutes {
//     ACCOUNT = 'Account',
//     BUSINESS = 'Business',
//     SUBSCRIPTIONS = 'Subscriptions',
//     GEO_OVERVIEW = 'Geo overview',
// }

export type PaymentSubscriptionRoutes =
    'Select plan' |
    'Choose plan length' |
    'Summary'

// export enum PaymentSubscriptionRoutes {
//     SELECT_PLAN = 'Select plan',
//     CHOOSE_PLAN_LENGTH = 'Choose plan length',
//     SUMMARY = 'Summary',
// }