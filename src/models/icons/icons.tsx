import React from 'react'
import { ImageProps } from "react-native"
import { Icon } from "@ui-kitten/components"
import { AllPinTypes, PinTypesEnum } from '../pin'
import Fontawesome5 from 'react-native-vector-icons/FontAwesome5'

export const SearchIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='search' />
    )

export const GridIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='grid-outline' />
    )

export const FilterIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='funnel-outline' />
    )

export const CloseIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='close-square-outline' />
    )

export const TrashIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='trash-2-outline' />
    )

export const BackIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='arrow-ios-back-outline' />
    )

export const VerticalDotsIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='more-vertical-outline' />
    )

export const MenuIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='menu-outline' />
    )

export const AddIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='plus-square-outline' />
    )

export const EditIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='edit-outline' />
    )

export const CalendarIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='calendar' />
    )

export const InfoIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='info-outline' />
    )

export const SettingsIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='settings-outline' />
    )

export const LogoutIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='log-out-outline' />
    )

export const BusinessIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='office-building' pack='mdi' />
        // <Fontawesome5 {...props} name={'building'} />
    )

export const CampingIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='nature-people' pack='mdi' />
    )

// export const FishingIcon = (props?: Partial<ImageProps>):
//     React.ReactElement<ImageProps> => (
//         <Icon {...props} name='fish' pack="mdi" />
//     )

// export const HikingIcon = (props?: Partial<ImageProps>):
//     React.ReactElement<ImageProps> => (
//         <Icon {...props} name='hiking' pack="mdi" />
//     )

// export const SwimIcon = (props?: Partial<ImageProps>):
//     React.ReactElement<ImageProps> => (
//         <Icon {...props} name='swimmer' pack="mdi" />
//     )

// export const BikingIcon = (props?: Partial<ImageProps>):
//     React.ReactElement<ImageProps> => (
//         <Icon {...props} name='biking' pack="mdi" />
//     )

export const StoreIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='store' pack="mdi" />
        // <Fontawesome5 {...props} name={'store-alt'} />
    )

export const CafeIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='coffee-outline' pack="mdi" />
        // <Fontawesome5 {...props} name={'coffee'} />
    )

export const RestaurantIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='food-fork-drink' pack="mdi" />
        // <Fontawesome5 {...props} name={'utensils'} />
    )

// export const HeritageIcon = (props?: Partial<ImageProps>):
//     React.ReactElement<ImageProps> => (
//         <Fontawesome5 {...props} name={'fort-awesome'} />
//     )

export const RestIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='bed-outline' pack="mdi" />
        // <Fontawesome5 {...props} name={'bed'} />
    )

export const NatureReserveIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='tree' fill='green' pack="mdi" />
        // <Fontawesome5 {...props} name={'tree'} />
    )

export const EventIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='calendar-outline' />
    )

export const PinIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='pin-outline' />
    )

export const UserIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='person-outline' />
    )

export const CrosshairIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='crosshairs-gps' pack='mdi' />
        // <Fontawesome5 {...props} name={'crosshairs'} />
    )

export const LikeIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='heart-outline' />
    )

export const FilledLikeIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='heart' fill='red' />
    )

export const CommentIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='message-circle-outline' />
    )

export const MessageIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='email-outline' />
    )

export const MapIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='pin-outline' />
    )

export const HomeIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='home-outline' />
    )

export const NotificationsIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='bell-outline' />
    )

export const FilledNotificationsIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='bell' />
    )

export const GetComponentIcon = (
    pinType: AllPinTypes,
    width: number = 24,
    height: number = 24,
) => {

    let defaultStyles = {
        height: 20,
        tintColor: 'blue'
    }

    switch (pinType) {
        case PinTypesEnum.bnb:
            return (
                <RestIcon style={{
                    ...defaultStyles
                }} />
            )

        case PinTypesEnum.cafe:
            return (
                <CafeIcon style={{
                    ...defaultStyles,
                    height: 20,
                    tintColor: 'black'
                }} />
            )

        case PinTypesEnum.camping:
            return (
                <CampingIcon style={{
                    ...defaultStyles,
                    height: 20,
                    tintColor: 'green'
                }} />
            )

        case PinTypesEnum.event:
            return (
                <EventIcon style={{
                    ...defaultStyles
                }} />
            )

        // case PinTypesEnum.fishing:
        //     return (
        //         <FishingIcon style={{
        //             ...defaultStyles
        //         }} />
        //     )

        // case PinTypesEnum.heritage:
        //     return (
        //         <HeritageIcon style={{
        //             ...defaultStyles
        //         }} />
        //     )

        case PinTypesEnum.nature_reserve:
            return (
                <NatureReserveIcon style={{
                    ...defaultStyles
                }} />
            )

        case PinTypesEnum.poi:
            return (
                <PinIcon style={{
                    ...defaultStyles
                }} />
            )

        case PinTypesEnum.restaurant:
            return (
                <RestaurantIcon style={{
                    ...defaultStyles
                }} />
            )

        case PinTypesEnum.rest_area:
            return (
                <RestIcon style={{
                    ...defaultStyles
                }} />
            )

        case PinTypesEnum.store:
            return (
                <StoreIcon style={{
                    ...defaultStyles
                }} />
            )

        // case PinTypesEnum.swimming:
        //     return (
        //         <SwimIcon style={{
        //             ...defaultStyles
        //         }} />
        //     )

        // case PinTypesEnum.trail:
        //     return (
        //         <HikingIcon style={{
        //             ...defaultStyles
        //         }} />
        //     )

        default:
            return (
                <PinIcon style={{
                    ...defaultStyles
                }} />
            )
    }
}