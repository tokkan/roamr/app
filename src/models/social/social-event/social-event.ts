import { PinTypesEnum } from "src/models/pin";

export interface ISocialEvent {
    _id: string
    business_id: string
    title: string
    description: string[]
    latitude: number,
    longitude: number,
    // images: ObjectId[]
    images: {
        modificationDate: number,
        size: number,
        mime: string,
        data: string,
        height: number,
    }[]
    pin_type: PinTypesEnum
}