export interface ICoordinates {
    latitude: number
    longitude: number
}

export enum PosterTypes {
    BUSINESS = 'Business',
    GLOBAL = 'Global',
}

export enum PostTypes {
    FEED = 'Feed',
    NEWS = 'News',
    MAINTENANCE = 'Maintenance',
    OFFER = 'Offer',
}

export interface IPost {
    _id: string
    pin_id: string
    posted_by_id: string
    posted_by_name: string
    title: string
    content: string
    avatar: string
    location: ICoordinates
    images: string[]
    videos: string[]
    comments: string[]
    likes: string[]
}