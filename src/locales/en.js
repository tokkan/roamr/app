export default {
    common: {
        ok: 'OK',
        submit: 'Submit',
        confirm: 'Confirm',
        dismiss: 'Dismiss',
        reset: 'Reset',
        cancel: 'Cancel',
        accept: 'Accept',
        create: 'Create',
        delete: 'Delete',
        update: 'Update',
        edit: 'Edit',
        post: 'Post',
        upload: 'Upload',
        download: 'Download',
        yes: 'Yes',
        no: 'No',
        open: 'Open',
        close: 'Close',
        toggle: 'Toggle',
        required: 'Required',
        optional: 'Optional',
        no_items: 'No items found',
        searching: 'Searching',
        language: 'Language',
    },
    drawer: {
        menu_title: 'Menu',
        tabs: {
            home: {
                title: 'Home',
                sub_title: '',
            },
            user_account: {
                title: 'Account',
                sub_title: '',
            },
            subscriptions: {
                title: 'Subscriptions',
                sub_title: '',
            },
            business: {
                title: 'Business',
                sub_title: 'Open business directory',
            },
            business_requests: {
                title: 'Requests',
                sub_title: 'Business Requests',
            },
            municipality: {
                title: 'Municipality',
                sub_title: 'Open municipality directory',
            },
            logout: {
                title: 'Logout',
                sub_title: 'Signs out of the app',
            }
        }
    },
    login: {
        facebook_login: "Login with Facebook",
    },
    settings: {
        settings_title: 'Settings',
        theme_title: 'Theme settings',
        system_theme: 'Use system theme',
        map_theme: 'Map theme',
        language: 'Language',
    },
    explore: {
        current_events: 'Current events in',
        suggestions: 'Suggestions',
        create_event: {
            title: 'Title',
            content: 'Content',
            pin_type: 'Type',
        }
    },
    map: {
        menu: {
            header: 'Options',
            get_nearby_pins: 'Get nearby pins',
            suggest_pin: 'Suggest pin',
            create_pin_item: 'Create new pin',
            create_business_item: 'Create business',
            create_event_item: 'Create event',
            copy_coordinates: 'Copy Coordinates',
            open_external_nav_app: 'Open navigation app',
        },
        help: {
            title: 'Help',
            description: '',
            on_press_info: 'On tap: tap on a icon and retrieve information',
            on_long_press_info: 'On long press: long press to open options',
            speed_dial_info: 'Speed dial: tap the speed dial in the lower right to open actions',
        }
    },
    business: {
        model: {
            name: 'Business name',
            sub_title: 'Sub title',
            description: 'Description',
            business_tags: 'Business tags',
            merchandise: 'Merchandise',
            business_hours: 'Business hours',
            contact: 'Contact',
            phone: 'Phone',
            email: 'Email',
            social_media_handle: 'Social network username',
            social_media_type: 'Social network',
            pin_type: 'Main business type',
            sub_pin_type: 'Sub business types',
        },
        create: {
            title: 'Apply for business',
        },
        details: {
            title: 'Business details',
            no_items: `Couldn't find business`,
        },
        list: {
            title: 'Connected businesses',
            post: 'Post',
            edit: 'Edit',
            tags: 'Tags',
            social_media: 'Social media',
        },
        requests: {
            title: 'Business requests',
            short_title: 'Requests',
            sub_title: 'Business requests',
            info: 'Info',
            confirm_message: 'Are you sure to activate this business?'
        }
    },
    post: {
        model: {
            title: 'Title',
            content: 'Content',
            assets: 'Assets',
            asset_title: 'Upload asset',
        },
        create: {
            title: 'Create post',
        }
    }
}