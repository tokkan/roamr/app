import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Modal, Card, Text, Button, Layout } from '@ui-kitten/components'
import { i18n } from '../../../locales/localization'
import { InfoIcon } from '../../../models'

interface IProps {
    visible: boolean
    setVisible: (value: boolean) => void
    title: string | undefined
    subTitle: string | undefined
    content: string
    onConfirm: (() => void) | (() => Promise<void>) | undefined
}

export const ConfirmationModal: React.FC<IProps> = ({
    visible,
    setVisible,
    title,
    subTitle,
    content,
    onConfirm,
}) => {

    const Header = (props: any) => {
        return (
            <>
                {title !== undefined && title.length > 0 ?
                    <View {...props}>
                        <Text category='h5'>{title}</Text>
                        {subTitle !== undefined && subTitle.length > 0 &&
                            <Text category='s2'>{subTitle}</Text>
                        }
                    </View>
                    :
                    <></>
                }
            </>
        )
    }

    const Footer = (props: any) => (
        <View {...props} style={[
            props.style,
            styles.actionContainer,
        ]}>
            {onConfirm !== undefined &&
                <Button
                    style={{
                        marginRight: 10,
                    }}
                    appearance='outline'
                    status='success'
                    onPress={onConfirm}
                >
                    {i18n.t('common.confirm')}
                </Button>
            }
            <Button
                appearance='outline'
                onPress={() => setVisible(false)}
            >
                {i18n.t('common.cancel')}
            </Button>
        </View>
    )

    return (
        <Modal
            visible={visible}
            backdropStyle={styles.backdrop}
            onBackdropPress={() => setVisible(false)}
        >
            <Card
                disabled={true}
                status='warning'
                header={Header}
                footer={Footer}
                style={styles.card}
            >
                {/* <InfoIcon /> */}
                <Text>
                    {content}
                </Text>
            </Card>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: 192,
    },
    actionContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    card: {
        flex: 1,
        margin: 2,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
})