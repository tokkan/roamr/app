import React from 'react'
import {
    TopNavigation, TopNavigationAction, Icon, Divider
} from '@ui-kitten/components'
import { ImageProps } from 'react-native'

const BackIcon = (props?: Partial<ImageProps>):
    React.ReactElement<ImageProps> => (
        <Icon {...props} name='arrow-ios-back-outline' />
    )

interface IProps {
    navigation: any
    title: string
    onGoBackCallback?: () => void
    replaceNavigate?: () => void
}

export const TopNavigationComponent: React.FC<IProps> = ({
    navigation,
    title,
    onGoBackCallback,
    replaceNavigate,
}) => {
    const goBack = () => {
        if (replaceNavigate === undefined) {
            navigation.goBack()
            
            if (onGoBackCallback !== undefined) {
                onGoBackCallback()
            }
        } else {
            replaceNavigate()
        }
    }

    const BackAction = () => (
        <TopNavigationAction
            icon={BackIcon}
            onPress={goBack}
        />
    )

    return (
        <>
            <TopNavigation
                title={title}
                alignment='center'
                accessoryLeft={BackAction}
            />
            <Divider />
        </>
    )
}