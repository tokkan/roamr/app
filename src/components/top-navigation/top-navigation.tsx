import React, { useState, useEffect } from 'react'
import {
    TopNavigation, TopNavigationAction, Divider
} from '@ui-kitten/components'
import { StyleSheet, Image, ImageProps } from 'react-native'
import { DrawerActions } from '@react-navigation/native'
import { MenuIcon } from './../../models/icons'

interface IProps {
    navigation: any
    title: string
    hasMenuAction: boolean
    rightAccessory?: {
        icon: (props?: Partial<ImageProps> | undefined) => React.ReactElement<ImageProps>
        action: (...params: any) => void
    }
}

export const TopNavigationComponent: React.FC<IProps> =
    ({
        navigation,
        title,
        hasMenuAction,
        rightAccessory,
    }) => {

        // const [visible, setVisible] = useState(false)
        // const [selectedIndex, setSelectedIndex] = React.useState<number | null>(null)

        useEffect(() => {
        }, [])

        const toggleDrawer = () => {
            navigation.dispatch(DrawerActions.toggleDrawer())
        }

        // const onItemSelect = (item: any) => {
        //     // setSelectedIndex(itemIndex)
        //     setVisible(false)
        // }

        const MenuAction = () => (
            <TopNavigationAction
                icon={MenuIcon}
                onPress={toggleDrawer}
            />
        )

        const RightAction = () => {
            if (rightAccessory !== undefined) {
                return (
                    <TopNavigationAction
                        icon={rightAccessory.icon}
                        onPress={rightAccessory.action}
                    />
                )
            } else {
                return <></>
            }
        }

        // const titleImage = () => (
        //     <Image
        //         source={{
        //             uri: require('./ROAMR-03-image-title.png')
        //         }}
        //     />
        // )

        return (
            <>
                <TopNavigation
                    title={title}
                    // title={titleImage}
                    alignment='center'
                    accessoryLeft={hasMenuAction ? MenuAction : undefined}
                    accessoryRight={rightAccessory !== undefined ? RightAction : undefined}
                />
                <Divider />
            </>
        )
    }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//         paddingLeft: 10,
//         paddingRight: 10,
//     },
//     listContainer: {
//         width: '100%',
//     },
//     containerItem: {
//         paddingTop: 20,
//     },
//     backdrop: {
//         backgroundColor: 'rgba(0, 0, 0, 0.5)',
//     },
//     button: {
//         marginTop: 20,
//     }
// })