import React from 'react'
import { Text } from '@ui-kitten/components'

interface IProps {
    count: number
    color?: string
    textColor?: string
}

export const NotificationBadge: React.FC<IProps> = ({
    count,
    color = '#408000',
    textColor = 'white',
}) => {
    if (count > 0) {
        return (
            <Text
                style={{
                    position: 'absolute',
                    height: 16,
                    width: 16,
                    borderRadius: 200,
                    fontSize: 12,
                    top: 8,
                    left: 37,
                    textAlign: 'center',
                    color: textColor,
                    backgroundColor: color,
                }}
            >
                {count > 99 ?
                    99 : count}
            </Text>
        )
    } else {
        return <></>
    }
}