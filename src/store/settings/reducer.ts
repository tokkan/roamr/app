import {
    ISettingsState, initialState, SettingsActions, SettingsActionTypes, LanguageTypes
} from "./actions"
import { i18n } from './../../locales/localization'

export default function (
    state: ISettingsState = initialState,
    action: SettingsActionTypes
): ISettingsState {
    switch (action.type) {
        case SettingsActions.CHANGE_APP_THEME:
            return {
                ...state,
                app_theme: {
                    ...state.app_theme,
                    theme: action.payload.theme,
                    // use_system_theme: action.payload.use_system_theme,
                }
            }

        case SettingsActions.SET_DEFAULT_APP_THEME:
            return {
                ...state,
                app_theme: {
                    // theme: action.payload.theme,
                    ...state.app_theme,
                    use_system_theme: action.payload.use_system_theme,
                }
            }

        case SettingsActions.CHANGE_MAP_THEME:
            return {
                ...state,
                map_theme: action.payload.map_theme,
            }

        case SettingsActions.CHANGE_LANGUAGE:

            i18n.locale = action.payload.language

            return {
                ...state,
                language: action.payload.language,
            }

        default:
            return state
    }
}