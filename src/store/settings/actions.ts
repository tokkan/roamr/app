import MapboxGL from "@react-native-mapbox-gl/maps"

export enum SettingsActions {
    CHANGE_APP_THEME = 'Change app theme',
    CHANGE_MAP_THEME = 'Change map theme',
    SET_DEFAULT_APP_THEME = 'Change to default',
    CHANGE_LANGUAGE = 'Change language',
}

export interface ISettingsActionDispatch {
    [SettingsActions.CHANGE_APP_THEME]: IAppThemePayload
    [SettingsActions.CHANGE_MAP_THEME]: IMapThemePayload
    [SettingsActions.SET_DEFAULT_APP_THEME]: IAppDefaultThemePayload
    [SettingsActions.CHANGE_LANGUAGE]: ILanguagePayload
}

export enum AppThemes {
    DARK = 'dark',
    LIGHT = 'light',
}

export enum LanguageTypes {
    EN = 'en',
    SE = 'se',
    DK = 'dk',
    NL = 'nl',
}

export type LanguageType =
    LanguageTypes.EN |
    LanguageTypes.SE

export interface ISettingsState {
    app_theme: IAppThemeState
    map_theme: MapboxGL.StyleURL
    language: LanguageType
}

export const initialState: ISettingsState = {
    app_theme: {
        theme: AppThemes.DARK,
        use_system_theme: false,
    },
    map_theme: MapboxGL.StyleURL.Outdoors,
    language: LanguageTypes.EN,
}

export interface IAppThemeState {
    theme: AppThemes
    use_system_theme: boolean
}

export interface IMapThemeState {
    map_theme: MapboxGL.StyleURL
}

export interface ILanguageState {
    language: LanguageType
}

export type SettingsActionTypes =
    IAppThemeAction |
    IAppDefaultThemeAction |
    IMapThemeAction |
    ILanguageAction

export interface IAction<T, P> {
    type: T
    payload: P
}

export interface IAppThemeAction extends IAction<
    typeof SettingsActions.CHANGE_APP_THEME,
    IAppThemeState
    > { }

export interface IAppDefaultThemeAction extends IAction<
    typeof SettingsActions.SET_DEFAULT_APP_THEME,
    IAppThemeState
    > { }

export interface IMapThemeAction extends IAction<
    typeof SettingsActions.CHANGE_MAP_THEME,
    IMapThemeState
    > { }

export interface ILanguageAction extends IAction<
    typeof SettingsActions.CHANGE_LANGUAGE,
    ILanguageState
    > { }

export interface IAppThemePayload {
    theme: AppThemes
    // use_system_theme: boolean
}

export interface IMapThemePayload {
    map_theme: MapboxGL.StyleURL
}

export interface IAppDefaultThemePayload {
    use_system_theme: boolean
}

export interface ILanguagePayload {
    language: LanguageType
}