import { all, put, takeLatest, select, call } from 'redux-saga/effects'
import axios, { AxiosResponse } from 'axios'
import { AuthActions, IAuthState } from './auth-actions'
// import { Actions } from '../player'
// import { IPlayer } from 'src/models'

// https://decembersoft.com/posts/redux-hero-part-4-every-hero-needs-a-villain-a-fun-introduction-to-redux-saga-js/

const source = axios.CancelToken.source()

export enum LoginPaths {
    LOGIN = '/auth/login',
    REGISTER = '/auth/register',
    CHECK_NAME = '/auth/check-name',
}

interface IStateFields {
    username: string
    password: string
}

function* securityCheck() {
}

function* loginUser() {
    const onLogin = async (values: IStateFields) => {

        try {

            let response =
                await axios.post(LoginPaths.LOGIN,
                    {
                        username: values.username,
                        password: values.password
                    },
                    {
                        cancelToken: source.token
                    })

            if (response) {
                // LoginUser({
                //     username: response.data.username,
                //     access_token: response.data.access_token,
                // })
                put({
                    type: AuthActions.LOGIN, payload: {
                        username: response.data.username,
                        access_token: response.data.access_token,
                    }
                })

            } else {
                source.cancel('Operation cancelled by failing getting response')
            }

        } catch (error) {

            if (axios.isCancel(error)) {
                if (__DEV__) {
                    console.log('HTTP request cancelled', error.message)
                }
            }

            if (__DEV__) {
                console.log(error)
            }
        }
    }
}

function* logoutUser() {
    console.log('Saga logoutUser')
    yield put({ type: AuthActions.LOGOUT })
}

// function* disconnectPlayer() {
//     console.log('Saga disconnectPlayer')
//     yield put({ type: Actions.DISCONNECT_PLAYER })
// }

// export function* logoutSaga() {
//     // yield all([
//     //     logoutUser(),
//     //     disconnectPlayer()
//     // ])
//     yield takeLatest(AuthActions.LOGOUT, disconnectPlayer)
// }

// interface IConnectPlayer {
//     access_token: string
//     player_name: string
// }

// function* connectPlayer({ props }: any) {

//     console.log('connect player')
//     console.log(props)
//     const root = yield select()
//     const state: IAuthState = root.auth

//     try {
//         const res = yield call(connectPlayerRequest, { username: state.username })

//         console.log('after res')
//         // console.log(res)
//         yield put({ type: Actions.CONNECT_PLAYER, payload: res })
//     } catch (error) {

//     }
// }

// async function connectPlayerRequest({ username }: any) {

//     console.log('connect player request')
//     console.log(username)

//     try {
//         const response =
//             await axios.get<any, AxiosResponse<IPlayer>>(`/players?username=${username}`,
//                 {
//                     headers: { Authorization: '' },
//                     cancelToken: source.token,
//                 })

//         // await axios.post(`/player?id=${player_id}`,
//         //     {
//         //         username: values.username,
//         //         access_token: values.password
//         //     },
//         //     {
//         //         cancelToken: source.token
//         //     })

//         if (response) {
//             return response.data

//         } else {
//             source.cancel('Operation cancelled by failing getting response')
//         }

//     } catch (error) {

//         if (axios.isCancel(error)) {
//             if (__DEV__) {
//                 console.log('HTTP request cancelled', error.message)
//             }
//         }
//     }
// }

// export function* loginSaga() {
//     yield takeLatest(AuthActions.LOGIN, connectPlayer)
// }