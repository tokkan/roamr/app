import {
    SocialMediaTypes, OtherProviderTypes, ProviderTypes
} from "./../../models"
import { CountryTypes } from "src/models/common/common"

export enum AuthActions {
    LOGIN = 'login',
    LOCAL_LOGIN = 'local login',
    SOCIAL_LOGIN = 'social login',
    LOGOUT = 'logout',
    AUTHENTICATE = 'authenticate',
    AUTHORIZE = 'authorize',
    SET_CURRENT_MUNICIPALITY = 'set current municipality'
}

export interface IAuthActionDispatch {
    [AuthActions.LOGIN]: ILocalLoginPayload
    [AuthActions.SOCIAL_LOGIN]: ISocialLoginPayload
    [AuthActions.LOGOUT]: null
    [AuthActions.SET_CURRENT_MUNICIPALITY]: ICurrentMunicipalityPayload
}

export enum AuthRoles {
    PLEB = 'pleb',
    AFFILIATE = 'affiliate',
    MOD = 'mod',
    ADMIN = 'admin',
}

export interface ICurrentMunicipality {
    municipality_fire_id: string
    municipality_mongo_id: string
    municipality_name: string
    municipality_code: string
}

export interface ICurrentCountry {
    country: CountryTypes
}

export interface IAuthState {
    _id: string
    id: string
    username: string
    name: string
    first_name: string
    last_name: string
    avatar: string
    access_token: string
    social_provider: ProviderTypes
    facebook_access_token: string
    mapbox_access_token: string
    isAuthenticated: boolean
    roles: AuthRoles[]
    authorization_levels: any[]
    current_municipality: ICurrentMunicipality
}

export const initState: IAuthState = {
    _id: '',
    id: '',
    username: '',
    name: '',
    first_name: '',
    last_name: '',
    avatar: '',
    access_token: '',
    social_provider: OtherProviderTypes.LOCAL,
    facebook_access_token: '',
    mapbox_access_token: '',
    isAuthenticated: false,
    roles: [],
    authorization_levels: [],
    current_municipality: {
        municipality_fire_id: '',
        municipality_mongo_id: '',
        municipality_name: '',
        municipality_code: '',
    }
}

export interface IAction<T, P> {
    type: T
    payload: P
}

export type AuthActionTypes =
    ILoginAction |
    ISocialLoginAction |
    ILogoutAction |
    ICurrentMunicipalityAction

export interface ILoginAction extends IAction<
    typeof AuthActions.LOGIN,
    IAuthState
    > { }

export interface ISocialLoginAction extends IAction<
    typeof AuthActions.SOCIAL_LOGIN,
    ISocialLoginPayload
    > { }

export interface ILogoutAction extends IAction<
    typeof AuthActions.LOGOUT,
    null
    > { }

export interface ICurrentMunicipalityAction extends IAction<
    typeof AuthActions.SET_CURRENT_MUNICIPALITY,
    ICurrentMunicipalityPayload
    > { }

export interface ISocialLoginPayload {
    _id: string
    id: string
    username: string
    name: string
    first_name: string
    last_name: string
    avatar: string
    access_token: string
    social_provider: ProviderTypes
    facebook_access_token: string
    mapbox_access_token: string
    roles: AuthRoles[]
}

export interface ILocalLoginPayload {
    username: string
    access_token: string
}

export interface ICurrentMunicipalityPayload {
    municipality_name: string
    municipality_code: string
}