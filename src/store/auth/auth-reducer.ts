import {
    AuthActions, IAuthState, initState, AuthActionTypes
} from './auth-actions'

export default function (
    state: IAuthState = initState,
    action: AuthActionTypes): IAuthState {

    switch (action.type) {

        case AuthActions.LOGIN:
            if (action.payload.access_token !== undefined &&
                action.payload.access_token !== null &&
                action.payload.access_token.length > 0) {

                const s = {
                    ...state,
                    _id: action.payload._id,
                    id: action.payload.id,
                    username: action.payload.username,
                    access_token: action.payload.access_token,
                    isAuthenticated: true,
                }

                return s

            } else {
                return {
                    ...state,
                    _id: '',
                    id: '',
                    username: '',
                    access_token: '',
                    isAuthenticated: false,
                }
            }

        case AuthActions.SOCIAL_LOGIN:
            return {
                ...state,
                ...action.payload,
                isAuthenticated: true,
            }

        case AuthActions.LOGOUT:
            return initState

        default:
            return state
    }
}