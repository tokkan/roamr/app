import { combineReducers, createStore } from 'redux'
import authReducer from './auth/auth-reducer'
import settingsReducer from './settings/reducer'

const rootReducer = combineReducers({
    auth: authReducer,
    settings: settingsReducer,
})

export type IRootState = ReturnType<typeof rootReducer>

export interface IActionDispatch<T> {
    <K extends keyof T>(action: { type: K, payload: T[K] }): () => T
}

export const store = createStore(rootReducer)